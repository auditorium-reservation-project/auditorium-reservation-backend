﻿using Microsoft.IdentityModel.Tokens;
using System.Buffers.Text;
using System.Text;

namespace AuditoriumReservation.Configuration
{
    public class AuthenticationConfiguration
    {
        public string JwtIssuerSigningKey { get; set; }
        public string JwtIssuer { get; set;  }
        public string JwtAudience { get; set; }
        public string HttpOnlyCookieName { get; set; }
        public int LifeTimeMinutes { get; set; }
    }
}
