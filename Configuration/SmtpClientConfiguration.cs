﻿namespace AuditoriumReservation.Configuration
{
    public class SmtpClientConfiguration
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string ClientLogin { get; set; }
        public string ClientPassword { get; set; }
        public string SenderAddress { get; set; }
        public string SenderName { get; set;}
    }
}
