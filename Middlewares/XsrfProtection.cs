﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.Extensions.Options;

namespace AuditoriumReservation.Middlewares
{
    public class XsrfProtection
    {
        private readonly RequestDelegate _next;

        public XsrfProtection(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IAntiforgery antiforgery)
        {
            try
            {
                context.Response.Cookies.Append("XsrfToken",
                    antiforgery.GetAndStoreTokens(context).RequestToken,
                    new CookieOptions
                    {
                        HttpOnly = false,
                        Secure = true,
                        MaxAge = TimeSpan.FromMinutes(60)
                    });
            } catch (ArgumentException)
            {

            }

            await _next(context);
        }
    }
}
