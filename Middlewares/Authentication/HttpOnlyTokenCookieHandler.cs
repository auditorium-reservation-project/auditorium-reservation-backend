﻿using AuditoriumReservation.Configuration;
using Microsoft.Extensions.Options;

namespace AuditoriumReservation.Middlewares.Authentication
{
    public class HttpOnlyTokenCookieHandler
    {
        private readonly RequestDelegate _next;
        private readonly AuthenticationConfiguration _authenticationConfiguration;
        public HttpOnlyTokenCookieHandler(RequestDelegate next, IOptions<AuthenticationConfiguration> authenticationConfiguration)
        {
            _next = next;
            _authenticationConfiguration = authenticationConfiguration.Value;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Cookies[_authenticationConfiguration.HttpOnlyCookieName];
            if (!string.IsNullOrEmpty(token))
            {
                context.Request.Headers.Add("Authorization", "Bearer " + token);
            }

            await _next(context);
        }
    }
}
