﻿using Microsoft.AspNetCore.Antiforgery;

namespace AuditoriumReservation.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseXsrfDetectionMiddleware(this IApplicationBuilder app)
            => app.UseMiddleware<XsrfProtection>();
    }
}
