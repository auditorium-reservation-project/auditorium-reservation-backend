﻿using System.Globalization;

namespace AuditoriumReservation.Utility
{
    public static class DateOnlyExtensions
    {
        public static IEnumerable<DateOnly> GetDatesOfWeek(this DateOnly date, CultureInfo ci)
        {
            Int32 firstDayOfWeek = Convert.ToInt32(ci.DateTimeFormat.FirstDayOfWeek);
            Int32 dayOfWeek = Convert.ToInt32(date.DayOfWeek);
            var startOfWeek = date.AddDays(firstDayOfWeek - dayOfWeek);
            var valuesDaysOfWeek = Enum.GetValues(typeof(DayOfWeek)).Cast<Int32>();
            return valuesDaysOfWeek.Select(v => startOfWeek.AddDays(v));
        }

        public static IEnumerable<DateOnly> GetRange(this DateOnly from, DateOnly to, int interval)
        {
            var dateTimeTo = to.ToDateTime(TimeOnly.MinValue);
            for(var day = from.ToDateTime(TimeOnly.MinValue).Date; day.Date <= dateTimeTo.Date; day.AddDays(interval))
            {
                yield return DateOnly.FromDateTime(day);
            }
        }
        public static IEnumerable<DateTime> GetRange(this DateTime from, DateTime to, int interval)
        {
            for(var day = from.Date; day.Date <= to.Date; day.AddDays(interval))
            {
                yield return day;
            }
        }
    }
}
