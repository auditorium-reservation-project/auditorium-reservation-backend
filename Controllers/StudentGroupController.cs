﻿using AuditoriumReservation.DTO.CreateUpdate;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("groups")]
    [AutoValidateAntiforgeryToken]
    public class StudentGroupController : ControllerBase
    {
        private readonly IStudentGroupService _groupService;

        public StudentGroupController(IStudentGroupService studentGroupService)
        {
            _groupService = studentGroupService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllGroups([FromQuery] bool includeFaculties = false)
        {
            return Ok(await _groupService.GetAllGroups(includeFaculties));
        }

        [HttpPut("{groupId:guid}/students")]
        public async Task<IActionResult> AddStudents([FromRoute] Guid groupId, [FromBody] List<Guid> studentIds)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _groupService.AddStudentsToGroup(groupId, studentIds));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Group {{id: {groupId}}}") { StatusCode = 500 };
            }
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetGroup([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _groupService.GetGroup(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Group {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateGroup([FromBody] CreateGroupDto groupDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _groupService.CreateGroup(groupDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed to create Group") { StatusCode = 500};
            }
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateGroup([FromRoute] Guid id, [FromBody] UpdateGroupDto groupDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _groupService.UpdateGroup(id, groupDto));
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed to update Group {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteGroup([FromRoute] Guid id)
        {
            try
            {
                await _groupService.DeleteGroup(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Group {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _groupService.DeleteAll();
            return Ok();
        }
    }
}
