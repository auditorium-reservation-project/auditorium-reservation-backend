﻿using AuditoriumReservation.Configuration;
using AuditoriumReservation.DAL;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace AuditoriumReservation.Controllers
{
    [Route("auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        private readonly AuthenticationConfiguration _authConfig;
        private readonly ITokenService _tokenService;

        public AuthenticationController(ReservationDbContext context,
            IAuthenticationService authenticationService,
            IOptions<AuthenticationConfiguration> authenticationConfiguration,
            IUserService userService,
            ITokenService tokenService)
        {
            _dbContext = context;
            _authenticationService = authenticationService;
            _authConfig = authenticationConfiguration.Value;
            _tokenService = tokenService;
            _userService = userService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var loginResult = await _authenticationService.Login(dto);

                var claims = new List<Claim>{ new Claim(ClaimTypes.Name, loginResult.Id.ToString()) };

                claims.AddRange(loginResult.Roles.Select(role => new Claim(ClaimTypes.Role, role.ToString())));

                var token = _tokenService.GetAccessToken(claims);
                HttpContext.Response.Cookies.Append(
                    _authConfig.HttpOnlyCookieName,
                    token,
                    new CookieOptions
                    {
                        MaxAge = TimeSpan.FromMinutes(_authConfig.LifeTimeMinutes),
                        HttpOnly = true,
                        Secure = true
                    });

                return Ok(UserConverter.FromUser(loginResult));
            }
            catch(AccessDeniedException ex)
            {
                return Unauthorized(ex.Message);
            }
            catch (Exception)
            {
                return Unauthorized();
            }
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            if (HttpContext.Request.Cookies.ContainsKey(_authConfig.HttpOnlyCookieName) == false)
            {
                return new ObjectResult("not logged in") { StatusCode = 205 };
            }

            HttpContext.Response.Cookies.Delete(_authConfig.HttpOnlyCookieName);
            return Ok("Logged out");
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(CreateUserDto registerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var result = await _userService.Register(registerDto);

                return Ok(result);
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return Unauthorized("Failed registration");
            }
        }
    }
}
