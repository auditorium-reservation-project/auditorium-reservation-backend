﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.CompilerServices;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("auditoria")]
    [AutoValidateAntiforgeryToken]
    public class AuditoriumController : ControllerBase
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IAuditoriumService _auditoriumService;
        public AuditoriumController(ReservationDbContext dbContext, IAuditoriumService auditoriumService)
        {
            _dbContext = dbContext;
            _auditoriumService = auditoriumService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAuditoria()
        {
            try
            {
                return Ok(await _auditoriumService.GetAuditoria());
            }
            catch (Exception)
            {
                return new ObjectResult("Failed retrieving Auditoria") { StatusCode = 500 };
            }
        }

        [HttpGet("available")]
        public async Task<IActionResult> GetUnoccupiedAuditoria([FromQuery] DateOnly date,
            [FromQuery] int timeSlotIndex,
            [FromQuery] Guid? buildingId = null)
        {
            try
            {
                return Ok(await _auditoriumService.GetUnoccupiedAuditoria(date, timeSlotIndex, buildingId));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return new ObjectResult("Failed retrieving auditoria") { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddAuditorium([FromBody] CreateAuditoriumDto auditoriumDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _auditoriumService.CreateAuditorium(auditoriumDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed creating auditorium") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetAuditorium([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _auditoriumService.GetAuditorium(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving auditorium {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateAuditorium([FromRoute] Guid id, [FromBody] UpdateAuditoriumDto auditoriumDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _auditoriumService.UpdateAuditorium(id, auditoriumDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed to update Auditorium {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteAuditorium([FromRoute] Guid id)
        {
            try
            {
                await _auditoriumService.DeleteAuditorium(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Auditorium {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _auditoriumService.DeleteAll();
            return Ok();
        }
    }
}
