﻿using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("buildings")]
    [AutoValidateAntiforgeryToken]
    public class BuildingController : ControllerBase
    {
        private readonly IBuildingService _buildingService;
        public BuildingController(IBuildingService buildingService)
        {
            _buildingService = buildingService;
        }

        [HttpGet]
        public async Task<IActionResult> GetBuildings([FromQuery] bool detailed = false)
        {
            return Ok(await _buildingService.GetAllBuildings(detailed));
        }        

        [HttpGet]
        [Route("{buildingId:guid}/auditoria")]
        public async Task<IActionResult> GetAuditoriaForBuilding([FromRoute] Guid buildingId)
        {
            try
            {
                return Ok(await _buildingService.GetAllAuditoria(buildingId));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500};
            }
        }

        [HttpGet]
        [Route("{buildingId:guid}/auditoria/{auditoriumId:guid}")]
        public async Task<IActionResult> GetAuditoriumForBuilding([FromRoute] Guid buildingId, [FromRoute] Guid auditoriumId)
        {
            try
            {
                return Ok(await _buildingService.GetAuditorium(buildingId, auditoriumId));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddBuilding([FromBody] CreateUpdateBuildingDto buildingDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _buildingService.CreateBuilding(buildingDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed creating building") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetBuilding([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _buildingService.GetBuilding(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving building {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateBuilding([FromRoute] Guid id, [FromBody] CreateUpdateBuildingDto buildingDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _buildingService.UpdateBuilding(id, buildingDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Building {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteBuilding([FromRoute] Guid id)
        {
            try
            {
                await _buildingService.DeleteBuilding(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Building {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _buildingService.DeleteAll();
            return Ok();
        }
    }
}
