﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using Microsoft.AspNetCore.Mvc;
using AuditoriumReservation.DTO.Enumerations;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Services.Schedule;
using AuditoriumReservation.DTO.ScheduleDto;
using AuditoriumReservation.Services.Synchronization;
using EnvironmentName = Microsoft.AspNetCore.Hosting.EnvironmentName;
using AuditoriumReservation.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("classes")]
    //[AutoValidateAntiforgeryToken]
    public class ClassController : ControllerBase
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IScheduleService _scheduleService;
        private readonly ISynchronizationService _syncService;
        public ClassController(ReservationDbContext context,
            IScheduleService scheduleService,
            ISynchronizationService syncService)
        {
            _dbContext = context;
            _scheduleService = scheduleService;
            _syncService = syncService;
        }

        [HttpPost("sync")]
        [Authorize(Roles = "Admin,Staff")]
        public async Task<IActionResult> SyncSchedule()
        {
            try
            {
                await _syncService.SyncSchedule();
                return Ok();
            }
            catch (Exception ex)
            {
                if(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == EnvironmentName.Development)
                {
                    return new ObjectResult(ex.ToString()) { StatusCode = 500 };
                }
                return new ObjectResult("Failed synchronizing schedule") { StatusCode = 500 };
            }
        }

        [HttpDelete("sync")]
        public async Task<IActionResult> DeleteSyncData()
        {
            try
            {
                await _syncService.DeleteSynchronizedData();
                return Ok();
            }
            catch (Exception)
            {
                return new ObjectResult("Failed deleting syncrhonized data") { StatusCode = 500 };
            }
        }

        [HttpGet("time-slots")]
        public async Task<IActionResult> GetTimeSlots()
        {
            try
            {
                return Ok(await _scheduleService.GetAllTimeSlots());
            }
            catch (Exception)
            {
                return new ObjectResult($"Failed retrieving Time slots") { StatusCode = 500 };
            }
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetClass([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _scheduleService.GetClass(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Class {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetClasses([FromQuery] GetClassesRequestDto getScheduleDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _scheduleService.GetSchedule(getScheduleDto));
            }
            catch (InvalidModelStateException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult(ex.ToString()) { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateClass([FromBody] CreateClassDto classDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _scheduleService.CreateClass(classDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ScheduleConflictException<ClassConcurrencyConflict> ex)
            {
                var conflictDtoList = ex.ScheduleConflicts
                    .Select(x => new ClassConcurrencyConflictDto(x))
                    .ToList();


                return Conflict(conflictDtoList);
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed creating Class") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateClass([FromRoute] Guid id, [FromBody] UpdateClassDto classDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _scheduleService.UpdateClass(id, classDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Class {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteClass([FromRoute] Guid id)
        {
            try
            {
                await _scheduleService.DeleteClass(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Class {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Classes.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
