﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Services.MailClient;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("users")]
    [AutoValidateAntiforgeryToken]
    public class UserController : ControllerBase
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IUserService _userService;
        private readonly IMailService _mailService;
        public UserController(ReservationDbContext dbContext, IUserService userService, IMailService mailService)
        {
            _dbContext = dbContext;
            _userService = userService;
            _mailService = mailService;
        }

        [DevelopmentOnly]
        [HttpPost("send-mail-test")]
        public async Task<IActionResult> SendMail(string text)
        {
            try
            {
                await _mailService.SendEmailAsync("disturbedelk@gmail.com", "Mail Test", text);
                return Ok();
            }
            catch (Exception ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
        }


        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] CreateUserDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _userService.Register(dto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed user registration") { StatusCode = 500 };
            }
        }

        [HttpGet("self")]
        public async Task<IActionResult> GetProfileSelf()
        {
            if (!Guid.TryParse(User.Identity?.Name, out var userId))
            {
                return Unauthorized("Failed User Authorization");
            }

            return await GetProfileById(userId);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetProfileById([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _userService.GetProfile(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed To Retrieve User Profile {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateProfile([FromRoute] Guid id, [FromBody] UpdateUserDto userDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _userService.UpdateProfile(id, userDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed to update User Profile {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteProfile([FromRoute] Guid id)
        {
            try
            {
                await _userService.DeleteProfile(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting User {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _userService.DeleteAll();
            return Ok();
        }
    }
}
