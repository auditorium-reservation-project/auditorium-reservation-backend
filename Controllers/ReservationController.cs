﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("reservations")]
    //[AutoValidateAntiforgeryToken]
    public class ReservationController : ControllerBase
    {
        private readonly IReservationService _reservationService;
        private readonly ReservationDbContext _dbContext;
        public ReservationController(IReservationService scheduleService, ReservationDbContext context)
        {
            _reservationService = scheduleService;
            _dbContext = context;
        }

        [HttpGet("applications")]
        [Authorize(Roles = "Staff,Admin")]
        public async Task<IActionResult> GetUnapproved()
        {
            try
            {
                return Ok(await _reservationService.GetUnapproved());
            } catch (Exception ex)
            {
                return new ObjectResult("Failed retrieving reservation applications") { StatusCode = 500 };
            }
        }

        [HttpGet("auditoria/{id:guid}")]
        public async Task<IActionResult> GetForAuditorium([FromRoute] Guid id, [FromQuery] DateOnly startDate, [FromQuery] DateOnly endDate)
        {
            try
            {
                return Ok(await _reservationService.GetForAuditorium(id, startDate, endDate));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Reservations for auditorium") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetReservation([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _reservationService.GetReservation(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Reservation {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetReservations([FromQuery] GetReservationsRequestDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _reservationService.GetReservations(dto));
            }
            catch (Exception)
            {
                return new ObjectResult("Failed retrieving Reservations") { StatusCode = 500 };
            }
        }

        [HttpPost("{id:guid}/approve")]
        [Authorize(Roles = "Staff")]
        public async Task<IActionResult> ApproveReservation([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _reservationService.ApproveReservation(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ScheduleException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return new ObjectResult("Failed Reservation approval");
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Student,Staff")]
        public async Task<IActionResult> AddReservation([FromBody] CreateReservationDto reservationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!Guid.TryParse(User.Identity?.Name, out Guid userId))
            {
                return Unauthorized("Failed User Authorization");
            }

            var user = await _dbContext.Users.FindAsync(userId);
            if (user is null)
            {
                return Unauthorized("Failed User Authorization");
            }

            try
            {
                return Ok(await _reservationService.CreateReservation(user, reservationDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ScheduleException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return new ObjectResult("Failed creating Reservaiton") { StatusCode = 500 };
            }
        }

        [HttpPost]
        [Route("{id:guid}/reject")]
        public async Task<IActionResult> RejectReservation([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _reservationService.CancelReservation(id));
            }
            catch (Exception)
            {
                return new ObjectResult($"Failed updating Reservation") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateReservation([FromRoute] Guid id, [FromBody] UpdateReservationDto reservationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!Guid.TryParse(User.Identity?.Name, out Guid userId))
            {
                return Unauthorized("Failed User Authorization");
            }

            var user = await _dbContext.Users.FindAsync(userId);
            if (user is null)
            {
                return Unauthorized("Failed User Authorization");
            }

            try
            {
                return Ok(await _reservationService.UpdateReservation(user, id, reservationDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Reservation {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPost]
        [Route("{id:guid}/cancel")]
        [Authorize(Roles = "Admin,Staff")]
        public async Task<IActionResult> CancelReservation([FromRoute] Guid id)
        {
            try
            {
                await _reservationService.CancelReservation(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed cancelling Reservation {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Reservations.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
