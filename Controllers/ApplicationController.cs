﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.Enumerations;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("applications")]
    [AutoValidateAntiforgeryToken]
    public class ApplicationController : ControllerBase
    {
        private readonly IApplicationsService _applicationsService;
        private readonly IReservationService _reservationService;
        private readonly ReservationDbContext _dbContext;
        public ApplicationController(IApplicationsService applicationsService, IReservationService reservationService, ReservationDbContext context)
        {
            _applicationsService = applicationsService;
            _reservationService = reservationService;
            _dbContext = context;
        }

        [HttpPost("roles")]
        [Authorize(Roles = "Staff,Admin,Student")]
        public async Task<IActionResult> ApplyForRole([FromQuery] Role role)
        {
            if(!Guid.TryParse(User.Identity.Name, out var userId))
            {
                return Unauthorized();
            }

            var user = await _dbContext.Users.SingleOrDefaultAsync(x => x.Id == userId);
            if(user is null)
            {
                return Unauthorized();
            }

            return Ok(await _applicationsService.Apply(user, role));
        }

        [HttpGet("roles")]
        [Authorize(Roles = "Staff,Admin")]
        public async Task<IActionResult> GetRoleApplications()
        {
            return Ok(await _applicationsService.GetRoleApplications());
        }

        [HttpPost("roles/{id:guid}/reject")]
        [Authorize(Roles = "Staff,Admin")]
        public async Task<IActionResult> RejectApplication([FromRoute]Guid id)
        {
            return Ok(await _applicationsService.Reject(id));
        }

        [HttpPost("roles/{id:guid}/approve")]
        [Authorize(Roles = "Staff,Admin")]
        public async Task<IActionResult> ApproveApplication([FromRoute] Guid id)
        {
            return Ok(await _applicationsService.Approve(id));
        }

    }
}
