﻿using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("faculties")]
    [AutoValidateAntiforgeryToken]
    public class FacultyController: ControllerBase
    {
        private readonly IFacultyService _facultyService;
        public FacultyController(IFacultyService facultyService)
        {
            _facultyService = facultyService;
        }

        [HttpGet]
        public async Task<IActionResult> GetFaculties([FromQuery] bool includeGroups = false)
        {
            try
            {
                return Ok(await _facultyService.GetFaculties(includeGroups));
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed retrieving Faculties") { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddFaculty([FromBody] CreateUpdateFacultyDto facultyDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _facultyService.CreateFaculty(facultyDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed creating Faculty") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetFaculty([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _facultyService.GetFaculty(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Faculty {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateFaculty([FromRoute] Guid id, [FromBody] CreateUpdateFacultyDto facultyDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _facultyService.UpdateFaculty(id, facultyDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Faculty {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteFaculty([FromRoute] Guid id)
        {
            try
            {
                await _facultyService.DeleteFaculty(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Faculty {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _facultyService.DeleteAll();
            return Ok();
        }
    }
}
