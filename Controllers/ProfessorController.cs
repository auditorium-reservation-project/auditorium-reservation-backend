﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("professors")]
    [AutoValidateAntiforgeryToken]
    public class ProfessorController : ControllerBase
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IProfessorService _professorService;
        public ProfessorController(ReservationDbContext context, IProfessorService professorService)
        {
            _dbContext = context;
            _professorService = professorService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProfessors()
        {
            try
            {
                return Ok(await _professorService.GetProfessors());
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed retrieving Professors") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("self")]
        public async Task<IActionResult> GetProfessorSelf()
        {
            var userId = Guid.Empty;
            try
            {
                return Ok(await _professorService.GetProfessorByLinkedUserId(userId));
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed retrieving Professors") { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddProfessor([FromBody] CreateUpdateProfessorDto professorDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _professorService.CreateProfessor(professorDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed creating Professor") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetProfessor([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _professorService.GetProfessor(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Professor {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateBuilding([FromRoute] Guid id, [FromBody] CreateUpdateProfessorDto professorDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _professorService.UpdateProfessor(id, professorDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Professor {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteProfessor([FromRoute] Guid id)
        {
            try
            {
                await _professorService.DeleteProfessor(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Professor {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _professorService.DeleteAll();
            return Ok();
        }
    }
}
