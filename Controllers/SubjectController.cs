﻿using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Controllers
{
    [ApiController]
    [Route("subjects")]
    [AutoValidateAntiforgeryToken]
    public class SubjectController: ControllerBase
    {
        private readonly ISubjectService _subjectService;
        public SubjectController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSubjects()
        {
            try
            {
                return Ok(await _subjectService.GetAllSubjects());
            }
            catch (Exception)
            {
                return new ObjectResult("Failed retrieving Subjects") { StatusCode = 500 };
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddSubject([FromBody] CreateUpdateSubjectDto subjectDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _subjectService.CreateSubject(subjectDto));
            }
            catch (AlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return new ObjectResult("Failed creating Subject") { StatusCode = 500 };
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetSubject([FromRoute] Guid id)
        {
            try
            {
                return Ok(await _subjectService.GetSubject(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed retrieving Subject {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateSubject([FromRoute] Guid id, [FromBody] CreateUpdateSubjectDto subjectDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await _subjectService.UpdateSubject(id, subjectDto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed updating Subject {{id: {id}}}") { StatusCode = 500 };
            }
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteSubject([FromRoute] Guid id)
        {
            try
            {
                await _subjectService.DeleteSubject(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ReservationBaseException ex)
            {
                return new ObjectResult(ex.Message) { StatusCode = 500 };
            }
            catch (Exception ex)
            {
                return new ObjectResult($"Failed deleting Subject {{id: {id}}}") { StatusCode = 500 };
            }
        }

        /// <summary>
        /// Development Only
        /// </summary>
        /// <remarks>Only available in dev. environment</remarks>
        [HttpDelete]
        [DevelopmentOnly]
        public async Task<IActionResult> DeleteAll()
        {
            await _subjectService.DeleteAll();
            return Ok();
        }
    }
}
