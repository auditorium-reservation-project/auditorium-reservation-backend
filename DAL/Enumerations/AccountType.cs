﻿namespace AuditoriumReservation.DAL.Enumerations
{
    public enum AccountType
    {
        PROFESSOR,
        STUDENT,
        NONE
    }
}
