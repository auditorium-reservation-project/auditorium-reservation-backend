﻿namespace AuditoriumReservation.DAL.Enumerations
{
    public enum ReservationStatus
    {
        Proposed,
        Approved
    }
}
