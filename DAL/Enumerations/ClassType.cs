﻿namespace AuditoriumReservation.DAL.Enumerations
{
    public enum ClassType
    {
        Lecture,
        Practicum,
        LaboratoryWork,
        Seminar,
        Control,
        Individual,
        DiffCredit,
        Credit,
        Consultation,
        Exam,
        Other
    }
}
