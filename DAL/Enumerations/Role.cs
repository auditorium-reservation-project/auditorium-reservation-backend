﻿namespace AuditoriumReservation.DAL.Enumerations
{
    public enum Role
    {
        Admin,
        Staff,
        Student
    }
}
