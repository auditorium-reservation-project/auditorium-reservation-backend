﻿using System.Collections.Generic;
using System.ComponentModel;
using AuditoriumReservation.DAL.Converters;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DAL.Entities.ReferenceEntityIdRecords;
using AuditoriumReservation.DAL.Enumerations;
using AuditoriumReservation.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AuditoriumReservation.DAL
{
    public class ReservationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Professor> Professors { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<Auditorium> Auditoria { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<RoleApplication> RoleApplications { get; set; }
        public DbSet<StaffMember> Staff { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<TimeSlot> TimeSlots { get; set; }

        //Synchronization entities
        public DbSet<BuildingRecord> BuildingsRecords { get; set; }
        public DbSet<AuditoriumRecord> AuditoriaRecords { get; set; }
        public DbSet<ProfessorRecord> ProfessorRecords { get; set; }
        public DbSet<FacultyRecord> FacultyRecords { get; set; }
        public DbSet<SubjectRecord> SubjectRecords { get; set; }
        public DbSet<StudentGroupRecord> StudentGroupRecords { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseNpgsql("Host=127.0.0.1;Username=AuditoriumReservationDevUser;Password=4a45938af38917434fdd336f3d28;Database=auditorium_reservation_db");
        }

        public ReservationDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(user =>
            {
                //user.HasDiscriminator<string>("account_type");
                user.HasIndex(u => u.Email);
                user.Property(u => u.Roles)
                    .HasConversion(new EnumCollectionValueConverter<Role>());
                user.HasMany(u => u.RelatedGroups)
                    .WithMany();
            });

            modelBuilder.Entity<RoleApplication>(roleApplication =>
            {
                roleApplication.HasOne(r => r.User)
                    .WithMany(u => u.RoleApplications)
                    .OnDelete(DeleteBehavior.Cascade);
                roleApplication.Property(r => r.Role)
                    .HasConversion(new EnumValueConverter<Role>());
                //roleApplication.HasAlternateKey(nameof(RoleApplication.User), nameof(RoleApplication.Role));
            });

            modelBuilder.Entity<Professor>(professor =>
            {
                professor.HasOne(p => p.LinkedUser)
                    .WithOne()
                    .HasForeignKey<Professor>("LinkedUserId")
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.SetNull);
                professor.HasMany(p => p.Faculties)
                    .WithMany(f => f.Professors);
            });

            modelBuilder.Entity<StaffMember>(staff =>
            {
                staff.HasOne(s => s.Faculty)
                    .WithMany(f => f.StaffMembers)
                    .OnDelete(DeleteBehavior.SetNull);

                staff.HasOne(s => s.ProfessorEntity)
                    .WithOne()
                    .HasForeignKey<StaffMember>("ProfessorEntityId")
                    .OnDelete(DeleteBehavior.Cascade);

                staff.HasOne(s => s.LinkedUser)
                    .WithOne()
                    .HasForeignKey<StaffMember>("LinkedUserId")
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<Auditorium>()
                .HasOne(a => a.Building)
                .WithMany(u => u.Auditoria)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<StudentGroup>()
                .HasOne(group => group.Faculty)
                .WithMany(faculty => faculty.StudentGroups)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Class>(@class =>
            {
                @class.HasOne(c => c.TimeSlot)
                    .WithMany(s => s.Classes)
                    .OnDelete(DeleteBehavior.Cascade);

                @class.HasOne(c => c.Subject)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Cascade);

                @class.HasOne(c => c.Auditorium)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Cascade);

                //@class.HasOne(c => c.StudentGroup)
                //    .WithMany()
                //    .OnDelete(DeleteBehavior.Cascade);

                @class.HasMany(c => c.StudentGroups)
                    .WithMany();

                @class.HasOne(c => c.Professor)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Cascade);

                @class.Property(@class => @class.Type)
                    .HasConversion(new EnumValueConverter<ClassType>());
            });

            modelBuilder.Entity<Reservation>(reservation =>
            {
                reservation.HasOne(r => r.CreatedBy)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Cascade);

                reservation.HasOne(r => r.Auditorium)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Cascade);

                reservation.HasOne(r => r.TimeSlot)
                    .WithMany()
                    .OnDelete(DeleteBehavior.Cascade);

            });

            modelBuilder.Entity<ProfessorRecord>(record =>
            {
                record.HasOne(r => r.RelatedEntity)
                    .WithOne()
                    .HasForeignKey<ProfessorRecord>("RelatedEntityId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<AuditoriumRecord>(record =>
            {
                record.HasOne(r => r.RelatedEntity)
                    .WithOne()
                    .HasForeignKey<AuditoriumRecord>("RelatedEntityId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<BuildingRecord>(record =>
            {
                record.HasOne(r => r.RelatedEntity)
                    .WithOne()
                    .HasForeignKey<BuildingRecord>("RelatedEntityId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<FacultyRecord>(record =>
            {
                record.HasOne(r => r.RelatedEntity)
                    .WithOne()
                    .HasForeignKey<FacultyRecord>("RelatedEntityId")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<SubjectRecord>(record =>
            {
                record.HasOne(r => r.RelatedEntity)
                    .WithOne()
                    .HasForeignKey<SubjectRecord>("RelatedEntityId")
                    .OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<StudentGroupRecord>(record =>
            {
                record.HasOne(r => r.RelatedEntity)
                    .WithOne()
                    .HasForeignKey<StudentGroupRecord>("RelatedEntityId")
                    .OnDelete(DeleteBehavior.Cascade);
            });
            //modelBuilder.Entity<ClassBaseEntity>().UseTphMappingStrategy();
            InitializeContextData(modelBuilder);
        }

        private void InitializeContextData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TimeSlot>()
                .HasData(
                    new TimeSlot { Index = 1, StartTime = TimeOnly.ParseExact("08:45", "HH:mm"), EndTime = TimeOnly.ParseExact("10:20", "HH:mm") },
                    new TimeSlot { Index = 2, StartTime = TimeOnly.ParseExact("10:35", "HH:mm"), EndTime = TimeOnly.ParseExact("12:10", "HH:mm") },
                    new TimeSlot { Index = 3, StartTime = TimeOnly.ParseExact("12:25", "HH:mm"), EndTime = TimeOnly.ParseExact("14:00", "HH:mm") },
                    new TimeSlot { Index = 4, StartTime = TimeOnly.ParseExact("14:45", "HH:mm"), EndTime = TimeOnly.ParseExact("16:20", "HH:mm") },
                    new TimeSlot { Index = 5, StartTime = TimeOnly.ParseExact("16:35", "HH:mm"), EndTime = TimeOnly.ParseExact("18:10", "HH:mm") },
                    new TimeSlot { Index = 6, StartTime = TimeOnly.ParseExact("18:25", "HH:mm"), EndTime = TimeOnly.ParseExact("20:00", "HH:mm") },
                    new TimeSlot { Index = 7, StartTime = TimeOnly.ParseExact("20:15", "HH:mm"), EndTime = TimeOnly.ParseExact("21:50", "HH:mm") }
                );

            modelBuilder.Entity<User>()
                .HasData(
                    new User() { 
                        Id = Guid.NewGuid(),
                        FirstName = "Root",
                        Email = "root@root.net",
                        AvatarUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg",
                        Password = "HochuSpat",
                        Roles = new List<Role>() { Role.Admin, Role.Staff, Role.Student }                        
                    }
                );

            //List<Building> buildings = new List<Building> { new Building() { Id = Guid.NewGuid(), Name = "1 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "2 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "3 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "4 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "5 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "6 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "7 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "8 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "9 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "10 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "11 корпус " }, new Building() { Id = Guid.NewGuid(), Name = "12 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "13 корпус (НИИББ)" }, new Building() { Id = Guid.NewGuid(), Name = "14 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "15 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "16 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "17 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "18 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "20 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "21 корпус (Ботсад)" }, new Building() { Id = Guid.NewGuid(), Name = "23 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "24 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "29 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "31 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "32 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "33 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "34 корпус" }, new Building() { Id = Guid.NewGuid(), Name = "Детский сад № 49" }, new Building() { Id = Guid.NewGuid(), Name = "Криогенный корпус" }, new Building() { Id = Guid.NewGuid(), Name = "Онлайн" } };
            //modelBuilder.Entity<Building>()
            //    .HasData(buildings.ToArray());


        }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.Properties<DateOnly>()
                .HaveConversion<Converters.DateOnlyConverter>()
                .HaveColumnType("date");
            configurationBuilder.Properties<TimeOnly>()
                .HaveConversion<Converters.TimeOnlyConverter>()
                .HaveColumnType("time");
            
            configurationBuilder.Properties<DateTime>()
                .HaveConversion<Converters.DateTimeConverter>()
                .HaveColumnType("timestamp with time zone");
            base.ConfigureConventions(configurationBuilder);
        }
    }
}
