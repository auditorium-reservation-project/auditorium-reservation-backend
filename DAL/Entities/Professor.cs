﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuditoriumReservation.DAL.Entities
{
    public class Professor
    {
        //[NotMapped]
        //public string FullName => this.FirstName + " " + this.MiddleName + " " + this.LastName;

        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Unnamed Professor";
        public ICollection<Faculty> Faculties { get; set; } = new List<Faculty>();
        public User? LinkedUser { get; set; }
    }
}
