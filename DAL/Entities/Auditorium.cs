﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class Auditorium
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name{ get; set; }
        public Building Building { get; set; }
    }
}
