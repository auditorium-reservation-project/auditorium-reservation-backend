﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class StaffMember
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name = "Staff Member";
        public Faculty? Faculty { get; set; }
        public User? LinkedUser { get; set; }
        public Professor ProfessorEntity { get; set; }
    }
}
