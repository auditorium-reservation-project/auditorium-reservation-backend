﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class Building
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Unnamed Building";
        public ICollection<Auditorium> Auditoria { get; set; } = new List<Auditorium>();
    }
}
