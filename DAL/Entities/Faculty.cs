﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class Faculty
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Unnamed Faculty";
        public ICollection<StudentGroup> StudentGroups { get; set; } = new List<StudentGroup>();
        public ICollection<Professor> Professors { get; set; } = new List<Professor>();
        public ICollection<StaffMember> StaffMembers { get; set; } = new List<StaffMember>();
    }
}
