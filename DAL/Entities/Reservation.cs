﻿using AuditoriumReservation.DAL.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class Reservation
    {
        [Key]
        public Guid Id { get; set; }
        public User CreatedBy { get; set; }
        public ReservationStatus Status { get; set; } = ReservationStatus.Proposed;
        public DateTime CreatedOn { get; set; }
        public string Name { get; set; }
        public string? Details { get; set; }
        public DateOnly Date { get; set; }
        public TimeSlot TimeSlot { get; set; }
        public Auditorium Auditorium { get; set; }
        public bool EmailSubscription { get; set; } = false;
    }
}
