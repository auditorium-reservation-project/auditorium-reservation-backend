﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class StudentGroup
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Unnamed Student Group";
        public Faculty Faculty { get; set; }
    }
}
