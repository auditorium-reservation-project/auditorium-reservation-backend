﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities.ReferenceEntityIdRecords
{
    public abstract class Record<T> where T : class
    {
        [Key]
        public string RemoteId { get; set; }
        public T RelatedEntity { get; set; }
    }

    public class ProfessorRecord : Record<Professor> {}
    public class AuditoriumRecord : Record<Auditorium> {}
    public class BuildingRecord : Record<Building> {}
    public class FacultyRecord : Record<Faculty> {}
    public class SubjectRecord : Record<Subject> {}
    public class StudentGroupRecord : Record<StudentGroup> {}
}
