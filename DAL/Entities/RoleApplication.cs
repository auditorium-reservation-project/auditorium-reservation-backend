﻿using AuditoriumReservation.DAL.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class RoleApplication
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public User User { get; set; }
        public Role Role { get; set; }
        public DateTime DateTimeOfApplication { get; set; } = DateTime.UtcNow;
    }
}
