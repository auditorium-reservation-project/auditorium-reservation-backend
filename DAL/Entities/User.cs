﻿using AuditoriumReservation.DAL.Enumerations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuditoriumReservation.DAL.Entities
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Email { get; set; }
        public string Password { get; set; }
        public string? AvatarUrl { get; set; }
        public ICollection<Role> Roles { get; set; } = new List<Role> { Role.Student };
        public ICollection<RoleApplication> RoleApplications { get; set; } = new List<RoleApplication>();
        public ICollection<StudentGroup> RelatedGroups { get; set; } = new List<StudentGroup>();
        public string FirstName { get; set; } = "Unnamed User";
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }

        [NotMapped]
        public string FullName => FirstName
            + (MiddleName is not null ? " " + MiddleName : "")
            + (LastName is not null ? " " + LastName : "");
    }
}
