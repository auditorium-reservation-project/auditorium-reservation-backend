﻿namespace AuditoriumReservation.DAL.Entities
{
    public class Subject
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; } = "Unnamed Class";
    }
}
