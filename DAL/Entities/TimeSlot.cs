﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DAL.Entities
{
    public class TimeSlot
    {
        [Key]
        public int Index{ get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public ICollection<Class> Classes { get; set; } = new List<Class>();
    }
}
