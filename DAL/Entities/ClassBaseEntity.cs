﻿using AuditoriumReservation.DAL.Enumerations;

namespace AuditoriumReservation.DAL.Entities
{
    public class Class
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public ClassType Type { get; set; }
        public Subject Subject { get; set; }
        public Professor Professor { get; set; }
        public ICollection<StudentGroup> StudentGroups { get; set; } = new List<StudentGroup>();
        public Auditorium Auditorium { get; set; }
        public TimeSlot TimeSlot { get; set; }
        //public DateOnly StartDate { get; set; }
        //public DateOnly EndDate { get; set; }
        public DateOnly Date { get; set; }
    }
}
