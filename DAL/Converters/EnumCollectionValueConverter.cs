﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;

namespace AuditoriumReservation.DAL
{
    public class EnumCollectionValueConverter<T> : ValueConverter<ICollection<T>, string> where T : Enum
    {
        public EnumCollectionValueConverter() : base(
          e => SerializeEntity(e),
          v => DeserializeValue(v))
        {

        }

        private static string SerializeEntity(ICollection<T> entity)
        {
            return JsonConvert.SerializeObject(entity.Select(e => e.ToString()).ToList());
        }
        private static ICollection<T> DeserializeValue(string value)
        {
            var rawCollection = JsonConvert.DeserializeObject<ICollection<string>>(value);
            if (rawCollection is null)
            {
                throw new ArgumentNullException("Raw collection is null when deserialized from " + value);
            }
            return rawCollection.Select(e => (T)Enum.Parse(typeof(T), e)).ToList();
        }
    }
}
