﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AuditoriumReservation.DAL.Converters
{
    public class DateTimeConverter: ValueConverter<DateTime, DateTime>
    {
        public DateTimeConverter() : base(
            v => v.ToUniversalTime(),
            v => DateTime.SpecifyKind(v, DateTimeKind.Utc)
        ) { }
    }
}
