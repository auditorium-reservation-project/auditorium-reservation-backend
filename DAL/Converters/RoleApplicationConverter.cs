﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DAL.Converters
{
    public static class RoleApplicationConverter
    {
        public static RoleApplicationDto FromApplication(RoleApplication application)
        {
            return new RoleApplicationDto()
            {
                Id = application.Id,
                Role = UserConverter.MatchRoleToDtoRole(application.Role),
                DateTimeOfApplication = application.DateTimeOfApplication,
                UserId = application.User.Id,
                UserName = application.User.FullName
            };
        }
    }
}
