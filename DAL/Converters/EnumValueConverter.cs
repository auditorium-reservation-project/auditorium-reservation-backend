﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AuditoriumReservation.DAL.Converters
{
    public class EnumValueConverter<T> : ValueConverter<T, string> where T : Enum
    {
        public EnumValueConverter() : base(
            e => e.ToString(),
            v => (T)Enum.Parse(typeof(T), v))
        { }
    }
}
