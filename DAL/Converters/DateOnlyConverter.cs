﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AuditoriumReservation.DAL.Converters
{
    public class DateOnlyConverter: ValueConverter<DateOnly,  DateTime>
    {
        public DateOnlyConverter() : base(
            dateOnly => dateOnly.ToDateTime(TimeOnly.MinValue),
            dateTime => DateOnly.FromDateTime(dateTime)
        ) { }
    }
}
