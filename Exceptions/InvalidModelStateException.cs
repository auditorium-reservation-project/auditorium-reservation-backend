﻿namespace AuditoriumReservation.Exceptions
{
    public class InvalidModelStateException : ReservationBaseException
    {
        public InvalidModelStateException(string message) : base(message) { }
    }
    public class InvalidModelStateException<T> : ReservationBaseException
    {
        public InvalidModelStateException(string message) : base(string.Concat(message, $" (for model: {typeof(T).Name})")) { }
        public InvalidModelStateException() : base($"{typeof(T).Name} is invalid") { }
        public InvalidModelStateException(object id) : base($"({typeof(T).Name}) {id.ToString()} is invalid") { }
    }
}
