﻿using AuditoriumReservation.Services.Schedule;

namespace AuditoriumReservation.Exceptions
{
    public class ScheduleConflictException<T> : ScheduleException where T : ScheduleConflict
    {
        public List<T> ScheduleConflicts;
        public ScheduleConflictException(ICollection<T> conflicts): base("Unresolved Schedule Conflict")
        {
            this.ScheduleConflicts = conflicts.ToList();
        }
    }
}
