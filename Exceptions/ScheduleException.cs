﻿namespace AuditoriumReservation.Exceptions
{
    public class ScheduleException : ReservationBaseException {
        public ScheduleException() { }
        public ScheduleException(string message) : base(message) { }
    }
}
