﻿namespace AuditoriumReservation.Exceptions
{
    public class ReservationBaseException : Exception
    {
        public ReservationBaseException() { }
        public ReservationBaseException(string message) : base(message) { }
        public ReservationBaseException(string message,  Exception innerException) : base(message, innerException) { }
    }
}
