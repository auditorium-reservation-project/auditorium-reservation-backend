﻿namespace AuditoriumReservation.Exceptions
{
    public class AccessDeniedException : ReservationBaseException
    {
        public AccessDeniedException() : base() { }
        public AccessDeniedException(string message) : base(message) { }
    }
}
