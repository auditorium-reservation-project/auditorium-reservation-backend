﻿namespace AuditoriumReservation.Exceptions
{
    public class NotFoundException : ReservationBaseException
    {
        public NotFoundException(string message) : base(message) { }
    }
    public class NotFoundException<T> : ReservationBaseException
    {
        public NotFoundException(string message) : base(message) { }
        public NotFoundException() : base($"{typeof(T).Name} was not found") { }
        public NotFoundException(object id) : base($"({typeof(T).Name}) {id.ToString()} was not found") { }
        public NotFoundException(ICollection<object> ids):
            base($"({typeof(T).Name}) [{String.Join(",", ids)}] were not found"){}
    }
}
