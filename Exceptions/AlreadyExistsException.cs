﻿namespace AuditoriumReservation.Exceptions
{
    public class AlreadyExistsException : ReservationBaseException
    {
        public AlreadyExistsException(string message) : base(message) { }
    }
    public class AlreadyExistsException<T> : ReservationBaseException
    {
        public AlreadyExistsException(string message) : base(message) { }
        public AlreadyExistsException() : base($"{typeof(T).Name} already exists") { }
        public AlreadyExistsException(object id) : base($"({typeof(T).Name}) {id.ToString()} already exists") { }
    }
}
