using AuditoriumReservation.Configuration;
using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.Middlewares;
using AuditoriumReservation.Middlewares.Authentication;
using AuditoriumReservation.Services;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Services.MailClient;
using AuditoriumReservation.Services.Synchronization;
using AuditoriumReservation.Utility;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Converters;
using System.Diagnostics;
using System.Text;
using IAuthenticationService = AuditoriumReservation.Services.Interfaces.IAuthenticationService;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;

builder.Configuration.AddJsonFile("Security.json");
builder.Configuration.AddJsonFile("SmtpClient.json");

services.Configure<AuthenticationConfiguration>(options =>
    builder.Configuration.GetSection("Security")
        .Bind(options, configureOptions => configureOptions.BindNonPublicProperties = true));

services.Configure<SmtpClientConfiguration>(options =>
    builder.Configuration.GetSection("Smtp")
        .Bind(options)
);

services.AddDbContext<ReservationDbContext>(options =>
{
    options.UseNpgsql(Environment.GetEnvironmentVariable("POSTGRESQL_DB_CONNECTION")
        ?? builder.Configuration.GetConnectionString("PostgresDbConnection"));
});

var authenticationConfiguration = builder.Configuration.GetSection("Security")
    .Get<AuthenticationConfiguration>(configureOptions => configureOptions.BindNonPublicProperties = true);

services.AddControllers()
    .AddNewtonsoftJson(options => {
        options.SerializerSettings.Converters.Add(new StringEnumConverter());
        options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
    });

services.AddScoped<IUserService, UserService>();
services.AddScoped<IFacultyService, FacultyService>();
services.AddScoped<IProfessorService, ProfessorService>();
services.AddScoped<ISubjectService, SubjectService>();
services.AddScoped<IStudentGroupService, StudentGroupService>();
services.AddScoped<IScheduleService, ScheduleService>();
services.AddScoped<IReservationService, ReservationService>();
services.AddScoped<IBuildingService, BuildingService>();
services.AddScoped<IAuditoriumService, AuditoriumService>();
services.AddScoped<IAuthenticationService, AuthenticationService>();
services.AddScoped<IUserAuthenticationHelperService, UserAuthenticationHelperService>();
services.AddScoped<IPasswordHasher<User>, PasswordHasher<User>>();
services.AddScoped<IApplicationsService,  ApplicationsService>();

services.AddScoped<HttpClient, HttpClient>();
services.AddScoped<IMailService, MailService>();
services.AddScoped<ISynchronizationService,  SynchronizationService>();
services.AddTransient<ITokenService, TokenService>();
services.AddHostedService<SynchronizationHelperService>();

services.AddEndpointsApiExplorer();
services.AddSwaggerGenNewtonsoftSupport();
services.AddSwaggerGen(options =>
{
    var schemaHelper = new SwashbuckleSchemaHelper();
    var documentationPath = Path.Combine(AppContext.BaseDirectory, "AuditoriumReservation.xml");
    options.CustomSchemaIds(type => schemaHelper.GetSchemaId(type));
    options.IncludeXmlComments(documentationPath);

});

services.AddHealthChecks();
services.AddCors();
services.AddAntiforgery(options => { options.HeaderName = "x-xsrf-token";  } );
services.AddMvc().AddControllersAsServices();

services.AddDbContext<ReservationDbContext>();


services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
{
    options.RequireHttpsMetadata = true;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
            authenticationConfiguration?.JwtIssuerSigningKey ?? throw new ArgumentNullException("Security:JwtIssuerSigningKey is not set in configuration")
        )),
        ValidIssuer = authenticationConfiguration.JwtIssuer,
        ValidAudience = authenticationConfiguration.JwtAudience,

    };
});


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        //options.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
        //options.RoutePrefix = string.Empty;
    });
}


app.UseCors(policy => {
    var env = Environment.GetEnvironmentVariable("ORIGINS_URL");
    var altOrigins = new string[] { "http://localhost:3000", "https://localhost:3000", "http://127.0.0.1:3000", "https://127.0.0.1:3000" };
    var origins = env == null ? altOrigins : new string[] { env };
    policy.WithOrigins(origins)
        .AllowCredentials()
        .AllowAnyMethod()
        .AllowAnyHeader();
});

app.UseCookiePolicy(new CookiePolicyOptions
{
    MinimumSameSitePolicy = SameSiteMode.Strict,
    HttpOnly = HttpOnlyPolicy.Always,
    Secure = CookieSecurePolicy.Always
});

app.UsePathBase("/api");
app.MapHealthChecks("/health");
app.UseHttpsRedirection();
app.UseXsrfDetectionMiddleware();

app.UseRouting();

app.UseMiddleware<HttpOnlyTokenCookieHandler>();
app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
