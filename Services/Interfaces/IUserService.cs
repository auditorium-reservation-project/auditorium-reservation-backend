﻿using AuditoriumReservation.DAL.Enumerations;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.Services.ActionResult;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> GetProfile(Guid id);
        Task<UserDto> UpdateProfile(Guid id, UpdateUserDto userDto);
        Task<UserDto> Register(CreateUserDto userDto);
        Task DeleteProfile(Guid id);
        Task AddRelatedGroups(ICollection<Guid> groupIds);
        Task RemoveRelatedGroups(ICollection<Guid> groupIds);
        Task<RoleApplicationDto> ApplyForRole(Guid userId, Role role);
        Task DeleteAll();
    }
}