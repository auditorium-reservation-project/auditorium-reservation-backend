﻿using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IProfessorService
    {
        Task<ICollection<ProfessorDto>> GetProfessors();
        Task<ProfessorDto> GetProfessor(Guid id);
        Task<ProfessorDto> GetProfessorByLinkedUserId(Guid id);
        Task<ProfessorDto> CreateProfessor(CreateUpdateProfessorDto professorDto);
        Task<ProfessorDto> UpdateProfessor(Guid id, CreateUpdateProfessorDto professorDto);
        Task DeleteProfessor(Guid id);
        Task DeleteAll();
    }
}