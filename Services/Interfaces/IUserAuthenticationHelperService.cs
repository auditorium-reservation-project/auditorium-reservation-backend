﻿using AuditoriumReservation.DAL.Entities;
using System.Security.Claims;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IUserAuthenticationHelperService
    {
        Task<User> GetUserFromPrincipal(ClaimsPrincipal principal);
        Task<User?> GetUserFromPrincipalNoThrow(ClaimsPrincipal principal);
    }
}