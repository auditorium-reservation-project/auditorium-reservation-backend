﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.Enumerations;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IApplicationsService
    {
        Task<ICollection<RoleApplicationDto>> GetRoleApplications();
        Task<RoleApplicationDto> Approve(Guid id);
        Task<RoleApplicationDto> Reject(Guid id);
        Task<RoleApplicationDto> Apply(User user, Role role);
    }
}