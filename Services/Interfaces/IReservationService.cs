﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.DTO.ScheduleDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IReservationService
    {
        Task<ICollection<ReservationDto>> GetReservations(GetReservationsRequestDto dto);
        Task<ReservationDto> GetReservation(Guid id);
        Task<ReservationDto> CreateReservation(User user, CreateReservationDto dto);
        Task<ReservationDto> UpdateReservation(User user, Guid id, UpdateReservationDto reservationDto);
        Task<ReservationCancellationDto> CancelReservation(Guid id, CancellationReason reason = CancellationReason.Cancelled);
        Task CancelReservation(Reservation reservation,
            CancellationReason reason,
            Class conflictingClass = null);
        Task<ReservationDto> ApproveReservation(Guid id);
        Task CancelReservations(ICollection<Reservation> reservations, CancellationReason reason);
        Task<ICollection<ReservationDto>> GetUnapproved();
        Task<ScheduleDto> GetForAuditorium(Guid id, DateOnly startDate, DateOnly endDate);
    }
}
