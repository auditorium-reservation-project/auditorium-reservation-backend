﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.DTO.ScheduleDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IScheduleService
    {
        Task<ClassDto> CreateClass(CreateClassDto classDto);
        Task<ClassDto> GetClass(Guid id);
        Task<ClassDto> UpdateClass(Guid id, UpdateClassDto classDto);
        Task DeleteClass(Guid id);
        Task<ScheduleDto> GetSchedule(GetClassesRequestDto dto);        
        Task<ICollection<TimeSlotDto>> GetAllTimeSlots();
        Task DeleteClasses(DateOnly? startDate = null, DateOnly? endDate = null, Guid? groupId = null);
        Task SyncAddClasses(ICollection<Class> classes);
    }
}