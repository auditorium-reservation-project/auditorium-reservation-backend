﻿using AuditoriumReservation.DTO.CreateUpdate;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IStudentGroupService
    {
        Task<ICollection<GroupDto>> GetAllGroups(bool includeFaculties);
        Task<GroupDto> AddStudentsToGroup(Guid groupId, List<Guid> studentIds);
        Task<GroupDto> GetGroup(Guid id);
        Task<GroupDto> CreateGroup(CreateGroupDto groupDto);
        Task<GroupDto> UpdateGroup(Guid id, UpdateGroupDto groupDto);
        Task DeleteGroup(Guid id);
        Task DeleteAll();
    }
}
