﻿using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IFacultyService
    {
        Task<ICollection<FacultyDto>> GetFaculties(bool includeGroups = false);
        Task<FacultyDto> GetFaculty(Guid id);
        Task<FacultyDto> CreateFaculty(CreateUpdateFacultyDto facultyDto);
        Task<FacultyDto> UpdateFaculty(Guid id, CreateUpdateFacultyDto facultyDto);
        Task DeleteFaculty(Guid facultyId);
        Task DeleteAll();
    }
}