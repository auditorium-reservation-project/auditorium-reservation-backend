﻿using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface ISubjectService
    {
        Task<ICollection<SubjectDto>> GetAllSubjects();
        Task<SubjectDto> GetSubject(Guid id);
        Task<SubjectDto> CreateSubject(CreateUpdateSubjectDto subjectDto);
        Task<SubjectDto> UpdateSubject(Guid id, CreateUpdateSubjectDto subjectDto);
        Task DeleteSubject(Guid id);
        Task DeleteAll();
    }
}
