﻿using System.Security.Claims;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface ITokenService
    {
        string GetAccessToken(ICollection<Claim> claims);
    }
}
