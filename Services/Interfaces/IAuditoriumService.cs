﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IAuditoriumService
    {
        Task<ICollection<AuditoriumDto>> GetAuditoria();
        Task<AuditoriumDto> GetAuditorium(Guid id);
        Task<AuditoriumDto> CreateAuditorium(CreateAuditoriumDto auditoriumDto);
        Task<AuditoriumDto> UpdateAuditorium(Guid id, UpdateAuditoriumDto auditoriumDto);
        Task DeleteAuditorium(Guid id);
        Task DeleteAll();
        Task<ICollection<AuditoriumDto>> GetUnoccupiedAuditoria(DateOnly date, int timeSlotIndex, Guid? buildingId = null);
    }
}