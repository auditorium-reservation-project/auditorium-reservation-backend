﻿using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IBuildingService
    {
        Task<ICollection<BuildingDto>> GetAllBuildings(bool includeAuditoria);
        Task<ICollection<AuditoriumDto>> GetAllAuditoria(Guid buildingId);
        Task<AuditoriumDto> GetAuditorium(Guid buildingId, Guid auditoriumId);
        Task<BuildingDto> GetBuilding(Guid buildingId);
        Task<BuildingDto> CreateBuilding(CreateUpdateBuildingDto buildingDto);
        Task<BuildingDto> UpdateBuilding(Guid buildingId, CreateUpdateBuildingDto buildingDto);
        Task DeleteBuilding(Guid buildingId);
        Task DeleteAll();
    }
}
