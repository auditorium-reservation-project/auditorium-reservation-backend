﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Services.ActionResult;

namespace AuditoriumReservation.Services.Interfaces
{
    public interface IAuthenticationService
    {
        Task<User> Login(LoginDto dto);
    }
}
