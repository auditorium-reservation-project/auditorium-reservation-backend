﻿using AuditoriumReservation.Controllers;
using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.Enumerations;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class ApplicationsService : IApplicationsService
    {
        private readonly ReservationDbContext _dbContext;
        public ApplicationsService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<RoleApplicationDto> Apply(User user, Role role)
        {
            var appl = new RoleApplication() { DateTimeOfApplication = DateTime.Now, User = user, Role = UserConverter.MatchDtoRoleToRole(role) };
            _dbContext.RoleApplications.Add(appl);
            await _dbContext.SaveChangesAsync();
            return appl.ToDto();
        }

        public async Task<RoleApplicationDto> Approve(Guid id)
        {
            var appl = await _dbContext.RoleApplications
                .Include(x => x.User)
                .SingleOrDefaultAsync(x => x.Id == id)
                    ?? throw new NotFoundException<RoleApplication>(id);

            appl.User.Roles.Add(appl.Role);

            _dbContext.RoleApplications.Remove(appl);
            await _dbContext.SaveChangesAsync();
            return appl.ToDto();
        }

        public async Task<ICollection<RoleApplicationDto>> GetRoleApplications()
        {
            return await _dbContext.RoleApplications
                .Include(x => x.User)
                .Select(x=>x.ToDto())
                .ToListAsync();
        }

        public async Task<RoleApplicationDto> Reject(Guid id)
        {
            var appl = await _dbContext.RoleApplications
                .Include(x => x.User)
                .SingleOrDefaultAsync(x => x.Id == id)
                    ?? throw new NotFoundException<RoleApplication>(id);

            _dbContext.RoleApplications.Remove(appl);
            await _dbContext.SaveChangesAsync();
            return appl.ToDto();
        }
    }
}
