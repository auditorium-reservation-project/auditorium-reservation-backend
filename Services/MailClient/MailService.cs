﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using AuditoriumReservation.Configuration;
using Microsoft.Extensions.Options;
using AuditoriumReservation.DAL.Entities;
using System.Globalization;
using AuditoriumReservation.DTO.ScheduleDto;
using AuditoriumReservation.DTO.EntityDto;
using System.Text.Json;

namespace AuditoriumReservation.Services.MailClient
{
    public class MailService : IMailService
    {
        private readonly SmtpClientConfiguration _config;
        public MailService(IOptions<SmtpClientConfiguration> config)
        {
            _config = config.Value;
        }

        public async Task NotifyOnReservationCancel(User user, ReservationCancellationDto dto)
        {
            var dateString = dto.Reservation.Created.ToShortDateString();
            var subject = $"Your reservation on {dateString} has been cancelled";
            var message = $"Your reservation on {dateString} has been cancelled {ParseCancellationReason(dto)}\n" +
                $"Reservation details: " +
                $"{ParseReservationDetails(dto.Reservation)}";

            await SendEmailAsync(user.Email, subject, message);
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            using var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(_config.SenderName, _config.SenderAddress));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Plain)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_config.SmtpHost, _config.SmtpPort, false);
                await client.AuthenticateAsync(_config.ClientLogin, _config.ClientPassword);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }

        private string ParseCancellationReason(ReservationCancellationDto res) =>
        res.Reason switch
        {
            CancellationReason.Cancelled => "",
            //CancellationReason.ProfessorBusy => " due to " + res.Reservation.Professor.FullName + " being busy",
            CancellationReason.AuditoriumOccupied => " due to auditorium " + res.Reservation.Auditorium.Name + " being occupied",
            //CancellationReason.StudentGroupBusy => " due to group " + res.Reservation.Group.Name + " being busy",
            CancellationReason.Other => "",
            _ => " due to an unknown reason"
        };

        private string ParseReservationDetails(ReservationDto reservation)
        {
            return JsonSerializer.Serialize(reservation);
            //throw new NotImplementedException("Update reservation details and dto");
        }
    }
}
