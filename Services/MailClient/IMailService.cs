﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.ScheduleDto;

namespace AuditoriumReservation.Services.MailClient
{
    public interface IMailService
    {
        Task NotifyOnReservationCancel(User user, ReservationCancellationDto dto);
        Task SendEmailAsync(string email, string subject, string message);
    }
}