﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class BuildingService : IBuildingService
    {
        private readonly ReservationDbContext _dbContext;
        public BuildingService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<BuildingDto> CreateBuilding(CreateUpdateBuildingDto buildingDto)
        {
            var building = new Building();
            if (buildingDto.Name is not null)
            {
                building.Name = buildingDto.Name;
            }

            await _dbContext.Buildings.AddAsync(building);
            await _dbContext.SaveChangesAsync();
            return BuildingConverter.FromBuilding(building);
        }
        public async Task<BuildingDto> GetBuilding(Guid id)
        {
            var building = await RetrieveBuildingEntity(id);

            return BuildingConverter.FromBuildingFull(building);
        }
        public async Task<BuildingDto> UpdateBuilding(Guid id, CreateUpdateBuildingDto buildingDto)
        {
            var building = await RetrieveBuildingEntity(id);

            if (buildingDto.Name is not null)
            {
                building.Name = buildingDto.Name;
            }

            await _dbContext.SaveChangesAsync();

            return BuildingConverter.FromBuildingFull(building);
        }
        public async Task DeleteBuilding(Guid id)
        {
            var building = await RetrieveBuildingEntity(id);
            _dbContext.Buildings.Remove(building);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<AuditoriumDto>> GetAllAuditoria(Guid buildingId)
        {
            var building = await RetrieveBuildingEntity(buildingId);
            return building.Auditoria.Select(x => AuditoriumConverter.FromAuditorum(x)).ToList();
        }

        public async Task<ICollection<BuildingDto>> GetAllBuildings(bool includeAuditoria)
        {
            IQueryable<Building> buildings = _dbContext.Buildings;
            if (includeAuditoria)
            {
                return await buildings.Include(x => x.Auditoria)
                    .OrderBy(x => x.Name)
                    .Select(x => BuildingConverter.FromBuildingFull(x))
                    .ToListAsync();
            }

            return await buildings.OrderBy(x => x.Name)
                .Select(x => BuildingConverter.FromBuilding(x))
                .ToListAsync();
        }

        public async Task<AuditoriumDto> GetAuditorium(Guid buildingId, Guid auditoriumId)
        {
            var building = await _dbContext.Buildings.Include(x => x.Auditoria)
                //.ThenInclude(a => a.Building)
                .Where(x => x.Id == buildingId)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException<Building>(buildingId);


            var auditorium = building.Auditoria.ToList()
                .Find(x => x.Id == auditoriumId)
                    ?? throw new NotFoundException<Auditorium>(auditoriumId);

            return AuditoriumConverter.FromAuditoriumFull(auditorium);
        }

        private async Task<Building> RetrieveBuildingEntity(Guid id)
        {
            return await _dbContext.Buildings
                .Include(x => x.Auditoria)
                .SingleOrDefaultAsync(x => x.Id == id)
                    ?? throw new NotFoundException<Building>(id);
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Buildings.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }
    }
}
