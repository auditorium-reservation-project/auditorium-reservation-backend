﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class ProfessorService : IProfessorService
    {
        private readonly ReservationDbContext _dbContext;
        public ProfessorService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ICollection<ProfessorDto>> GetProfessors()
        {
            return await _dbContext.Professors
                .OrderBy(x => x.Name)
                .Select(x => ProfessorConverter.FromProfessor(x))
                .ToListAsync();
        }

        public async Task<ProfessorDto> GetProfessor(Guid id)
        {
            var professor = await _dbContext.Professors.FindAsync(id)
                ?? throw new NotFoundException<Professor>(id);

            return ProfessorConverter.FromProfessor(professor);
        }

        public async Task<ProfessorDto> CreateProfessor(CreateUpdateProfessorDto professorDto)
        {
            var professor = new Professor();

            if (professorDto.FullName is not null)
            {
                professor.Name = professorDto.FullName;
            }

            if (professorDto.LinkedUser is not null)
            {
                professor.LinkedUser = await _dbContext.Users.FindAsync(professorDto.LinkedUser)
                    ?? throw new NotFoundException("Referenced User was not found");
            }

            await _dbContext.Professors.AddAsync(professor);
            await _dbContext.SaveChangesAsync();

            return ProfessorConverter.FromProfessor(professor);
        }

        public async Task<ProfessorDto> UpdateProfessor(Guid id, CreateUpdateProfessorDto professorDto)
        {
            var professor = await _dbContext.Professors.FindAsync(id)
                ?? throw new NotFoundException<Professor>(id);

            if (professorDto.FullName is not null)
            {
                professor.Name = professorDto.FullName;
            }

            if (professorDto.LinkedUser is not null)
            {
                professor.LinkedUser = await _dbContext.Users.SingleOrDefaultAsync()
                    ?? throw new NotFoundException("Referenced User was not found");
            }

            await _dbContext.SaveChangesAsync();
            return ProfessorConverter.FromProfessor(professor);
        }

        public async Task DeleteProfessor(Guid id)
        {
            var professor = await _dbContext.Professors.FindAsync(id)
                ?? throw new NotFoundException<Professor>(id);
            _dbContext.Remove(professor);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ProfessorDto> GetProfessorByLinkedUserId(Guid id)
        {
            var professor = await _dbContext.Professors
                .Where(x => x.LinkedUser != null && x.LinkedUser.Id == id)
                .Include(x => x.LinkedUser)
                .Include(x => x.Faculties)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException("User is not linked to any Professor");

            return ProfessorConverter.FromProfessorFull(professor);
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Professors.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }
    }
}
