﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.CreateUpdate;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class StudentGroupService : IStudentGroupService
    {
        private readonly ReservationDbContext _dbContext;
        public StudentGroupService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GroupDto> AddStudentsToGroup(Guid groupId, List<Guid> studentIds)
        {
            throw new NotImplementedException();
        }
        public async Task<ICollection<GroupDto>> GetAllGroups(bool includeFaculties)
        {
            var groups = _dbContext.StudentGroups;
            if(includeFaculties)
            {
                return await groups.Include(x => x.Faculty)
                    .Select(x => StudentGroupConverter.FromGroupFull(x))
                    .ToListAsync();
            }
            return await groups.Select(x => StudentGroupConverter.FromGroup(x)).ToListAsync();
        }

        public async Task<GroupDto> GetGroup(Guid id)
        {
            var group = await _dbContext.StudentGroups.Include(x => x.Faculty)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException<StudentGroup>(id);

            return StudentGroupConverter.FromGroupFull(group);
        }

        public async Task<GroupDto> CreateGroup(CreateGroupDto groupDto)
        {
            var faculty = await _dbContext.Faculties.FindAsync(groupDto.FacultyId)
                ?? throw new NotFoundException<Faculty>(groupDto.FacultyId);

            var group = new StudentGroup() { Faculty = faculty };
            if(groupDto.Name is not null)
            {
                group.Name = groupDto.Name;
            }


            group.Faculty = faculty;

            await _dbContext.StudentGroups.AddAsync(group);
            await _dbContext.SaveChangesAsync();

            return StudentGroupConverter.FromGroupFull(group);
        }
        public async Task<GroupDto> UpdateGroup(Guid id, UpdateGroupDto groupDto)
        {
            var group = new StudentGroup();

            group.Name = groupDto.Name ?? group.Name;

            if (groupDto.FacultyId is not null)
            {
                var faculty = await _dbContext.Faculties.FindAsync(groupDto.FacultyId)
                    ?? throw new NotFoundException("Referenced Faculty does not exist");

                group.Faculty = faculty;
            }

            await _dbContext.SaveChangesAsync();

            return StudentGroupConverter.FromGroupFull(group);
        }

        public async Task DeleteGroup(Guid id)
        {
            var group = await _dbContext.StudentGroups.FindAsync(id)
                ?? throw new NotFoundException<StudentGroup>(id);
            _dbContext.Remove(group);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.StudentGroups.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }
    }
}
