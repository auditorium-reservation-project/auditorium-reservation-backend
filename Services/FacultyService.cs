﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DAL;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using Microsoft.EntityFrameworkCore;
using AuditoriumReservation.Services.Interfaces;
using System.Text.RegularExpressions;

namespace AuditoriumReservation.Services
{
    public class FacultyService : IFacultyService
    {
        private readonly ReservationDbContext _dbContext;
        public FacultyService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<FacultyDto> CreateFaculty(CreateUpdateFacultyDto dto)
        {
            var faculty = new Faculty();
            if (dto.Name is not null)
            {
                faculty.Name = dto.Name;
            }

            if (dto.GroupIds is not null)
            {
                faculty.StudentGroups = await GetGroupsFromDtoList(dto.GroupIds);
            }

            if(dto.ProfessorIds is not null)
            {
                faculty.Professors = await GetProfessorsFromDtoList(dto.ProfessorIds);
            }

            await _dbContext.Faculties.AddAsync(faculty);
            await _dbContext.SaveChangesAsync();

            return FacultyConverter.FromFacultyFull(faculty);
        }

        public async Task<ICollection<FacultyDto>> GetFaculties(bool includeGroups = false)
        {
            if (includeGroups)
            {
                return await _dbContext.Faculties
                    .Include(x => x.StudentGroups)
                    .OrderBy(x => x.Name)
                    .Select(x => FacultyConverter.FromFacultyFull(x))
                    .ToListAsync();
            }
            return await _dbContext.Faculties.Select(f => FacultyConverter.FromFaculty(f)).ToListAsync();
        }

        public async Task<FacultyDto> GetFaculty(Guid id)
        {
            var faculty = await _dbContext.Faculties.Include(f => f.Professors)
                .Include(f => f.StudentGroups)
                .Include(f => f.Professors)
                .FirstOrDefaultAsync(x => x.Id.Equals(id))
                    ?? throw new NotFoundException<Faculty>(id);

            return FacultyConverter.FromFacultyFull(faculty);
        }

        public async Task<FacultyDto> UpdateFaculty(Guid id, CreateUpdateFacultyDto dto)
        {
            var faculty = await _dbContext.Faculties
                .Include(x => x.Professors)
                .Include(x => x.StudentGroups)
                .SingleOrDefaultAsync(x => x.Id == id)
                    ?? throw new NotFoundException<Faculty>(id);

            if(dto.Name is not null)
            {
                faculty.Name = dto.Name;
            }

            if (dto.GroupIds is not null)
            {
                faculty.StudentGroups = await GetGroupsFromDtoList(dto.GroupIds);
            }

            if (dto.ProfessorIds is not null)
            {
                faculty.Professors = await GetProfessorsFromDtoList(dto.ProfessorIds);
            }

            await _dbContext.SaveChangesAsync();

            return FacultyConverter.FromFacultyFull(faculty);
        }

        public async Task DeleteFaculty(Guid id)
        {
            var faculty = await _dbContext.Faculties.FindAsync(id)
                ?? throw new NotFoundException<Faculty>(id);
            _dbContext.Faculties.Remove(faculty);
            await _dbContext.SaveChangesAsync();
        }

        private async Task<ICollection<StudentGroup>> GetGroupsFromDtoList(ICollection<Guid> ids)
        {
            var groups = await _dbContext.StudentGroups
                    .Where(x => ids.Contains(x.Id))
                    .ToListAsync();
            if (groups.Count != ids.Count)
            {
                var invalidGroupIds = ids
                    .Where(x => groups.All(g => g.Id != x));
                throw new NotFoundException<StudentGroup>(invalidGroupIds);
            }
            return groups;
        }

        private async Task<ICollection<Professor>> GetProfessorsFromDtoList(ICollection<Guid> ids)
        {
            var professors = await _dbContext.Professors
                    .Where(x => ids.Contains(x.Id))
                    .ToListAsync();
            if (professors.Count != ids.Count)
            {
                var invalidProfessorIds = ids
                    .Where(x => professors.All(p => p.Id != x));
                throw new NotFoundException<Professor>(invalidProfessorIds);
            }

            return professors;
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Faculties.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }
    }
}