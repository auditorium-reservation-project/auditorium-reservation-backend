﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class AuditoriumService : IAuditoriumService
    {
        private readonly ReservationDbContext _dbContext;
        public AuditoriumService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ICollection<AuditoriumDto>> GetAuditoria()
        {
            return await _dbContext.Auditoria
                .Select(x => AuditoriumConverter.FromAuditorum(x))
                .ToListAsync();
        }

        public async Task<AuditoriumDto> GetAuditorium(Guid id)
        {
            var auditorium = await _dbContext.Auditoria
                    .Include(x => x.Building)
                    .SingleOrDefaultAsync(x => x.Id == id)
                ?? throw new NotFoundException<Auditorium>(id);

            return AuditoriumConverter.FromAuditoriumFull(auditorium);
        }

        public async Task<AuditoriumDto> CreateAuditorium(CreateAuditoriumDto auditoriumDto)
        {
            var auditorium = new Auditorium() { Name = auditoriumDto.Name };
            var parentBuilding = await _dbContext.Buildings.FindAsync(auditoriumDto.BuildingId)
                ?? throw new NotFoundException<Building>(auditoriumDto.BuildingId);

            auditorium.Building = parentBuilding;

            await _dbContext.AddAsync(auditorium);
            await _dbContext.SaveChangesAsync();

            return AuditoriumConverter.FromAuditoriumFull(auditorium);
        }

        public async Task<AuditoriumDto> UpdateAuditorium(Guid id, UpdateAuditoriumDto auditoriumDto)
        {
            var auditorium = await _dbContext.Auditoria
                .Include(x => x.Building)
                .SingleOrDefaultAsync(x => x.Id == id)
                ?? throw new NotFoundException<Auditorium>(id);

            if (auditoriumDto.Name is not null)
            {
                auditorium.Name = auditoriumDto.Name;
            }

            if (auditoriumDto.BuildingId is not null)
            {
                var parentBuilding = await _dbContext.Buildings.FindAsync(auditoriumDto.BuildingId)
                    ?? throw new NotFoundException<Building>(auditoriumDto.BuildingId);
                auditorium.Building = parentBuilding;
            }

            await _dbContext.SaveChangesAsync();

            return AuditoriumConverter.FromAuditoriumFull(auditorium);
        }

        public async Task DeleteAuditorium(Guid id)
        {
            var auditorium = await _dbContext.Auditoria.FindAsync(id)
                ?? throw new NotFoundException<Auditorium>(id);
            _dbContext.Auditoria.Remove(auditorium);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Auditoria.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<AuditoriumDto>> GetUnoccupiedAuditoria(DateOnly date, int timeSlotIndex, Guid? buildingId = null)
        {
            var timeSlot = await _dbContext.TimeSlots
                .SingleOrDefaultAsync(x => x.Index == timeSlotIndex)
                    ?? throw new NotFoundException("Referenced Time Slot index is invalid");

            var occupiedAuditoriaQuery = _dbContext.Classes
                    .Include(x => x.Auditorium)
                    .Include(x => x.TimeSlot)
                    .Where(x => x.Date == date)
                    .Where(x => x.TimeSlot.Index == timeSlotIndex);
            IQueryable<Auditorium> auditoriaQuery = _dbContext.Auditoria;

            if (buildingId is not null)
            {
                var building = await _dbContext.Buildings.FindAsync(buildingId)
                    ?? throw new NotFoundException<Building>(buildingId);

                occupiedAuditoriaQuery = occupiedAuditoriaQuery
                    .Include(x => x.Auditorium)
                    .ThenInclude(a => a.Building)
                    .Where(x => x.Auditorium.Building.Id == buildingId);

                auditoriaQuery = auditoriaQuery
                    .Include(x => x.Building)
                    .Where(x => x.Building.Id == buildingId);
            }

            var occupiedIds = await occupiedAuditoriaQuery
                .Select(x => x.Id)
                .ToListAsync();

            return await auditoriaQuery
                    .Where(x => occupiedIds.Contains(x.Id) == false)
                    .Select(x => AuditoriumConverter.FromAuditorum(x))
                    .ToListAsync();
        }
    }
}
