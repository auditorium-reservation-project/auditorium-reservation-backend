﻿using AuditoriumReservation.DAL.Entities;

namespace AuditoriumReservation.Services.Schedule
{
    public class ScheduleConflict { }
    public class ClassConcurrencyConflict : ScheduleConflict
    {
        public DateOnly Date;
        public TimeSlot TimeSlot;
        public Class Occupant;
        public ConcurrencyConflictType ConflictType;
        public ClassConcurrencyConflict(Class conflictingClass,
            ConcurrencyConflictType conflictType,
            DateOnly? date = null,
            TimeSlot? timeSlot = null)
        {
            this.Date = date ?? conflictingClass.Date;
            this.TimeSlot = timeSlot ?? conflictingClass.TimeSlot;
            this.ConflictType = conflictType;
            Occupant = conflictingClass;
        }
    }

    public enum ConcurrencyConflictType
    {
        StudentGroupBusy,
        ProfessorBusy,
        AuditoriumOccupied,
        Other
    }

    //public class StudentGroupBusy : ClassConcurrenceConflict
    //{
    //    public StudentGroup Group;
    //    public StudentGroupBusy(Class @class, StudentGroup? group = null) : base(@class)
    //    {
    //        this.Group = group ?? @class.StudentGroup;
    //    }
    //}

    //public class ProfessorBusy : ClassConcurrenceConflict
    //{
    //    public Professor Professor;
    //    public ProfessorBusy(Class @class, Professor? professor = null) : base(@class)
    //    {
    //        Professor = professor ?? @class.Professor;
    //    }
    //}

    //public class AuditoriumOccupied : ClassConcurrenceConflict
    //{
    //    public Auditorium Auditorium;
    //    public AuditoriumOccupied(Class @class, Auditorium? auditorium = null) : base(@class)
    //    {
    //        Auditorium = auditorium ?? @class.Auditorium;
    //    }
    //}
}
