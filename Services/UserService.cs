﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Converters;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DAL.Enumerations;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.ActionResult;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class UserService : IUserService
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IPasswordHasher<User> _passwordHasher;
        public UserService(ReservationDbContext dbContext, IPasswordHasher<User> passwordHasher)
        {
            _dbContext = dbContext;
            _passwordHasher = passwordHasher;
        }

        public async Task DeleteProfile(Guid userId)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id.Equals(userId));

            if (user is null)
            {
                throw new Exception("User not found");
            }

            _dbContext.Remove(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<UserDto> GetProfile(Guid userId)
        {
            var user = await _dbContext.Users
                .Include(x => x.RelatedGroups)
                .Include(x => x.RoleApplications)
                .SingleOrDefaultAsync(x => x.Id == userId)
                    ?? throw new NotFoundException<User>(userId);
            
            return UserConverter.FromUser(user);
        }

        public async Task<ServiceActionResult<UserDto>> GetProfile(string Email)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync<User>(x => x.Email.Equals(Email));
            if (user is null)
            {
                return new ServiceActionResult<UserDto>(new NotFoundException<User>(Email));
            }
            return new ServiceActionResult<UserDto>(UserConverter.FromUser(user));
        }

        public async Task<UserDto> Register(CreateUserDto dto)
        {
            var exsistingUser = await _dbContext.Users.FirstOrDefaultAsync(x => x.Email == dto.Email);
            if (exsistingUser is not null)
            {
                throw new AlreadyExistsException("Email address already taken");
            }

            var user = new User() { FirstName = dto.FirstName };

            user.MiddleName = dto.MiddleName ?? user.MiddleName;
            user.LastName = dto.LastName ?? user.LastName;
            user.Email = dto.Email;
            user.AvatarUrl = dto.AvatarUrl ?? user.AvatarUrl;

            user.Password = _passwordHasher.HashPassword(user, dto.Password);

            await _dbContext.Users.AddAsync(user);

            var roleApplications = new List<RoleApplication>();
            if (dto.Roles is not null)
            {
                foreach (var role in dto.Roles.Select(UserConverter.MatchDtoRoleToRole))
                {
                    if(user.Roles.Contains(role) || roleApplications.Any(x =>  x.Role == role))
                    {
                        continue;
                    }
                    roleApplications.Add(new RoleApplication()
                    {
                        User = user,
                        Role = role
                    });
                }
            }

            await _dbContext.RoleApplications.AddRangeAsync(roleApplications);
            await _dbContext.SaveChangesAsync();

            return UserConverter.FromUser(user);
        }

        public async Task<UserDto> UpdateProfile(Guid userId, UpdateUserDto updateDto)
        {
            var user = await _dbContext.Users.SingleOrDefaultAsync(x => x.Id == userId)
                ?? throw new NotFoundException<User>(userId);

            user.FirstName = updateDto.FirstName ?? user.FirstName;
            user.LastName = updateDto.LastName ?? user.LastName;
            user.MiddleName = updateDto.MiddleName ?? user.MiddleName;
            user.AvatarUrl = updateDto.AvatarUrl ?? user.AvatarUrl;
            
            if(updateDto.Email is not null)
            {
                var sameEmail = await _dbContext.Users.FirstOrDefaultAsync(x => x.Email ==  updateDto.Email);
                if(sameEmail is not null)
                {
                    throw new AlreadyExistsException("Email taken");
                }

                user.Email = updateDto.Email;
            }

            if(updateDto.Password is not null)
            {
                user.Password = _passwordHasher.HashPassword(user, updateDto.Password);
            }

            await _dbContext.SaveChangesAsync();

            return UserConverter.FromUser(user);
        }

        public async Task AddRelatedGroups(ICollection<Guid> groupIds)
        {
            throw new NotImplementedException();
        }

        public async Task RemoveRelatedGroups(ICollection<Guid> groupIds)
        {
            throw new NotImplementedException();
        }

        public async Task<RoleApplicationDto> ApplyForRole(Guid userId, Role role)
        {
            var user = await _dbContext.Users.Include(x => x.RoleApplications)
                .Where(x => x.Id == userId)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException("Failed to apply for role: user not found");

            if (user.RoleApplications.Where(x => x.Role == role).Count() > 0)
            {
                throw new AlreadyExistsException($"Role application for {UserConverter.MatchRoleToDtoRole(role)} already exists for this user");
            }

            var application = new RoleApplication()
            {
                User = user,
                Role = role
            };

            await _dbContext.RoleApplications.AddAsync(application);
            await _dbContext.SaveChangesAsync();

            return application.ToDto();
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Users.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }
    }
}
