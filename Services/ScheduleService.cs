﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.Enumerations;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.DTO.ScheduleDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Services.MailClient;
using AuditoriumReservation.Services.Schedule;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using MimeKit.Cryptography;
using System.ComponentModel;
using System.Globalization;
using System.Security.Claims;
using System.Xml.Linq;

namespace AuditoriumReservation.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IUserService _userService;
        private readonly IMailService _mailService;
        private readonly IQueryable<Class> _classes;
        private readonly IQueryable<TimeSlot> _timeSlots;
        private readonly IReservationService _reservationService;

        public ScheduleService(ReservationDbContext context,
            IUserService userService,
            IMailService mailService,
            IReservationService reservationService)
        {
            _dbContext = context;
            _userService = userService;
            _mailService = mailService;
            _reservationService = reservationService;
            _classes = _dbContext.Classes
                .Include(x => x.Professor)
                .Include(x => x.StudentGroups)
                .Include(x => x.Subject)
                .Include(x => x.Auditorium).ThenInclude(a => a.Building)
                .Include(x => x.TimeSlot);
            _timeSlots = _dbContext.TimeSlots;
        }

        public async Task<ICollection<TimeSlotDto>> GetAllTimeSlots()
        {
            return await _dbContext.TimeSlots
                .Select(x => TimeSlotConverter.FromTimeSlot(x))
                .ToListAsync();
        }
        public async Task<ScheduleDto> GetSchedule(GetClassesRequestDto dto)
        {
            if (dto.StartDate.CompareTo(dto.EndDate) > 0)
            {
                throw new InvalidModelStateException<GetClassesRequestDto>("Unable to retrieve schedule: requested Start Date must not be later than End Date");
            }
            return dto.Type switch
            {
                ScheduleType.StudentGroup => await GetStudentGroupSchedule(dto),
                ScheduleType.Professor => await GetProfessorSchedule(dto),
                ScheduleType.Auditorium => await GetAuditoriumSchedule(dto),
                _ => throw new ReservationBaseException($"Invalid {typeof(ScheduleType).Name} while retrieving schedule")
            };
        }
        private async Task<ScheduleDto> GetStudentGroupSchedule(GetClassesRequestDto dto)
        {
            if (!Guid.TryParse(dto.RequestForEntityId, out var groupId))
            {
                throw new InvalidModelStateException<GetClassesRequestDto>("Invalid Id: Expected type is GUID");
            }

            var group = await _dbContext.StudentGroups.FindAsync(groupId)
                ?? throw new NotFoundException("Requested student group does not exist");


            var classes = await _classes
                .Where(x => x.Date >= dto.StartDate && x.Date <= dto.EndDate)
                .Where(x => x.StudentGroups.Any(x => x.Id == groupId))
                .OrderBy(x => x.Date).ThenBy(x => x.TimeSlot.Index)
                //.Select(x => ClassConverter.FromClass(x))
                .ToListAsync();            

            return await GenerateSchedule(classes, dto.StartDate, dto.EndDate);
        }
        private async Task<ScheduleDto> GetProfessorSchedule(GetClassesRequestDto dto)
        {
            if (!Guid.TryParse(dto.RequestForEntityId, out var professorId))
            {
                throw new InvalidModelStateException<GetClassesRequestDto>("Invalid Id: Expected type is GUID");
            }
            var professor = await _dbContext.Professors.FindAsync(professorId)
                    ?? throw new NotFoundException<Professor>(professorId);

            var classes = await _classes
                .Where(x => x.Date >= dto.StartDate && x.Date <= dto.EndDate)
                .Where(x => x.Professor.Id == professorId)
                .OrderBy(x => x.Date).ThenBy(x => x.TimeSlot.Index)
                .ToListAsync();

            return await GenerateSchedule(classes, dto.StartDate, dto.EndDate);
        }
        private async Task<ScheduleDto> GetAuditoriumSchedule(GetClassesRequestDto dto)
        {
            if (!Guid.TryParse(dto.RequestForEntityId, out var auditoriumId))
            {
                throw new InvalidModelStateException<GetClassesRequestDto>("Invalid Id: Expected type is GUID");
            }
            var auditorium = await _dbContext.Auditoria.FindAsync(auditoriumId)
                    ?? throw new NotFoundException<Auditorium>(auditoriumId);

            var classes = await _classes
                .Where(x => x.Date >= dto.StartDate && x.Date <= dto.EndDate)
                .Where(x => x.Auditorium.Id == auditoriumId)
                .OrderBy(x => x.Date).ThenBy(x => x.TimeSlot.Index)
                //.Select(x => ClassConverter.FromClass(x))
                .ToListAsync();

            if (dto.IncludeReservation)
            {
                var reservations = await _dbContext.Reservations
                    .Include(x => x.CreatedBy)
                    .Include(x => x.Auditorium)
                    .Include(x => x.TimeSlot)
                    .Where(x => x.Auditorium.Id == auditoriumId)
                    .ToListAsync();
                return await GenerateSchedule(classes, dto.StartDate, dto.EndDate, true, reservations);
            }

            return await GenerateSchedule(classes, dto.StartDate, dto.EndDate);
        }

        private async Task<ScheduleDto> GenerateSchedule(List<Class> classes, DateOnly startDate, DateOnly endDate, bool IncludeReservation = false, List<Reservation> reservations = null)
        {
            var days = DateRange(startDate.ToDateTime(TimeOnly.MinValue), endDate.ToDateTime(TimeOnly.MinValue))
                .Select(x => DateOnly.FromDateTime(x));

            var schedule = new ScheduleDto()
            {
                StartDate = startDate,
                EndDate = endDate,
                Days = new List<ScheduleDayDto>()
            };

            var classesIndex = 0;
            var timeSlots = await _timeSlots.ToListAsync();

            foreach (var date in days)
            {
                schedule.Days.Add(new ScheduleDayDto()
                {
                    Date = date,
                    Slots = GenerateScheduleSlots(classes, date, ref classesIndex, timeSlots)
                });

            }
            if (IncludeReservation)
            {
                throw new NotImplementedException();
            }
            return schedule;
        }

        private List<ScheduleSlotDto> GenerateScheduleSlots(List<Class> classes,
            DateOnly date,
            ref int classIndex,
            List<TimeSlot> timeSlots)
        {
            var slots = new List<ScheduleSlotDto>();
            if (classIndex >= classes.Count || classes[classIndex].Date > date)
            {
                return _timeSlots.Select(x => new ScheduleSlotDto()
                {
                    Type = ScheduleSlotType.Empty,
                    StartTime = x.StartTime,
                    EndTime = x.EndTime,
                    SlotIndex = x.Index
                }).ToList();
            }

            foreach (var timeSlot in timeSlots)
            {
                if (classIndex < classes.Count && classes[classIndex].Date == date)
                {
                    slots.Add(ScheduleConverter.SlotDtoFromClass(classes[classIndex]));
                    classIndex++;
                }
                else
                {
                    slots.Add(ScheduleConverter.SlotDtoFromTimeSlot(timeSlot));
                }
            }
            return slots;
        }



        public async Task<ClassDto> GetClass(Guid id)
        {
            return ClassConverter.FromClass(await GetClassNavigationIncluded(id));
        }
        private async Task<Class> GetClassNavigationIncluded(Guid id)
        {
            return await _dbContext.Classes
                .Include(x => x.Auditorium)
                .Include(x => x.Professor)
                .Include(x => x.StudentGroups)
                .Include(x => x.TimeSlot)
                .Include(x => x.Subject)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException<Class>(id);
        }

        public async Task<ClassDto> CreateClass(CreateClassDto dto)
        {
            var subject = await GetReferencedEntityById<Subject>(dto.SubjectId);
            var professor = await GetReferencedEntityById<Professor>(dto.ProfessorId);
            var auditorium = await GetReferencedEntityById<Auditorium>(dto.AuditoriumId);
            var timeSlot = await _dbContext.TimeSlots.Where(x => x.Index == dto.TimeSlotIndex)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException("Referenced time slot index is invalid");

            var groups = await _dbContext.StudentGroups
                .Where(x => dto.GroupIds.Contains(x.Id))
                .ToListAsync();

            if(groups.Count < dto.GroupIds.Count)
            {
                throw new NotFoundException("Unable to find all referenced student groups: verify selected groups are valid");
            }

            var @class = new Class()
            {
                Auditorium = auditorium,
                Subject = subject,
                Professor = professor,
                StudentGroups = groups,
                Date = dto.Date,
                TimeSlot = timeSlot,
                Type = ClassConverter.MatchDtoTypeToType(dto.Type)
            };

            var checkResult = await CheckClassConflicts(@class);
            if (checkResult.HasConflicts)
            {
                throw new ScheduleConflictException<ClassConcurrencyConflict>(checkResult.Conflicts);
            }

            await CancelOverlappingAppointments(@class);

            await _dbContext.AddAsync(@class);
            await _dbContext.SaveChangesAsync();

            return ClassConverter.FromClass(@class);
        }

        public async Task<ClassDto> UpdateClass(Guid id, UpdateClassDto classDto)
        {
            var @class = await GetClassNavigationIncluded(id);
            if (classDto.SubjectId is not null)
            {
                @class.Subject = await GetReferencedEntityById<Subject>(classDto.SubjectId.Value);
            }

            if (classDto.ProfessorId is not null)
            {
                @class.Professor = await GetReferencedEntityById<Professor>(classDto.ProfessorId.Value);
            }

            if (classDto.AuditoriumId is not null)
            {
                @class.Auditorium = await GetReferencedEntityById<Auditorium>(classDto.AuditoriumId.Value);
            }

            if (classDto.GroupIds is not null && classDto.GroupIds.Count > 0)
            {
                @class.StudentGroups = await _dbContext.StudentGroups.Where(x => classDto.GroupIds.Contains(x.Id)).ToListAsync();
                if(@class.StudentGroups.Count < classDto.GroupIds.Count)
                {
                    throw new NotFoundException("Failed to retrieve all referenced student groups: verify all grops are valid");
                }
            }

            if (classDto.Type is not null)
            {
                @class.Type = ClassConverter.MatchDtoTypeToType(classDto.Type.Value);
            }

            if (classDto.Date is not null)
            {
                @class.Date = classDto.Date.Value;
            }

            var checkResult = await CheckClassConflicts(@class);

            if (checkResult.HasConflicts)
            {
                throw new ScheduleConflictException<ClassConcurrencyConflict>(checkResult.Conflicts);
            }

            await CancelOverlappingAppointments(@class);

            await _dbContext.SaveChangesAsync();

            return ClassConverter.FromClass(@class);
        }

        public async Task DeleteClass(Guid id)
        {
            var @class = await _dbContext.Classes.FindAsync(id)
                ?? throw new NotFoundException<Class>(id);
            _dbContext.Classes.Remove(@class);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteClasses(DateOnly? startDate = null, DateOnly? endDate = null, Guid? groupId = null)
        {
            startDate = startDate ?? DateOnly.FromDateTime(DateTime.Now);
            endDate = endDate ?? startDate.Value.AddDays(1);

            var classesQuery = _classes.Where(x => x.Date >= startDate && x.Date <= endDate);

            if (groupId is not null)
            {
                classesQuery = classesQuery.Where(x => x.StudentGroups.Any(x => x.Id == groupId));
            }

            _dbContext.RemoveRange(await classesQuery.ToListAsync());
            await _dbContext.SaveChangesAsync();
        }

        public async Task SyncAddClasses(ICollection<Class> classes)
        {
            var overlappingReservations = new List<Reservation>();            

            foreach (var @class in classes)
            {
                var overlappingClasses = await _dbContext.Classes
                    .Where(x => x.Date == @class.Date && x.TimeSlot.Index == @class.TimeSlot.Index)
                    .ToListAsync();

                if(overlappingClasses.Count > 0)
                {
                    _dbContext.Classes.RemoveRange(overlappingClasses);
                }

                _dbContext.Classes.Add(@class);

                overlappingReservations.AddRange(
                    await _dbContext.Reservations
                        .Include(x => x.CreatedBy)
                        .Where(x => x.TimeSlot.Index == @class.TimeSlot.Index)
                        .Where(x => x.Date == @class.Date)
                        .Where(x => x.Auditorium == @class.Auditorium)
                        .ToListAsync());
            }

            await _reservationService.CancelReservations(overlappingReservations, CancellationReason.AuditoriumOccupied);

            await _dbContext.SaveChangesAsync();
        }

        private async Task<T> GetReferencedEntityById<T>(Guid id) where T : class
        {
            return await _dbContext.FindAsync<T>(id)
                ?? throw new NotFoundException<T>(id);
        }

        private async Task CancelOverlappingAppointments(Class @class)
        {
            var reservations = await _dbContext.Reservations
                .Include(x => x.TimeSlot)
                .Include(x => x.Auditorium)
                .Where(x => x.TimeSlot.Index == @class.TimeSlot.Index)
                .Where(x => x.Date == @class.Date)
                .Where(x => x.Auditorium == @class.Auditorium)
                .ToListAsync();

            foreach (var res in reservations)
            {
                await _reservationService.CancelReservation(res, CancellationReason.AuditoriumOccupied, @class);
            }
        }

        private async Task<(bool HasConflicts, List<ClassConcurrencyConflict> Conflicts)> CheckClassConflicts(Class @class)
        {
            bool hasConflicts = false;
            List<ClassConcurrencyConflict> conflicts = new List<ClassConcurrencyConflict>();

            var classes = _dbContext.Classes
                .Where(x => x.TimeSlot.Index == @class.TimeSlot.Index)
                .Where(x => x.Date == @class.Date)
                .Where(x => x.Id != @class.Id);

            if (classes.Count() <= 0)
            {
                return (false, conflicts);
            }

            var conflict = await classes
                .Where(x => x.StudentGroups.Any(x => @class.StudentGroups.Any(g => g.Id == x.Id)))
                .ToListAsync();

            if (conflict.Count > 0)
            {
                hasConflicts = true;
                conflict.ForEach(x =>
                    conflicts.Add(new ClassConcurrencyConflict(x, ConcurrencyConflictType.StudentGroupBusy)));
            }

            conflict = await classes
                .Where(x => x.Professor == @class.Professor)
                .ToListAsync();

            if (conflict.Count > 0)
            {
                hasConflicts = true;
                conflict.ForEach(x =>
                    conflicts.Add(new ClassConcurrencyConflict(x, ConcurrencyConflictType.ProfessorBusy)));
            }

            conflict = await classes
                .Where(x => x.Auditorium == @class.Auditorium)
                .ToListAsync();

            if (conflict.Count > 0)
            {
                hasConflicts = true;
                conflict.ForEach(x =>
                    conflicts.Add(new ClassConcurrencyConflict(x, ConcurrencyConflictType.AuditoriumOccupied)));
            }

            return (hasConflicts, conflicts);
        }

        private IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d));
        }

        private IEnumerable<DateTime> GetWeeks(DayOfWeek weekStart, DateTime startDate)
        {
            DateTime current = DateTime.Today.AddDays(weekStart - DateTime.Today.DayOfWeek);
            while (current >= startDate)
            {
                yield return current;

                // move to the previous week
                current = current.AddDays(-7);
            }
        }
    }

    public enum CancellationReason
    {
        Cancelled,
        StudentGroupBusy,
        ProfessorBusy,
        AuditoriumOccupied,
        Other
    }
}
