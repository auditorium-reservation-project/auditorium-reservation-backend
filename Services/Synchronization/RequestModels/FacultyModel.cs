﻿namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class FacultyModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
