﻿namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class BuildingModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
