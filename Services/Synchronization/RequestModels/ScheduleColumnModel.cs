﻿namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class ScheduleColumnModel
    {
        public DateOnly Date { get; set; }
        public ICollection<LessonGridModel> Lessons { get; set; } = new List<LessonGridModel>();
    }
}
