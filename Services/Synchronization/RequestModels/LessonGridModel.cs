﻿using AuditoriumReservation.DAL.Enumerations;

namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class LessonGridModel
    {
        //Empty
        public string Type { get; set; }//EMPTY | LESSON
        public int LessonNumber { get; set; }
        public int starts { get; set; }
        public int ends { get; set; }
        //Filled
        public string? Id { get; set; }
        public string? Title { get; set; }
        public LessonType? LessonType { get; set; }
        public ICollection<StudentGroupModel>? Groups { get; set; } = new List<StudentGroupModel>();
        public ProfessorModel? Professor { get; set; }
        public AudienceModel? Audience { get; set; }

    }

    public enum LessonType
    {
        LECTURE,
        PRACTICE,
        LABORATORY,
        SEMINAR,
        INDIVIDUAL,
        CONTROL_POINT,
        OTHER,
        DIFFERENTIAL_CREDIT,
        CREDIT,
        CONSULTATION,
        EXAM,
        BOOKING
    }

    public static class ScheduleModelUtils
    {
        public static ClassType MapLessonTypeToClasType(LessonType type) => type switch
        {
            LessonType.LECTURE => ClassType.Lecture,
            LessonType.PRACTICE => ClassType.Practicum,
            LessonType.SEMINAR => ClassType.Seminar,
            LessonType.INDIVIDUAL => ClassType.Individual,
            LessonType.CONTROL_POINT => ClassType.Control,
            LessonType.DIFFERENTIAL_CREDIT => ClassType.DiffCredit,
            LessonType.CREDIT => ClassType.Credit,
            LessonType.CONSULTATION => ClassType.Consultation,
            LessonType.EXAM => ClassType.Exam,
            LessonType.OTHER => ClassType.Other,
            _ => ClassType.Other
        };
    }
}
