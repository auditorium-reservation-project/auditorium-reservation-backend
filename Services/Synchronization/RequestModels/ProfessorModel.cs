﻿namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class ProfessorModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
    }
}
