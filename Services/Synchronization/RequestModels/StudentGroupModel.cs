﻿namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class StudentGroupModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSubgroup { get; set; }
    }
}
