﻿namespace AuditoriumReservation.Services.Synchronization.RequestModels
{
    public class AudienceModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
