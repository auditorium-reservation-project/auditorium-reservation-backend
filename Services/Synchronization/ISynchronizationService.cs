﻿namespace AuditoriumReservation.Services.Synchronization
{
    public interface ISynchronizationService
    {
        Task SyncSchedule(IEnumerable<DateOnly>? dates = null);
        Task DeleteSynchronizedData();
    }
}