﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DAL.Entities.ReferenceEntityIdRecords;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Services.Synchronization.RequestModels;
using AuditoriumReservation.Utility;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Org.BouncyCastle.Pqc.Crypto.Frodo;
using System.Globalization;
using System.Net.Http.Headers;
using System.Reflection.Metadata.Ecma335;

namespace AuditoriumReservation.Services.Synchronization
{
    public class SynchronizationService : ISynchronizationService
    {
        private readonly HttpClient _httpClient;
        private readonly IScheduleService _scheduleService;
        private readonly IReservationService _reservationService;
        private readonly ReservationDbContext _dbContext;
        private readonly string _baseApiRoute;
        public SynchronizationService(HttpClient client,
            IScheduleService scheduleService,
            IReservationService reservationService,
            ReservationDbContext context)
        {
            _httpClient = client;
            _scheduleService = scheduleService;
            _dbContext = context;
            _reservationService = reservationService;
            _baseApiRoute = Environment.GetEnvironmentVariable("INTIME_API_BASE")
                ?? throw new ArgumentNullException("Failed to read INTIME_API_BASE environment variable");

            _httpClient.BaseAddress = new Uri(_baseApiRoute);
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "Reservation Service Backend");

        }

        public async Task SyncSchedule(IEnumerable<DateOnly>? dates = null)
        {
            //if (dates is null)
            //{
            //    dates = DateOnly.FromDateTime(DateTime.Now).GetDatesOfWeek(CultureInfo.CurrentCulture);
            //}
            //else if (dates.Count() <= 0)
            //{
            //    return;
            //}
            var from = DateOnly.FromDateTime(DateTime.Now.AddDays(-21));
            var to = DateOnly.FromDateTime(DateTime.Now.AddDays(21));


            await SyncProfessors(false);
            await SyncBuildingsRooms(false);
            await SyncFaculties(false);
            await _dbContext.SaveChangesAsync();
            await SyncSchedule(from, to);

            await _dbContext.SaveChangesAsync();
        }

        private async Task SyncProfessors(bool saveChanges = true)
        {
            var professorsRequestResponse = await _httpClient.GetAsync("v1/professors");
            var professorsResponseBody = await professorsRequestResponse.Content.ReadAsStringAsync();
            var professors = JsonConvert.DeserializeObject<List<ProfessorModel>>(professorsResponseBody)
                ?? throw new JsonSerializationException("Failed to deserialize professors data retrieved from httpClient");

            var remoteIds = professors.Select(x => x.Id).ToArray();
            var entities = new List<Professor>();
            var records = new List<ProfessorRecord>();

            var existingRecords = await _dbContext.ProfessorRecords
                .Include(x => x.RelatedEntity)
                .Where(x => remoteIds.Contains(x.RemoteId))
                .ToListAsync();

            foreach (var p in professors)
            {
                var existingRecord = existingRecords.SingleOrDefault(x => x.RemoteId == p.Id);
                var entity = existingRecord?.RelatedEntity ?? new Professor();
                var record = existingRecord ?? new ProfessorRecord();

                entity.Name = p.FullName;
                record.RelatedEntity = entity;
                record.RemoteId = p.Id;

                if (existingRecord is null)
                {
                    entities.Add(entity);
                    records.Add(record);
                }
            }

            await _dbContext.Professors.AddRangeAsync(entities);
            await _dbContext.ProfessorRecords.AddRangeAsync(records);

            if (saveChanges == true)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SyncBuildingsRooms(bool saveChanges = true)
        {
            var buildingsRequestResponse = await _httpClient.GetAsync("v1/buildings");
            var buildingsResponseBody = await buildingsRequestResponse.Content.ReadAsStringAsync();
            var buildings = JsonConvert.DeserializeObject<ICollection<BuildingModel>>(buildingsResponseBody)
                ?? throw new JsonSerializationException("Failed to deserialize buildings data retrieved from httpClient");

            var remoteIds = buildings.Select(x => x.Id).ToArray();

            var entities = new List<Building>();
            var records = new List<BuildingRecord>();

            var existingRecords = await _dbContext.BuildingsRecords
                .Include(x => x.RelatedEntity)
                .Where(x => remoteIds.Contains(x.RemoteId))
                .ToListAsync();

            foreach (var b in buildings)
            {
                var existingRecord = existingRecords.SingleOrDefault(x => x.RemoteId == b.Id);
                var entity = existingRecord?.RelatedEntity ?? new Building();
                var record = existingRecord ?? new BuildingRecord();

                entity.Name = b.Name;
                record.RemoteId = b.Id;
                record.RelatedEntity = entity;

                await SyncAuditoriaForBuilding(record, false);

                if (existingRecord is null)
                {
                    entities.Add(entity);
                    records.Add(record);
                }
            }

            await _dbContext.Buildings.AddRangeAsync(entities);
            await _dbContext.BuildingsRecords.AddRangeAsync(records);

            if (saveChanges)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SyncAuditoriaForBuilding(BuildingRecord buildingRecord, bool saveChanges = true)
        {
            var audiencesRequestResponse = await _httpClient.GetAsync($"v1/buildings/{buildingRecord.RemoteId}/audiences");
            var audiencesResponseBody = await audiencesRequestResponse.Content.ReadAsStringAsync();
            var audiences = JsonConvert.DeserializeObject<List<AudienceModel>>(audiencesResponseBody)
                ?? throw new JsonSerializationException("Failed to deserialize audience data retrieved from httpClient");

            var remoteIds = audiences.Select(x => x.Id).ToArray();
            var entities = new List<Auditorium>();
            var records = new List<AuditoriumRecord>();

            var existingRecords = await _dbContext.AuditoriaRecords
                .Include(x => x.RelatedEntity)
                .Where(x => remoteIds.Contains(x.RemoteId))
                .ToListAsync();

            foreach (var a in audiences)
            {
                var existingRecord = existingRecords.SingleOrDefault(x => x.RemoteId == a.Id);
                var entity = existingRecord?.RelatedEntity ?? new Auditorium();
                var record = existingRecord ?? new AuditoriumRecord();

                entity.Name = a.Name;
                entity.Building = buildingRecord.RelatedEntity;
                record.RemoteId = a.Id;
                record.RelatedEntity = entity;

                if (existingRecord is null)
                {
                    entities.Add(entity);
                    records.Add(record);
                }
            }
            //await EnsureOnlineVAuditoriaExists();
            await _dbContext.Auditoria.AddRangeAsync(entities);
            await _dbContext.AuditoriaRecords.AddRangeAsync(records);

            if (saveChanges)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        //private async Task EnsureOnlineVAuditoriaExists()
        //{
        //    var auditoria = await _dbContext.AuditoriaRecords
        //        .Include(x => x.RelatedEntity)
        //        .SingleOrDefaultAsync(x => x.RemoteId == )
        //}

        private async Task SyncFaculties(bool saveChanges = true)
        {
            var facultiesRequestResponse = await _httpClient.GetAsync("v1/faculties");
            var facultiesResponseBody = await facultiesRequestResponse.Content.ReadAsStringAsync();
            var faculties = JsonConvert.DeserializeObject<List<FacultyModel>>(facultiesResponseBody)
                ?? throw new JsonSerializationException("Failed to deserialize faculty data retrieved from httpClient");

            var remoteIds = faculties.Select(x => x.Id).ToArray();
            var entities = new List<Faculty>();
            var records = new List<FacultyRecord>();

            var existingRecords = await _dbContext.FacultyRecords
                .Include(x => x.RelatedEntity)
                .Where(x => remoteIds.Contains(x.RemoteId))
                .ToListAsync();

            foreach (var a in faculties)
            {
                var existingRecord = existingRecords.SingleOrDefault(x => x.RemoteId == a.Id);
                var entity = existingRecord?.RelatedEntity ?? new Faculty();
                var record = existingRecord ?? new FacultyRecord();

                entity.Name = a.Name;
                record.RemoteId = a.Id;
                record.RelatedEntity = entity;

                await SyncGroupsForFaculty(record, false);

                if (existingRecord is null)
                {
                    entities.Add(entity);
                    records.Add(record);
                }
            }

            await _dbContext.Faculties.AddRangeAsync(entities);
            await _dbContext.FacultyRecords.AddRangeAsync(records);

            if (saveChanges == true)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SyncGroupsForFaculty(FacultyRecord facultyRecord, bool saveChanges = false)
        {
            var groupsRequestResponse = await _httpClient.GetAsync($"v1/faculties/{facultyRecord.RemoteId}/groups");
            var groupsResponseBody = await groupsRequestResponse.Content.ReadAsStringAsync();
            var groups = JsonConvert.DeserializeObject<List<StudentGroupModel>>(groupsResponseBody)
                ?? throw new JsonSerializationException("Failed to deserialize Student Groups data retrieved from httpClient");

            var remoteIds = groups.Select(x => x.Id).ToArray();
            var entities = new List<StudentGroup>();
            var records = new List<StudentGroupRecord>();

            var existingRecords = await _dbContext.StudentGroupRecords
                .Include(x => x.RelatedEntity)
                .Where(x => remoteIds.Contains(x.RemoteId))
                .ToListAsync();

            foreach (var g in groups)
            {
                var existingRecord = existingRecords.SingleOrDefault(x => x.RemoteId == g.Id);
                var entity = existingRecord?.RelatedEntity ?? new StudentGroup();
                var record = existingRecord ?? new StudentGroupRecord();

                entity.Name = g.Name;
                entity.Faculty = facultyRecord.RelatedEntity;
                record.RemoteId = g.Id;
                record.RelatedEntity = entity;

                if (existingRecord is null)
                {
                    entities.Add(entity);
                    records.Add(record);
                }
            }

            await _dbContext.StudentGroups.AddRangeAsync(entities);
            await _dbContext.StudentGroupRecords.AddRangeAsync(records);

            if (saveChanges)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SyncSchedule(DateOnly from, DateOnly to)
        {
            var auditoriaRecords = _dbContext.AuditoriaRecords.Include(x => x.RelatedEntity);
            var professorRecords = _dbContext.ProfessorRecords.Include(x => x.RelatedEntity);
            var groupRecords = _dbContext.StudentGroupRecords.Include(x => x.RelatedEntity);

            
            var auditoria = await auditoriaRecords.ToListAsync();

            var classes = new List<Class>();

            var progress = 1;
            foreach (var x in auditoria)
            {
                Console.WriteLine($"[{progress++}/{auditoria.Count}] Parsing schedule");
                var scheduleRequestResponse = await _httpClient.GetAsync($"v1/schedule/audience?id={x.RemoteId}&dateFrom={from.ToString("yyyy-MM-dd")}&dateTo={to.ToString("yyyy-MM-dd")}");
                var scheduleResponseBody = await scheduleRequestResponse.Content.ReadAsStringAsync();
                var schedule = JsonConvert.DeserializeObject<List<ScheduleColumnModel>>(scheduleResponseBody)
                    ?? throw new JsonSerializationException("Failed to deserialize schedule data retrieved from httpClient");
                foreach (var column in schedule)
                {
                    foreach (var lesson in column.Lessons)
                    {
                        if (lesson.Type == "EMPTY")
                        {
                            continue;
                        }

                        if (lesson.Type != "LESSON")
                        {
                            continue;
                        }



                        var @class = new Class() { Id = Guid.NewGuid() };

                        //Subject
                        var subject = await _dbContext.Subjects.FirstOrDefaultAsync(x => x.Name == lesson.Title);
                        if (subject is null)
                        {
                            subject = new Subject();
                            subject.Name = lesson.Title ?? subject.Name;
                            _dbContext.Subjects.Add(subject);
                            await _dbContext.SaveChangesAsync();
                        }

                        @class.Subject = subject;

                        //Lesson Type
                        if (lesson.LessonType is not null)
                        {
                            @class.Type = ScheduleModelUtils.MapLessonTypeToClasType(lesson.LessonType.Value);
                        }
                        else
                        {
                            @class.Type = DAL.Enumerations.ClassType.Other;
                        }

                        //TimeSlot

                        var startTime = TimeOnly.FromTimeSpan(TimeSpan.FromSeconds(lesson.starts)).AddHours(1);
                        var endTime = TimeOnly.FromTimeSpan(TimeSpan.FromSeconds(lesson.ends)).AddHours(1);

                        var timeSlot = await _dbContext.TimeSlots.FirstOrDefaultAsync(x => startTime - x.StartTime < TimeSpan.FromHours(1) && endTime - x.EndTime < TimeSpan.FromHours(1))
                            ?? throw new NotFoundException("Time Slot within set intrevals was not found, aborting sync: " + startTime.ToShortTimeString() + " - " + endTime.ToShortTimeString());
                        @class.TimeSlot = timeSlot;


                        //Professor
                        var professor = await professorRecords.SingleOrDefaultAsync(x => x.RemoteId == lesson.Professor.Id);
                        if (professor is null)
                        {
                            professor = new ProfessorRecord()
                            {
                                RemoteId = lesson.Professor.Id,
                                RelatedEntity = new Professor()
                                {
                                    Name = lesson.Professor.FullName
                                }
                            };
                            _dbContext.ProfessorRecords.Add(professor);
                            await _dbContext.SaveChangesAsync();
                        }

                        @class.Professor = professor.RelatedEntity;

                        //Audience
                        //var audience = await auditoriaRecords.SingleOrDefaultAsync(x => x.RemoteId == lesson.Audience.Id)
                        //    ?? throw new NotFoundException<AuditoriumRecord>(lesson?.Audience?.Id.ToString() ?? "null");

                        @class.Auditorium = x.RelatedEntity;

                        //Date
                        @class.Date = column.Date;

                        //Groups
                        var groupRemoteIds = lesson.Groups.Select(x => x.Id).ToList();
                        var groups = await groupRecords
                            .Where(x => groupRemoteIds.Contains(x.RemoteId))
                            .Select(x => x.RelatedEntity)
                            .ToListAsync();

                        @class.StudentGroups = groups;

                        classes.Add(@class);
                    }
                }
            }

            var res = _dbContext.Reservations.Include(x => x.TimeSlot).Include(x => x.Auditorium);
            var resToCancel = new List<Reservation>();
            foreach (var @class in classes)
            {
                resToCancel.AddRange(await res
                    .Where(x => x.Date == @class.Date && x.TimeSlot.Index == @class.TimeSlot.Index)
                    .Where(x => x.Auditorium.Id == @class.Auditorium.Id)
                    .ToListAsync());
            }

            await _reservationService.CancelReservations(resToCancel, CancellationReason.AuditoriumOccupied);
            await _scheduleService.DeleteClasses(from, to);
            await _dbContext.Classes.AddRangeAsync(classes);
        }

        private async Task HandleConcurrencyConflicts()
        {

            throw new NotImplementedException();
        }

        public async Task DeleteSynchronizedData()
        {
            var auditoriaRecords = await _dbContext.AuditoriaRecords.Include(x => x.RelatedEntity).ToListAsync();
            var professorRecords = await _dbContext.ProfessorRecords.Include(x => x.RelatedEntity).ToListAsync();
            var groupRecords = await _dbContext.StudentGroupRecords.Include(x => x.RelatedEntity).ToListAsync();
            var subjectRecords = await _dbContext.SubjectRecords.Include(x => x.RelatedEntity).ToListAsync();
            var buildingRecords = await _dbContext.BuildingsRecords.Include(x => x.RelatedEntity).ToListAsync();
            var facultyRecords = await _dbContext.FacultyRecords.Include(x => x.RelatedEntity).ToListAsync();

            _dbContext.Auditoria.RemoveRange(auditoriaRecords.Select(x => x.RelatedEntity));
            _dbContext.Professors.RemoveRange(professorRecords.Select(x => x.RelatedEntity));
            _dbContext.StudentGroups.RemoveRange(groupRecords.Select(x => x.RelatedEntity));
            _dbContext.Subjects.RemoveRange(subjectRecords.Select(x => x.RelatedEntity));
            _dbContext.Buildings.RemoveRange(buildingRecords.Select(x => x.RelatedEntity));
            _dbContext.Faculties.RemoveRange(facultyRecords.Select(x => x.RelatedEntity));
            await _dbContext.SaveChangesAsync();
        }
    }
}
