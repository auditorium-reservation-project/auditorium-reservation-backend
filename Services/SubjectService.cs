﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly ReservationDbContext _dbContext;
        public SubjectService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<SubjectDto> GetSubject(Guid id)
        {
            var subject = await _dbContext.Subjects.FindAsync(id)
                ?? throw new NotFoundException<Subject>(id);

            return SubjectConverter.FromSubject(subject);
        }

        public async Task<SubjectDto> CreateSubject(CreateUpdateSubjectDto subjectDto)
        {
            var subject = new Subject();
            if (subjectDto.Name != null)
            {
                subject.Name = subjectDto.Name;
            }
            await _dbContext.Subjects.AddAsync(subject);
            await _dbContext.SaveChangesAsync();

            return SubjectConverter.FromSubject(subject);
        }

        public async Task<SubjectDto> UpdateSubject(Guid id, CreateUpdateSubjectDto subjectDto)
        {
            var subject = await _dbContext.Subjects.FindAsync(id)
                ?? throw new NotFoundException<Subject>(id);

            subject.Name = subjectDto.Name ?? subject.Name;

            await _dbContext.SaveChangesAsync();

            return SubjectConverter.FromSubject(subject);
        }

        public async Task DeleteSubject(Guid id)
        {
            var subject = await _dbContext.Subjects.FindAsync(id)
                ?? throw new NotFoundException<Subject>(id);
            _dbContext.Subjects.Remove(subject);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<SubjectDto>> GetAllSubjects()
        {
            return await _dbContext.Subjects
                .Select(x => SubjectConverter.FromSubject(x))
                .ToListAsync();
        }

        public async Task DeleteAll()
        {
            _dbContext.RemoveRange(await _dbContext.Subjects.ToArrayAsync());
            await _dbContext.SaveChangesAsync();
        }
    }
}
