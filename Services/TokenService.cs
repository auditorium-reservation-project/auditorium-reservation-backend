﻿using AuditoriumReservation.Configuration;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace AuditoriumReservation.Services
{
    public class TokenService : ITokenService
    {
        private readonly AuthenticationConfiguration _authConfig;
        private readonly JwtSecurityTokenHandler _jwtHandler;
        public TokenService(IOptions<AuthenticationConfiguration> config)
        {
            _authConfig = config.Value;
            _jwtHandler = new JwtSecurityTokenHandler();
        }
        public string GetAccessToken(ICollection<Claim> claims)
        {
            var credentials = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_authConfig.JwtIssuerSigningKey)),
                SecurityAlgorithms.HmacSha256
            );

            var tokenOptions = new JwtSecurityToken(
                _authConfig.JwtIssuer,
                _authConfig.JwtAudience,
                signingCredentials: credentials,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_authConfig.LifeTimeMinutes));

            return _jwtHandler.WriteToken(tokenOptions);
        }
    }
}
