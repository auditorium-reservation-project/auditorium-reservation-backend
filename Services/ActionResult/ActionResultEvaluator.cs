﻿using AuditoriumReservation.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace AuditoriumReservation.Services.ActionResult
{
    public class ActionResultEvaluator
    {
        public static ObjectResult Evaluate(Exception ex)
        {
            return ex switch
            {
                NotFoundException => new NotFoundObjectResult(ex.Message),
                AlreadyExistsException => new ConflictObjectResult(ex.Message),
                ReservationBaseException => new ObjectResult("Unhandled Internal Error") { StatusCode = 500 },
                _ => new ObjectResult("Unhandled Internal Error") { StatusCode = 500 }
            };
        }
    }
}
