﻿namespace AuditoriumReservation.Services.ActionResult
{
    public class ServiceActionResult
    {
        public bool Success { get; protected set; }
        public Exception? Exception { get; protected set; }

        public ServiceActionResult(bool success, Exception exception)
        {
            Success = success;
            Exception = exception;
        }
        public ServiceActionResult(Exception exception)
        {
            Success = false;
            Exception = exception;
        }
        public ServiceActionResult(bool success) { Success = success; }

        public ServiceActionResult()
        {
            Success = true;
        }
        public ServiceActionResult Succeed()
        {
            return new ServiceActionResult();
        }
        public ServiceActionResult Fail(Exception? exception)
        {
            return exception is not null ? new ServiceActionResult(exception)
                    : new ServiceActionResult(false);
        }
        public T Evaluate<T>(Func<T> succeed, Func<Exception?, T> fail)
        {
            return Success ? succeed() : fail(Exception);
        }
    }

    public class ServiceActionResult<T> : ServiceActionResult
    {
        public T? Content { get; set; }
        public ServiceActionResult(T? content)
        {
            Content = content;
        }
        public ServiceActionResult(bool success, Exception exception)
        {
            Success = success;
            Exception = exception;
        }
        public ServiceActionResult(Exception exception)
        {
            Success = false;
            Exception = exception;
        }
        public ServiceActionResult(bool success) { Success = success; }
        public ServiceActionResult Succeed(T? content)
        {
            return new ServiceActionResult<T>(content);
        }


    }
}
