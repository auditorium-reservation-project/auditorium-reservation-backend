﻿namespace AuditoriumReservation.Services
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using AuditoriumReservation.Controllers;
    using Microsoft.Extensions.Hosting;

    public class SynchronizationHelperService : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly IServiceProvider _serviceProvider; 

        public SynchronizationHelperService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(60));
            return Task.CompletedTask;
        }

        private async void DoWork(object? state)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var scheduleController = scope.ServiceProvider.GetRequiredService<ClassController>();
                await scheduleController.SyncSchedule();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }

}
