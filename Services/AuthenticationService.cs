﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.ActionResult;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using IAuthenticationService = AuditoriumReservation.Services.Interfaces.IAuthenticationService;

namespace AuditoriumReservation.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private ReservationDbContext _dbContext;
        private IPasswordHasher<User> _passwordHasher;
        public AuthenticationService(ReservationDbContext dbContext, IPasswordHasher<User> passwordHasher)
        {
            _dbContext = dbContext;
            _passwordHasher = passwordHasher;
        }
        public async Task<User> Login(LoginDto dto)
        {
            var user = await _dbContext.Users
                .SingleOrDefaultAsync(x => x.Email == dto.Email)
                    ?? throw new AccessDeniedException("Invalid Credentials");

            if(user.Password == dto.Password)
            {
                user.Password = _passwordHasher.HashPassword(user, dto.Password);
                await _dbContext.SaveChangesAsync();
            }

            var verificationResult = _passwordHasher.VerifyHashedPassword(user, user.Password, dto.Password);

            if(verificationResult.Equals(PasswordVerificationResult.Failed))
            {
                throw new AccessDeniedException("Invalid Credentials");
            }

            return user;
        }
    }
}