﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.RequestDto;
using AuditoriumReservation.DTO.ScheduleDto;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using AuditoriumReservation.Services.MailClient;
using AuditoriumReservation.Services.Schedule;
using Microsoft.EntityFrameworkCore;

namespace AuditoriumReservation.Services
{
    public class ReservationService : IReservationService
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IMailService _mailService;
        private readonly IQueryable<Reservation> _reservations;

        public ReservationService(ReservationDbContext dbContext, IMailService mailService)
        {
            _dbContext = dbContext;
            _mailService = mailService;
            _reservations = _dbContext.Reservations
                .Include(x => x.Auditorium)
                .Include(x => x.CreatedBy)
                .Include(x => x.TimeSlot);
        }

        public async Task<ReservationDto> GetReservation(Guid id)
        {
            var reservation = await _reservations
                .SingleOrDefaultAsync(x => x.Id == id)
                    ?? throw new NotFoundException<Reservation>(id);

            return ReservationConverter.ToDto(reservation);
        }

        public async Task<ICollection<ReservationDto>> GetReservations(GetReservationsRequestDto dto)
        {
            var reservationsQuery = _reservations
                .Where(x => x.Date >= dto.StartDate && x.Date <= dto.EndDate);

            if(dto.AuditoriaIds is not null)
            {
                reservationsQuery = reservationsQuery.Where(x => dto.AuditoriaIds.Contains(x.Auditorium.Id));
            }


            return await reservationsQuery
                .Select(x => ReservationConverter.ToDto(x))
                .ToListAsync();
        }

        public async Task<ReservationDto> CreateReservation(User user, CreateReservationDto dto)
        {
            var auditorium = await _dbContext.Auditoria.FindAsync(dto.AuditoriumId)
                ?? throw new NotFoundException<Auditorium>(dto.AuditoriumId);

            var timeSlot = await _dbContext.TimeSlots
                .Where(x => x.Index == dto.TimeSlotIndex)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException("Invalid time slot index");

            bool hasOverlaps = await _dbContext.Classes
                .AnyAsync(x => x.Date == dto.Date
                    && x.TimeSlot.Index == dto.TimeSlotIndex
                    && x.Auditorium.Id == dto.AuditoriumId);

            hasOverlaps &= await _dbContext.Reservations
                .AnyAsync(x => x.Date == dto.Date
                    && x.TimeSlot.Index == dto.TimeSlotIndex
                    && x.Auditorium.Id == dto.AuditoriumId);

            if (hasOverlaps){
                throw new ScheduleException("Unable to create reservation: auditorium occupied");
            }

            var reservation = new Reservation()
            {
                Name = dto.Name,
                Date = dto.Date,
                CreatedBy = user,
                Details = dto.Details,
                Auditorium = auditorium,
                TimeSlot = timeSlot,
                CreatedOn = DateTime.Now,
                EmailSubscription = dto.NotifyByEmail,
                Status = DAL.Enumerations.ReservationStatus.Proposed
            };

            await _dbContext.Reservations.AddAsync(reservation);
            await _dbContext.SaveChangesAsync();

            return ReservationConverter.ToDto(reservation);
        }        

        public async Task<ReservationDto> UpdateReservation(User user, Guid id, UpdateReservationDto reservationDto)
        {
            var reservation = await _reservations.Where(x => x.Id == id).SingleOrDefaultAsync()
                ?? throw new NotFoundException<Reservation>(id);

            if(reservation.CreatedBy.Id != user.Id)
            {
                throw new UnauthorizedAccessException();
            }

            if(reservation.Status == DAL.Enumerations.ReservationStatus.Approved)
            {
                throw new ScheduleException("Reservation already approved");
            }

            reservation.Name = reservationDto.Name ?? reservation.Name;
            reservation.Details = reservationDto.Details ?? reservation.Details;
            reservation.EmailSubscription = reservationDto.NotifyByEmail ?? reservation.EmailSubscription;

            await _dbContext.SaveChangesAsync();

            return ReservationConverter.ToDto(reservation);
        }
        public async Task<ReservationCancellationDto> CancelReservation(
            Guid id,
            CancellationReason reason = CancellationReason.Cancelled)
        {
            var reservation = await _dbContext.Reservations
                .Include(x => x.TimeSlot)
                .Include(x => x.Auditorium)
                .Include(x => x.CreatedBy)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync()
                    ?? throw new NotFoundException<Reservation>(id);

            await CancelReservation(reservation, reason);
            return ReservationConverter.FromCancelled(reservation, reason);
        }

        public async Task CancelReservation(Reservation reservation, CancellationReason reason, Class conflictingClass = null)
        {
            if (reservation.EmailSubscription == true)
            {
                await _mailService.NotifyOnReservationCancel(reservation.CreatedBy,
                    ReservationConverter.FromCancelled(reservation, reason));
            }
            _dbContext.Reservations.Remove(reservation);
            await _dbContext.SaveChangesAsync();
        }

        public async Task CancelReservations(ICollection<Reservation> reservations, CancellationReason reason)
        {
            foreach (var res in reservations)
            {
                if (res.EmailSubscription == true)
                {
                    await _mailService.NotifyOnReservationCancel(res.CreatedBy,
                        ReservationConverter.FromCancelled(res, reason));
                }
            }

            _dbContext.Reservations.RemoveRange(reservations);
            await _dbContext.SaveChangesAsync();
        }

        async Task<ReservationDto> IReservationService.ApproveReservation(Guid id)
        {
            var reservation = await _dbContext.Reservations
                .Include(x => x.TimeSlot)
                .Include(x => x.CreatedBy)
                .Include(x => x.Auditorium)
                .SingleOrDefaultAsync(x => x.Id == id)
                    ?? throw new NotFoundException("Invalid reservation id");

            reservation.Status = DAL.Enumerations.ReservationStatus.Approved;

            if(reservation.EmailSubscription == true)
            {
                await _mailService.SendEmailAsync(reservation.CreatedBy.Email, "Your reservation has been approved", "Your reservation for auditorium " + reservation.Auditorium.Name + " has been approved");
            }
            await _dbContext.SaveChangesAsync();
            return reservation.ToDto();
        }

        public async Task<ICollection<ReservationDto>> GetUnapproved()
        {
            return await _dbContext.Reservations
                .Include(x => x.CreatedBy)
                .Include(x => x.TimeSlot)
                .Include(x => x.Auditorium)
                .Where(x => x.Status == DAL.Enumerations.ReservationStatus.Proposed)
                .Select(x => x.ToDto())
                .ToListAsync();
        }

        public async Task<ScheduleDto> GetForAuditorium(Guid id, DateOnly startDate, DateOnly endDate)
        {
            var reservations = await _dbContext.Reservations
                .Include(x => x.TimeSlot)
                .Include(x => x.Auditorium)
                .Include(x => x.CreatedBy)
                .Where(x => x.Status == DAL.Enumerations.ReservationStatus.Approved)
                .Where(x => x.Auditorium.Id == id)
                .Where(x => x.Date >= startDate && x.Date <= endDate)
                .ToListAsync();

            var days = DateRange(startDate.ToDateTime(TimeOnly.MinValue), endDate.ToDateTime(TimeOnly.MinValue))
                .Select(x => DateOnly.FromDateTime(x));

            var schedule = new ScheduleDto()
            {
                StartDate = startDate,
                EndDate = endDate,
                Days = new List<ScheduleDayDto>()
            };

            var reservationsIndex = 0;
            var timeSlots = await _dbContext.TimeSlots.ToListAsync();

            foreach (var date in days)
            {
                schedule.Days.Add(new ScheduleDayDto()
                {
                    Date = date,
                    Slots = GenerateScheduleSlots(reservations, date, ref reservationsIndex, timeSlots)
                });

            }
            return schedule;
        }
        private List<ScheduleSlotDto> GenerateScheduleSlots(List<Reservation> reservations,
            DateOnly date,
            ref int index,
            List<TimeSlot> timeSlots)
        {
            var slots = new List<ScheduleSlotDto>();
            if (index >= reservations.Count || reservations[index].Date > date)
            {
                return _dbContext.TimeSlots.Select(x => new ScheduleSlotDto()
                {
                    Type = ScheduleSlotType.Empty,
                    StartTime = x.StartTime,
                    EndTime = x.EndTime,
                    SlotIndex = x.Index
                }).ToList();
            }

            foreach (var timeSlot in timeSlots)
            {
                if (index < reservations.Count && reservations[index].Date == date && reservations[index].TimeSlot.Index == timeSlot.Index)
                {
                    slots.Add(ScheduleConverter.SlotDtoFromReservation(reservations[index]));
                    index++;
                }
                else
                {
                    slots.Add(ScheduleConverter.SlotDtoFromTimeSlot(timeSlot));
                }
            }
            return slots;
        }
        private IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d));
        }
    }
}
