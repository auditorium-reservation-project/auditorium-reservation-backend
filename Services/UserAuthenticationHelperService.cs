﻿using AuditoriumReservation.DAL;
using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.Exceptions;
using AuditoriumReservation.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace AuditoriumReservation.Services
{
    public class UserAuthenticationHelperService : IUserAuthenticationHelperService
    {
        private readonly ReservationDbContext _dbContext;
        private readonly IQueryable<User> _users;
        public UserAuthenticationHelperService(ReservationDbContext dbContext)
        {
            _dbContext = dbContext;
            _users = _dbContext.Users;
        }

        public async Task<User> GetUserFromPrincipal(ClaimsPrincipal principal)
        {
            if(principal?.Identity?.Name is null) { 
                throw new ArgumentNullException(nameof(principal));
            }

            if(!Guid.TryParse(principal.Identity.Name, out var id))
            {
                throw new ArgumentException("Invalid user id");
            }

            return await _users.SingleOrDefaultAsync(x => x.Id == id)
                ?? throw new NotFoundException("Failed to authenticate");
        }

        public async Task<User?> GetUserFromPrincipalNoThrow(ClaimsPrincipal principal)
        {
            if (principal?.Identity?.Name is null)
            {
                return null;
            }

            if (!Guid.TryParse(principal.Identity.Name, out var id))
            {
                return null;
            }

            return await _users.SingleOrDefaultAsync(x => x.Id == id);                
        }
    }
}
