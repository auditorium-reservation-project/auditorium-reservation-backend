﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class FixFKConstraintsOnOptionalRelations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Users_Id",
                table: "Professors");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Professors_Id",
                table: "Staff");

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "SubjectRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "StudentGroupRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProfessorEntityId",
                table: "Staff",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "LinkedUserId",
                table: "Professors",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "ProfessorRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "FacultyRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "BuildingsRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "AuditoriaRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Staff_ProfessorEntityId",
                table: "Staff",
                column: "ProfessorEntityId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Professors_LinkedUserId",
                table: "Professors",
                column: "LinkedUserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Users_LinkedUserId",
                table: "Professors",
                column: "LinkedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Professors_ProfessorEntityId",
                table: "Staff",
                column: "ProfessorEntityId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Users_LinkedUserId",
                table: "Professors");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Professors_ProfessorEntityId",
                table: "Staff");

            migrationBuilder.DropIndex(
                name: "IX_Staff_ProfessorEntityId",
                table: "Staff");

            migrationBuilder.DropIndex(
                name: "IX_Professors_LinkedUserId",
                table: "Professors");

            migrationBuilder.DropColumn(
                name: "ProfessorEntityId",
                table: "Staff");

            migrationBuilder.DropColumn(
                name: "LinkedUserId",
                table: "Professors");            

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "SubjectRecords",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "StudentGroupRecords",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "ProfessorRecords",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "FacultyRecords",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "BuildingsRecords",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "RelatedEntityId",
                table: "AuditoriaRecords",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Users_Id",
                table: "Professors",
                column: "Id",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Professors_Id",
                table: "Staff",
                column: "Id",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
