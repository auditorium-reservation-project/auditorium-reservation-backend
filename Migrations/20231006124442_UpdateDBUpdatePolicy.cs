﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class UpdateDBUpdatePolicy : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("035276eb-1500-4aa0-ac81-f6d7f162015d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("0c6f66f1-78a5-41a2-99ba-cf8ec96dca22"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("123b11bd-d0e8-4a28-b659-099c4e692cb9"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("16a33881-1f18-4846-97c1-3427eddc0515"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("2ed33c64-e5ab-4c3b-b030-b7fe4ffcc214"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("3340f217-da0e-4a4a-8ea6-91e29036de72"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("43160f47-fadc-45ae-b3d6-af3013154f8e"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("4bdc740e-f909-4746-a818-b5617190a459"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("50a2a935-6d66-42db-9481-8aefd783caee"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("511401e0-5939-43b6-9fca-377a9d76c458"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("54edcfed-b214-4d1f-92f6-6354f916a9b3"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("69808a8b-4e4d-4d79-a839-37a41380b32d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("6a393eb0-568d-48e8-b0b8-15bb8aa3bf7c"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("6e758d7c-3a0e-45c0-882a-c839cdeea036"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("751829c3-9f4e-4fbb-990a-9337f7358dc2"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("8a7439e5-2d99-469f-82cf-11d390b96b2d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("98240eaf-b062-4de8-823f-aa8f3f4bc304"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("9dd2435b-aa5e-429b-be64-f5baa42796b6"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("a295d475-aad4-4b6f-8c8f-454b997ddeb4"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("b71cccbe-d0b1-43b7-b3a1-cbf49557bbe0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("b7f73711-2224-49a8-a43f-21b3ba8b9859"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e047eefc-c1c1-4025-83bc-9a1f58b456f0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e28d418d-77f1-4e12-8b3b-9e2654df6d7a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e49d2b42-d154-4e31-a147-36ea89b8efc2"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e93fc5de-64ac-4655-9402-66cf6f80f465"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ea3540a3-0d60-4662-8750-ab740a7b10b7"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ebd57657-1a75-4018-8af5-7bcd30e7901e"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ed8e7a8d-8d9f-40f6-b7e2-cb09ca6b4ff0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f782a678-b861-46c5-8c62-29ca226c63b0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ff23b3d5-85dd-48be-9c44-c6129d7fab7f"));

            migrationBuilder.InsertData(
                table: "Buildings",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("06ccf9ac-179f-4b73-bd3a-65401fa0a41d"), "23 корпус" },
                    { new Guid("093eb00b-9ca5-411d-8b62-88559b09e92f"), "6 корпус" },
                    { new Guid("0e5ea39c-2bcb-46bc-a94c-c7740f733ca1"), "7 корпус" },
                    { new Guid("1aae5ef0-05d0-4641-b9e6-b18b106dee76"), "17 корпус" },
                    { new Guid("26aff5be-1ea9-4e91-a349-2b8451dfc1c2"), "12 корпус" },
                    { new Guid("27f7547b-7e2e-48b2-a7d8-2ef5610d123f"), "Криогенный корпус" },
                    { new Guid("348c0484-5dce-49ee-aa00-40da52aab306"), "20 корпус" },
                    { new Guid("4935c4ad-acb9-44e3-9ce2-418af0c31c29"), "16 корпус" },
                    { new Guid("4bcce890-b3e1-451c-b6e6-6617b3a7094a"), "2 корпус" },
                    { new Guid("66bb0cf0-05cb-47de-854d-877f0bd22fd3"), "4 корпус" },
                    { new Guid("7505fc73-01c6-456d-abd3-14dd4a843f62"), "31 корпус" },
                    { new Guid("8662702e-1843-4d54-b31a-073cb6a8a54a"), "5 корпус" },
                    { new Guid("932a62cc-22e2-4e3b-8fa8-6bc7ef7e94a4"), "1 корпус" },
                    { new Guid("9a0d74e3-ea12-49a4-9c5f-bebf13baee81"), "34 корпус" },
                    { new Guid("9e873265-5ac8-41a3-9fe0-ff0ebe5bd959"), "29 корпус" },
                    { new Guid("a6d57755-5d55-4e20-83eb-de7d70293e6d"), "11 корпус " },
                    { new Guid("b060b282-8989-4c70-8557-55a613ad72e7"), "9 корпус" },
                    { new Guid("be5080c0-2949-4452-8567-083ab8507c30"), "32 корпус" },
                    { new Guid("c2821932-e694-4115-9ba3-84d8fb8f31b1"), "8 корпус" },
                    { new Guid("c8baeaf5-fecc-448c-b1c7-8000ff4f30b3"), "15 корпус" },
                    { new Guid("ceca1fab-9d5a-40d7-8748-10393770da1a"), "Онлайн" },
                    { new Guid("d27839eb-2f69-43c2-a59a-6840b0cbcb2d"), "21 корпус (Ботсад)" },
                    { new Guid("dc6f4e34-7b47-4f39-8843-327ccb9fbb55"), "24 корпус" },
                    { new Guid("e0f3ee23-8e68-49e2-8b39-4beeb700ce9b"), "18 корпус" },
                    { new Guid("e374ef46-abed-401a-a7b6-7b3561bf5abb"), "13 корпус (НИИББ)" },
                    { new Guid("e8c3ada9-0560-4fb2-b59d-467e5451722e"), "14 корпус" },
                    { new Guid("ec1c332b-0312-4c99-bc3f-c879f075656a"), "10 корпус" },
                    { new Guid("f26f345c-c241-44f0-a980-527cf60100bb"), "33 корпус" },
                    { new Guid("f278d59e-479e-4c73-8349-da8094e5befc"), "3 корпус" },
                    { new Guid("f6883bb1-e4ea-4f5b-b4c8-db81d9eaf02d"), "Детский сад № 49" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("06ccf9ac-179f-4b73-bd3a-65401fa0a41d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("093eb00b-9ca5-411d-8b62-88559b09e92f"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("0e5ea39c-2bcb-46bc-a94c-c7740f733ca1"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("1aae5ef0-05d0-4641-b9e6-b18b106dee76"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("26aff5be-1ea9-4e91-a349-2b8451dfc1c2"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("27f7547b-7e2e-48b2-a7d8-2ef5610d123f"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("348c0484-5dce-49ee-aa00-40da52aab306"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("4935c4ad-acb9-44e3-9ce2-418af0c31c29"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("4bcce890-b3e1-451c-b6e6-6617b3a7094a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("66bb0cf0-05cb-47de-854d-877f0bd22fd3"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("7505fc73-01c6-456d-abd3-14dd4a843f62"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("8662702e-1843-4d54-b31a-073cb6a8a54a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("932a62cc-22e2-4e3b-8fa8-6bc7ef7e94a4"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("9a0d74e3-ea12-49a4-9c5f-bebf13baee81"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("9e873265-5ac8-41a3-9fe0-ff0ebe5bd959"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("a6d57755-5d55-4e20-83eb-de7d70293e6d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("b060b282-8989-4c70-8557-55a613ad72e7"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("be5080c0-2949-4452-8567-083ab8507c30"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("c2821932-e694-4115-9ba3-84d8fb8f31b1"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("c8baeaf5-fecc-448c-b1c7-8000ff4f30b3"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ceca1fab-9d5a-40d7-8748-10393770da1a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("d27839eb-2f69-43c2-a59a-6840b0cbcb2d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("dc6f4e34-7b47-4f39-8843-327ccb9fbb55"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e0f3ee23-8e68-49e2-8b39-4beeb700ce9b"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e374ef46-abed-401a-a7b6-7b3561bf5abb"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e8c3ada9-0560-4fb2-b59d-467e5451722e"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ec1c332b-0312-4c99-bc3f-c879f075656a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f26f345c-c241-44f0-a980-527cf60100bb"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f278d59e-479e-4c73-8349-da8094e5befc"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f6883bb1-e4ea-4f5b-b4c8-db81d9eaf02d"));

            migrationBuilder.InsertData(
                table: "Buildings",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("035276eb-1500-4aa0-ac81-f6d7f162015d"), "31 корпус" },
                    { new Guid("0c6f66f1-78a5-41a2-99ba-cf8ec96dca22"), "10 корпус" },
                    { new Guid("123b11bd-d0e8-4a28-b659-099c4e692cb9"), "34 корпус" },
                    { new Guid("16a33881-1f18-4846-97c1-3427eddc0515"), "1 корпус" },
                    { new Guid("2ed33c64-e5ab-4c3b-b030-b7fe4ffcc214"), "14 корпус" },
                    { new Guid("3340f217-da0e-4a4a-8ea6-91e29036de72"), "17 корпус" },
                    { new Guid("43160f47-fadc-45ae-b3d6-af3013154f8e"), "4 корпус" },
                    { new Guid("4bdc740e-f909-4746-a818-b5617190a459"), "8 корпус" },
                    { new Guid("50a2a935-6d66-42db-9481-8aefd783caee"), "32 корпус" },
                    { new Guid("511401e0-5939-43b6-9fca-377a9d76c458"), "33 корпус" },
                    { new Guid("54edcfed-b214-4d1f-92f6-6354f916a9b3"), "7 корпус" },
                    { new Guid("69808a8b-4e4d-4d79-a839-37a41380b32d"), "6 корпус" },
                    { new Guid("6a393eb0-568d-48e8-b0b8-15bb8aa3bf7c"), "21 корпус (Ботсад)" },
                    { new Guid("6e758d7c-3a0e-45c0-882a-c839cdeea036"), "13 корпус (НИИББ)" },
                    { new Guid("751829c3-9f4e-4fbb-990a-9337f7358dc2"), "23 корпус" },
                    { new Guid("8a7439e5-2d99-469f-82cf-11d390b96b2d"), "5 корпус" },
                    { new Guid("98240eaf-b062-4de8-823f-aa8f3f4bc304"), "12 корпус" },
                    { new Guid("9dd2435b-aa5e-429b-be64-f5baa42796b6"), "Криогенный корпус" },
                    { new Guid("a295d475-aad4-4b6f-8c8f-454b997ddeb4"), "11 корпус " },
                    { new Guid("b71cccbe-d0b1-43b7-b3a1-cbf49557bbe0"), "3 корпус" },
                    { new Guid("b7f73711-2224-49a8-a43f-21b3ba8b9859"), "15 корпус" },
                    { new Guid("e047eefc-c1c1-4025-83bc-9a1f58b456f0"), "16 корпус" },
                    { new Guid("e28d418d-77f1-4e12-8b3b-9e2654df6d7a"), "Детский сад № 49" },
                    { new Guid("e49d2b42-d154-4e31-a147-36ea89b8efc2"), "9 корпус" },
                    { new Guid("e93fc5de-64ac-4655-9402-66cf6f80f465"), "24 корпус" },
                    { new Guid("ea3540a3-0d60-4662-8750-ab740a7b10b7"), "2 корпус" },
                    { new Guid("ebd57657-1a75-4018-8af5-7bcd30e7901e"), "29 корпус" },
                    { new Guid("ed8e7a8d-8d9f-40f6-b7e2-cb09ca6b4ff0"), "18 корпус" },
                    { new Guid("f782a678-b861-46c5-8c62-29ca226c63b0"), "Онлайн" },
                    { new Guid("ff23b3d5-85dd-48be-9c44-c6129d7fab7f"), "20 корпус" }
                });
        }
    }
}
