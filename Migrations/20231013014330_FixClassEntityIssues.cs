﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class FixClassEntityIssues : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Auditoria_AuditoriumId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Professors_ProfessorId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Subjects_SubjectId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_TimeSlots_TimeSlotIndex",
                table: "ScheduleEvents");

            migrationBuilder.DropTable(
                name: "ClassBaseEntityStudentGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ScheduleEvents",
                table: "ScheduleEvents");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("1673a08b-bab1-4d30-95c3-e5f5502adb37"));

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "ScheduleEvents");

            migrationBuilder.RenameTable(
                name: "ScheduleEvents",
                newName: "Classes");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_TimeSlotIndex",
                table: "Classes",
                newName: "IX_Classes_TimeSlotIndex");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_SubjectId",
                table: "Classes",
                newName: "IX_Classes_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_ProfessorId",
                table: "Classes",
                newName: "IX_Classes_ProfessorId");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_AuditoriumId",
                table: "Classes",
                newName: "IX_Classes_AuditoriumId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Classes",
                table: "Classes",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ClassStudentGroup",
                columns: table => new
                {
                    ClassId = table.Column<Guid>(type: "uuid", nullable: false),
                    StudentGroupsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassStudentGroup", x => new { x.ClassId, x.StudentGroupsId });
                    table.ForeignKey(
                        name: "FK_ClassStudentGroup_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassStudentGroup_StudentGroups_StudentGroupsId",
                        column: x => x.StudentGroupsId,
                        principalTable: "StudentGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("83044b0f-1883-4fc8-93e4-8e1243750455"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });

            migrationBuilder.CreateIndex(
                name: "IX_ClassStudentGroup_StudentGroupsId",
                table: "ClassStudentGroup",
                column: "StudentGroupsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Auditoria_AuditoriumId",
                table: "Classes",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Professors_ProfessorId",
                table: "Classes",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Subjects_SubjectId",
                table: "Classes",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_TimeSlots_TimeSlotIndex",
                table: "Classes",
                column: "TimeSlotIndex",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Auditoria_AuditoriumId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Professors_ProfessorId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Subjects_SubjectId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_TimeSlots_TimeSlotIndex",
                table: "Classes");

            migrationBuilder.DropTable(
                name: "ClassStudentGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Classes",
                table: "Classes");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("83044b0f-1883-4fc8-93e4-8e1243750455"));

            migrationBuilder.RenameTable(
                name: "Classes",
                newName: "ScheduleEvents");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_TimeSlotIndex",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_TimeSlotIndex");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_SubjectId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_ProfessorId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_ProfessorId");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_AuditoriumId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_AuditoriumId");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "ScheduleEvents",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ScheduleEvents",
                table: "ScheduleEvents",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ClassBaseEntityStudentGroup",
                columns: table => new
                {
                    ClassBaseEntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    StudentGroupsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassBaseEntityStudentGroup", x => new { x.ClassBaseEntityId, x.StudentGroupsId });
                    table.ForeignKey(
                        name: "FK_ClassBaseEntityStudentGroup_ScheduleEvents_ClassBaseEntityId",
                        column: x => x.ClassBaseEntityId,
                        principalTable: "ScheduleEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassBaseEntityStudentGroup_StudentGroups_StudentGroupsId",
                        column: x => x.StudentGroupsId,
                        principalTable: "StudentGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("1673a08b-bab1-4d30-95c3-e5f5502adb37"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });

            migrationBuilder.CreateIndex(
                name: "IX_ClassBaseEntityStudentGroup_StudentGroupsId",
                table: "ClassBaseEntityStudentGroup",
                column: "StudentGroupsId");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Auditoria_AuditoriumId",
                table: "ScheduleEvents",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Professors_ProfessorId",
                table: "ScheduleEvents",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Subjects_SubjectId",
                table: "ScheduleEvents",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_TimeSlots_TimeSlotIndex",
                table: "ScheduleEvents",
                column: "TimeSlotIndex",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
