﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class ChangeReservationModelToMinified : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Users_CreatedById",
                table: "ScheduleEvents");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleEvents_CreatedById",
                table: "ScheduleEvents");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "ScheduleEvents");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "ScheduleEvents");

            migrationBuilder.DropColumn(
                name: "EmailSubscription",
                table: "ScheduleEvents");

            migrationBuilder.CreateTable(
                name: "Reservations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatorUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Details = table.Column<string>(type: "text", nullable: true),
                    Date = table.Column<DateTime>(type: "date", nullable: false),
                    TimeSlotId = table.Column<int>(type: "integer", nullable: false),
                    AuditoriumId = table.Column<Guid>(type: "uuid", nullable: false),
                    EmailSubscription = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reservations_Auditoria_AuditoriumId",
                        column: x => x.AuditoriumId,
                        principalTable: "Auditoria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reservations_TimeSlots_TimeSlotId",
                        column: x => x.TimeSlotId,
                        principalTable: "TimeSlots",
                        principalColumn: "Index",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reservations_Users_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_AuditoriumId",
                table: "Reservations",
                column: "AuditoriumId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_CreatorUserId",
                table: "Reservations",
                column: "CreatorUserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_TimeSlotId",
                table: "Reservations",
                column: "TimeSlotId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reservations");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "ScheduleEvents",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "ScheduleEvents",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EmailSubscription",
                table: "ScheduleEvents",
                type: "boolean",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleEvents_CreatedById",
                table: "ScheduleEvents",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Users_CreatedById",
                table: "ScheduleEvents",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
