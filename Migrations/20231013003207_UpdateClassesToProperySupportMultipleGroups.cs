﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class UpdateClassesToProperySupportMultipleGroups : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_StudentGroups_StudentGroupId",
                table: "ScheduleEvents");

            migrationBuilder.DropIndex(
                name: "IX_ScheduleEvents_StudentGroupId",
                table: "ScheduleEvents");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("709cbc95-c63b-4fe6-80b2-3d8b79951089"));

            migrationBuilder.DropColumn(
                name: "StudentGroupId",
                table: "ScheduleEvents");

            migrationBuilder.CreateTable(
                name: "ClassBaseEntityStudentGroup",
                columns: table => new
                {
                    ClassBaseEntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    StudentGroupsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassBaseEntityStudentGroup", x => new { x.ClassBaseEntityId, x.StudentGroupsId });
                    table.ForeignKey(
                        name: "FK_ClassBaseEntityStudentGroup_ScheduleEvents_ClassBaseEntityId",
                        column: x => x.ClassBaseEntityId,
                        principalTable: "ScheduleEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassBaseEntityStudentGroup_StudentGroups_StudentGroupsId",
                        column: x => x.StudentGroupsId,
                        principalTable: "StudentGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("1673a08b-bab1-4d30-95c3-e5f5502adb37"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });

            migrationBuilder.CreateIndex(
                name: "IX_ClassBaseEntityStudentGroup_StudentGroupsId",
                table: "ClassBaseEntityStudentGroup",
                column: "StudentGroupsId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClassBaseEntityStudentGroup");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("1673a08b-bab1-4d30-95c3-e5f5502adb37"));

            migrationBuilder.AddColumn<Guid>(
                name: "StudentGroupId",
                table: "ScheduleEvents",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("709cbc95-c63b-4fe6-80b2-3d8b79951089"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleEvents_StudentGroupId",
                table: "ScheduleEvents",
                column: "StudentGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_StudentGroups_StudentGroupId",
                table: "ScheduleEvents",
                column: "StudentGroupId",
                principalTable: "StudentGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
