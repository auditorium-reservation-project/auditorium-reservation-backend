﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class UpdateModelHierarchyForSynchronizationCompatibility : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Users_ProfessorId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_FacultyProfessor_Users_ProfessorsId",
                table: "FacultyProfessor");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorRecords_Users_ProfessorId",
                table: "ProfessorRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSlots_Classes_ClassId",
                table: "TimeSlot");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TimeSlots",
                table: "TimeSlot");

            migrationBuilder.DropIndex(
                name: "IX_TimeSlots_ClassId",
                table: "TimeSlot");

            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "TimeSlot");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "TimeSlot");

            migrationBuilder.RenameTable(
                name: "TimeSlot",
                newName: "TimeSlots");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "Classes",
                newName: "Date");

            migrationBuilder.RenameColumn(
                name: "Day",
                table: "TimeSlots",
                newName: "Index");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<Guid>(
                name: "LinkedUserId",
                table: "Staff",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Classes",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TimeSlotIndex",
                table: "Classes",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Classes",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "Index",
                table: "TimeSlots",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TimeSlots",
                table: "TimeSlots",
                column: "Index");

            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    LinkedUserId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Professors_Users_LinkedUserId",
                        column: x => x.LinkedUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "TimeSlots",
                columns: new[] { "Index", "EndTime", "StartTime" },
                values: new object[,]
                {
                    { 1, new TimeSpan(0, 10, 20, 0, 0), new TimeSpan(0, 8, 45, 0, 0) },
                    { 2, new TimeSpan(0, 12, 10, 0, 0), new TimeSpan(0, 10, 35, 0, 0) },
                    { 3, new TimeSpan(0, 14, 0, 0, 0), new TimeSpan(0, 12, 25, 0, 0) },
                    { 4, new TimeSpan(0, 16, 20, 0, 0), new TimeSpan(0, 14, 45, 0, 0) },
                    { 5, new TimeSpan(0, 18, 10, 0, 0), new TimeSpan(0, 16, 35, 0, 0) },
                    { 6, new TimeSpan(0, 20, 0, 0, 0), new TimeSpan(0, 18, 25, 0, 0) },
                    { 7, new TimeSpan(0, 21, 50, 0, 0), new TimeSpan(0, 20, 15, 0, 0) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Staff_LinkedUserId",
                table: "Staff",
                column: "LinkedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_TimeSlotIndex",
                table: "Classes",
                column: "TimeSlotIndex");

            migrationBuilder.CreateIndex(
                name: "IX_Professors_LinkedUserId",
                table: "Professors",
                column: "LinkedUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Professors_ProfessorId",
                table: "Classes",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_TimeSlots_TimeSlotIndex",
                table: "Classes",
                column: "TimeSlotIndex",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FacultyProfessor_Professors_ProfessorsId",
                table: "FacultyProfessor",
                column: "ProfessorsId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorRecords_Professors_ProfessorId",
                table: "ProfessorRecords",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Users_LinkedUserId",
                table: "Staff",
                column: "LinkedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Professors_ProfessorId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_TimeSlots_TimeSlotIndex",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_FacultyProfessor_Professors_ProfessorsId",
                table: "FacultyProfessor");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorRecords_Professors_ProfessorId",
                table: "ProfessorRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Users_LinkedUserId",
                table: "Staff");

            migrationBuilder.DropTable(
                name: "Professors");

            migrationBuilder.DropIndex(
                name: "IX_Staff_LinkedUserId",
                table: "Staff");

            migrationBuilder.DropIndex(
                name: "IX_Classes_TimeSlotIndex",
                table: "Classes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TimeSlots",
                table: "TimeSlots");

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "TimeSlots",
                keyColumn: "Index",
                keyValue: 7);

            migrationBuilder.DropColumn(
                name: "LinkedUserId",
                table: "Staff");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "TimeSlotIndex",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Classes");

            migrationBuilder.RenameTable(
                name: "TimeSlots",
                newName: "TimeSlot");

            migrationBuilder.RenameColumn(
                name: "Date",
                table: "Classes",
                newName: "StartDate");

            migrationBuilder.RenameColumn(
                name: "Index",
                table: "TimeSlot",
                newName: "Day");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountType",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "Classes",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<int>(
                name: "Day",
                table: "TimeSlot",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "TimeSlot",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ClassId",
                table: "TimeSlot",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TimeSlot",
                table: "TimeSlot",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSlots_ClassId",
                table: "TimeSlot",
                column: "ClassId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Users_ProfessorId",
                table: "Classes",
                column: "ProfessorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FacultyProfessor_Users_ProfessorsId",
                table: "FacultyProfessor",
                column: "ProfessorsId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorRecords_Users_ProfessorId",
                table: "ProfessorRecords",
                column: "ProfessorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSlots_Classes_ClassId",
                table: "TimeSlot",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id");
        }
    }
}
