﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class DatetimeConversionFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("83044b0f-1883-4fc8-93e4-8e1243750455"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("d776313d-124c-4753-987d-150e1f8aa74e"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("d776313d-124c-4753-987d-150e1f8aa74e"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("83044b0f-1883-4fc8-93e4-8e1243750455"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });
        }
    }
}
