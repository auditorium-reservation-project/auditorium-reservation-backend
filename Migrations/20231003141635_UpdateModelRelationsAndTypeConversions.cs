﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class UpdateModelRelationsAndTypeConversions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Auditories_AuditoriumId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Faculty_Users_ProfessorId",
                table: "Faculty");

            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_Auditories_AuditoriumId",
                table: "Reservations");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Faculty_FacultyId",
                table: "Staff");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentGroup_Faculty_FacultyId",
                table: "StudentGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Faculty",
                table: "Faculty");

            migrationBuilder.DropIndex(
                name: "IX_Faculty_ProfessorId",
                table: "Faculty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Auditories",
                table: "Auditories");

            migrationBuilder.DropIndex(
                name: "IX_Auditories_number",
                table: "Auditories");

            migrationBuilder.DropColumn(
                name: "ProfessorId",
                table: "Faculty");

            migrationBuilder.DropColumn(
                name: "number",
                table: "Auditories");

            migrationBuilder.RenameTable(
                name: "Faculty",
                newName: "Faculties");

            migrationBuilder.RenameTable(
                name: "Auditories",
                newName: "Auditoria");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "StartTime",
                table: "TimeSlot",
                type: "time",
                nullable: false,
                oldClrType: typeof(TimeOnly),
                oldType: "time without time zone");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "EndTime",
                table: "TimeSlot",
                type: "time",
                nullable: false,
                oldClrType: typeof(TimeOnly),
                oldType: "time without time zone");

            migrationBuilder.AddColumn<Guid>(
                name: "BuildingId",
                table: "Auditoria",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Auditoria",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Faculties",
                table: "Faculties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Auditoria",
                table: "Auditoria",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "AuditoriaRecords",
                columns: table => new
                {
                    RemoteId = table.Column<string>(type: "text", nullable: false),
                    AuditoriumId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditoriaRecords", x => x.RemoteId);
                    table.ForeignKey(
                        name: "FK_AuditoriaRecords_Auditoria_AuditoriumId",
                        column: x => x.AuditoriumId,
                        principalTable: "Auditoria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Buildings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buildings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FacultyProfessor",
                columns: table => new
                {
                    FacultiesId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProfessorsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacultyProfessor", x => new { x.FacultiesId, x.ProfessorsId });
                    table.ForeignKey(
                        name: "FK_FacultyProfessor_Faculties_FacultiesId",
                        column: x => x.FacultiesId,
                        principalTable: "Faculties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FacultyProfessor_Users_ProfessorsId",
                        column: x => x.ProfessorsId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FacultyRecords",
                columns: table => new
                {
                    RemoteId = table.Column<string>(type: "text", nullable: false),
                    FacultyId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacultyRecords", x => x.RemoteId);
                    table.ForeignKey(
                        name: "FK_FacultyRecords_Faculties_FacultyId",
                        column: x => x.FacultyId,
                        principalTable: "Faculties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProfessorRecords",
                columns: table => new
                {
                    RemoteId = table.Column<string>(type: "text", nullable: false),
                    ProfessorId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfessorRecords", x => x.RemoteId);
                    table.ForeignKey(
                        name: "FK_ProfessorRecords_Users_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingsRecords",
                columns: table => new
                {
                    RemoteId = table.Column<string>(type: "text", nullable: false),
                    BuildingId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingsRecords", x => x.RemoteId);
                    table.ForeignKey(
                        name: "FK_BuildingsRecords_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Auditoria_BuildingId",
                table: "Auditoria",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_AuditoriaRecords_AuditoriumId",
                table: "AuditoriaRecords",
                column: "AuditoriumId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingsRecords_BuildingId",
                table: "BuildingsRecords",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_FacultyProfessor_ProfessorsId",
                table: "FacultyProfessor",
                column: "ProfessorsId");

            migrationBuilder.CreateIndex(
                name: "IX_FacultyRecords_FacultyId",
                table: "FacultyRecords",
                column: "FacultyId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorRecords_ProfessorId",
                table: "ProfessorRecords",
                column: "ProfessorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Auditoria_Buildings_BuildingId",
                table: "Auditoria",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Auditoria_AuditoriumId",
                table: "Classes",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_Auditoria_AuditoriumId",
                table: "Reservations",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Faculties_FacultyId",
                table: "Staff",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentGroup_Faculties_FacultyId",
                table: "StudentGroup",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Auditoria_Buildings_BuildingId",
                table: "Auditoria");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Auditoria_AuditoriumId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_Auditoria_AuditoriumId",
                table: "Reservations");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Faculties_FacultyId",
                table: "Staff");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentGroup_Faculties_FacultyId",
                table: "StudentGroup");

            migrationBuilder.DropTable(
                name: "AuditoriaRecords");

            migrationBuilder.DropTable(
                name: "BuildingsRecords");

            migrationBuilder.DropTable(
                name: "FacultyProfessor");

            migrationBuilder.DropTable(
                name: "FacultyRecords");

            migrationBuilder.DropTable(
                name: "ProfessorRecords");

            migrationBuilder.DropTable(
                name: "Buildings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Faculties",
                table: "Faculties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Auditoria",
                table: "Auditoria");

            migrationBuilder.DropIndex(
                name: "IX_Auditoria_BuildingId",
                table: "Auditoria");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "Auditoria");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Auditoria");

            migrationBuilder.RenameTable(
                name: "Faculties",
                newName: "Faculty");

            migrationBuilder.RenameTable(
                name: "Auditoria",
                newName: "Auditories");

            migrationBuilder.AlterColumn<TimeOnly>(
                name: "StartTime",
                table: "TimeSlot",
                type: "time without time zone",
                nullable: false,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AlterColumn<TimeOnly>(
                name: "EndTime",
                table: "TimeSlot",
                type: "time without time zone",
                nullable: false,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AddColumn<Guid>(
                name: "ProfessorId",
                table: "Faculty",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "number",
                table: "Auditories",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Faculty",
                table: "Faculty",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Auditories",
                table: "Auditories",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Faculty_ProfessorId",
                table: "Faculty",
                column: "ProfessorId");

            migrationBuilder.CreateIndex(
                name: "IX_Auditories_number",
                table: "Auditories",
                column: "number",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Auditories_AuditoriumId",
                table: "Classes",
                column: "AuditoriumId",
                principalTable: "Auditories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Faculty_Users_ProfessorId",
                table: "Faculty",
                column: "ProfessorId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_Auditories_AuditoriumId",
                table: "Reservations",
                column: "AuditoriumId",
                principalTable: "Auditories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Faculty_FacultyId",
                table: "Staff",
                column: "FacultyId",
                principalTable: "Faculty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentGroup_Faculty_FacultyId",
                table: "StudentGroup",
                column: "FacultyId",
                principalTable: "Faculty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
