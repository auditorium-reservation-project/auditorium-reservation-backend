﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class Update : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_StudentGroup_StudentGroupId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentGroup_Faculties_FacultyId",
                table: "StudentGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentGroup",
                table: "StudentGroup");

            migrationBuilder.RenameTable(
                name: "StudentGroup",
                newName: "StudentGroups");

            migrationBuilder.RenameIndex(
                name: "IX_StudentGroup_FacultyId",
                table: "StudentGroups",
                newName: "IX_StudentGroups_FacultyId");

            migrationBuilder.AddColumn<Guid>(
                name: "GroupId",
                table: "Users",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentGroups",
                table: "StudentGroups",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Buildings",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("035276eb-1500-4aa0-ac81-f6d7f162015d"), "31 корпус" },
                    { new Guid("0c6f66f1-78a5-41a2-99ba-cf8ec96dca22"), "10 корпус" },
                    { new Guid("123b11bd-d0e8-4a28-b659-099c4e692cb9"), "34 корпус" },
                    { new Guid("16a33881-1f18-4846-97c1-3427eddc0515"), "1 корпус" },
                    { new Guid("2ed33c64-e5ab-4c3b-b030-b7fe4ffcc214"), "14 корпус" },
                    { new Guid("3340f217-da0e-4a4a-8ea6-91e29036de72"), "17 корпус" },
                    { new Guid("43160f47-fadc-45ae-b3d6-af3013154f8e"), "4 корпус" },
                    { new Guid("4bdc740e-f909-4746-a818-b5617190a459"), "8 корпус" },
                    { new Guid("50a2a935-6d66-42db-9481-8aefd783caee"), "32 корпус" },
                    { new Guid("511401e0-5939-43b6-9fca-377a9d76c458"), "33 корпус" },
                    { new Guid("54edcfed-b214-4d1f-92f6-6354f916a9b3"), "7 корпус" },
                    { new Guid("69808a8b-4e4d-4d79-a839-37a41380b32d"), "6 корпус" },
                    { new Guid("6a393eb0-568d-48e8-b0b8-15bb8aa3bf7c"), "21 корпус (Ботсад)" },
                    { new Guid("6e758d7c-3a0e-45c0-882a-c839cdeea036"), "13 корпус (НИИББ)" },
                    { new Guid("751829c3-9f4e-4fbb-990a-9337f7358dc2"), "23 корпус" },
                    { new Guid("8a7439e5-2d99-469f-82cf-11d390b96b2d"), "5 корпус" },
                    { new Guid("98240eaf-b062-4de8-823f-aa8f3f4bc304"), "12 корпус" },
                    { new Guid("9dd2435b-aa5e-429b-be64-f5baa42796b6"), "Криогенный корпус" },
                    { new Guid("a295d475-aad4-4b6f-8c8f-454b997ddeb4"), "11 корпус " },
                    { new Guid("b71cccbe-d0b1-43b7-b3a1-cbf49557bbe0"), "3 корпус" },
                    { new Guid("b7f73711-2224-49a8-a43f-21b3ba8b9859"), "15 корпус" },
                    { new Guid("e047eefc-c1c1-4025-83bc-9a1f58b456f0"), "16 корпус" },
                    { new Guid("e28d418d-77f1-4e12-8b3b-9e2654df6d7a"), "Детский сад № 49" },
                    { new Guid("e49d2b42-d154-4e31-a147-36ea89b8efc2"), "9 корпус" },
                    { new Guid("e93fc5de-64ac-4655-9402-66cf6f80f465"), "24 корпус" },
                    { new Guid("ea3540a3-0d60-4662-8750-ab740a7b10b7"), "2 корпус" },
                    { new Guid("ebd57657-1a75-4018-8af5-7bcd30e7901e"), "29 корпус" },
                    { new Guid("ed8e7a8d-8d9f-40f6-b7e2-cb09ca6b4ff0"), "18 корпус" },
                    { new Guid("f782a678-b861-46c5-8c62-29ca226c63b0"), "Онлайн" },
                    { new Guid("ff23b3d5-85dd-48be-9c44-c6129d7fab7f"), "20 корпус" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_GroupId",
                table: "Users",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_StudentGroups_StudentGroupId",
                table: "Classes",
                column: "StudentGroupId",
                principalTable: "StudentGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentGroups_Faculties_FacultyId",
                table: "StudentGroups",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_StudentGroups_GroupId",
                table: "Users",
                column: "GroupId",
                principalTable: "StudentGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classes_StudentGroups_StudentGroupId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentGroups_Faculties_FacultyId",
                table: "StudentGroups");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_StudentGroups_GroupId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_GroupId",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentGroups",
                table: "StudentGroups");

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("035276eb-1500-4aa0-ac81-f6d7f162015d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("0c6f66f1-78a5-41a2-99ba-cf8ec96dca22"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("123b11bd-d0e8-4a28-b659-099c4e692cb9"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("16a33881-1f18-4846-97c1-3427eddc0515"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("2ed33c64-e5ab-4c3b-b030-b7fe4ffcc214"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("3340f217-da0e-4a4a-8ea6-91e29036de72"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("43160f47-fadc-45ae-b3d6-af3013154f8e"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("4bdc740e-f909-4746-a818-b5617190a459"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("50a2a935-6d66-42db-9481-8aefd783caee"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("511401e0-5939-43b6-9fca-377a9d76c458"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("54edcfed-b214-4d1f-92f6-6354f916a9b3"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("69808a8b-4e4d-4d79-a839-37a41380b32d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("6a393eb0-568d-48e8-b0b8-15bb8aa3bf7c"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("6e758d7c-3a0e-45c0-882a-c839cdeea036"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("751829c3-9f4e-4fbb-990a-9337f7358dc2"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("8a7439e5-2d99-469f-82cf-11d390b96b2d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("98240eaf-b062-4de8-823f-aa8f3f4bc304"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("9dd2435b-aa5e-429b-be64-f5baa42796b6"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("a295d475-aad4-4b6f-8c8f-454b997ddeb4"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("b71cccbe-d0b1-43b7-b3a1-cbf49557bbe0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("b7f73711-2224-49a8-a43f-21b3ba8b9859"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e047eefc-c1c1-4025-83bc-9a1f58b456f0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e28d418d-77f1-4e12-8b3b-9e2654df6d7a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e49d2b42-d154-4e31-a147-36ea89b8efc2"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e93fc5de-64ac-4655-9402-66cf6f80f465"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ea3540a3-0d60-4662-8750-ab740a7b10b7"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ebd57657-1a75-4018-8af5-7bcd30e7901e"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ed8e7a8d-8d9f-40f6-b7e2-cb09ca6b4ff0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f782a678-b861-46c5-8c62-29ca226c63b0"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ff23b3d5-85dd-48be-9c44-c6129d7fab7f"));

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Users");

            migrationBuilder.RenameTable(
                name: "StudentGroups",
                newName: "StudentGroup");

            migrationBuilder.RenameIndex(
                name: "IX_StudentGroups_FacultyId",
                table: "StudentGroup",
                newName: "IX_StudentGroup_FacultyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentGroup",
                table: "StudentGroup",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_StudentGroup_StudentGroupId",
                table: "Classes",
                column: "StudentGroupId",
                principalTable: "StudentGroup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentGroup_Faculties_FacultyId",
                table: "StudentGroup",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
