﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class FixIndexesForReservations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_TimeSlots_TimeSlotId",
                table: "Reservations");

            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_Users_CreatorUserId",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_AuditoriumId",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_CreatorUserId",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_TimeSlotId",
                table: "Reservations");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("d776313d-124c-4753-987d-150e1f8aa74e"));

            migrationBuilder.RenameColumn(
                name: "TimeSlotId",
                table: "Reservations",
                newName: "TimeSlotIndex");

            migrationBuilder.RenameColumn(
                name: "CreatorUserId",
                table: "Reservations",
                newName: "CreatedById");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("dfda1520-cc0c-4f78-8d96-080763bd4e5d"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_AuditoriumId",
                table: "Reservations",
                column: "AuditoriumId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_CreatedById",
                table: "Reservations",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_TimeSlotIndex",
                table: "Reservations",
                column: "TimeSlotIndex");

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_TimeSlots_TimeSlotIndex",
                table: "Reservations",
                column: "TimeSlotIndex",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_Users_CreatedById",
                table: "Reservations",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_TimeSlots_TimeSlotIndex",
                table: "Reservations");

            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_Users_CreatedById",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_AuditoriumId",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_CreatedById",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_TimeSlotIndex",
                table: "Reservations");

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("dfda1520-cc0c-4f78-8d96-080763bd4e5d"));

            migrationBuilder.RenameColumn(
                name: "TimeSlotIndex",
                table: "Reservations",
                newName: "TimeSlotId");

            migrationBuilder.RenameColumn(
                name: "CreatedById",
                table: "Reservations",
                newName: "CreatorUserId");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarUrl", "Email", "FirstName", "LastName", "MiddleName", "Password", "Roles" },
                values: new object[] { new Guid("d776313d-124c-4753-987d-150e1f8aa74e"), "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/220px-Cat03.jpg", "root@root.net", "Root", null, null, "HochuSpat", "[\"Admin\",\"Staff\",\"Student\"]" });

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_AuditoriumId",
                table: "Reservations",
                column: "AuditoriumId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_CreatorUserId",
                table: "Reservations",
                column: "CreatorUserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_TimeSlotId",
                table: "Reservations",
                column: "TimeSlotId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_TimeSlots_TimeSlotId",
                table: "Reservations",
                column: "TimeSlotId",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_Users_CreatorUserId",
                table: "Reservations",
                column: "CreatorUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
