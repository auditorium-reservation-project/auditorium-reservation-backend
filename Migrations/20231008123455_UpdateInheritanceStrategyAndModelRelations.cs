﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace AuditoriumReservation.Migrations
{
    /// <inheritdoc />
    public partial class UpdateInheritanceStrategyAndModelRelations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuditoriaRecords_Auditoria_AuditoriumId",
                table: "AuditoriaRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingsRecords_Buildings_BuildingId",
                table: "BuildingsRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Auditoria_AuditoriumId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Professors_ProfessorId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_StudentGroups_StudentGroupId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Subject_SubjectId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_TimeSlots_TimeSlotIndex",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Users_CreatedById",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_FacultyRecords_Faculties_FacultyId",
                table: "FacultyRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorRecords_Professors_ProfessorId",
                table: "ProfessorRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Users_LinkedUserId",
                table: "Professors");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Faculties_FacultyId",
                table: "Staff");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Users_LinkedUserId",
                table: "Staff");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_StudentGroups_GroupId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_GroupId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Staff_LinkedUserId",
                table: "Staff");

            migrationBuilder.DropIndex(
                name: "IX_Professors_LinkedUserId",
                table: "Professors");

            migrationBuilder.DropIndex(
                name: "IX_ProfessorRecords_ProfessorId",
                table: "ProfessorRecords");

            migrationBuilder.DropIndex(
                name: "IX_FacultyRecords_FacultyId",
                table: "FacultyRecords");

            migrationBuilder.DropIndex(
                name: "IX_BuildingsRecords_BuildingId",
                table: "BuildingsRecords");

            migrationBuilder.DropIndex(
                name: "IX_AuditoriaRecords_AuditoriumId",
                table: "AuditoriaRecords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subject",
                table: "Subject");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Classes",
                table: "Classes");

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("06ccf9ac-179f-4b73-bd3a-65401fa0a41d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("093eb00b-9ca5-411d-8b62-88559b09e92f"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("0e5ea39c-2bcb-46bc-a94c-c7740f733ca1"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("1aae5ef0-05d0-4641-b9e6-b18b106dee76"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("26aff5be-1ea9-4e91-a349-2b8451dfc1c2"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("27f7547b-7e2e-48b2-a7d8-2ef5610d123f"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("348c0484-5dce-49ee-aa00-40da52aab306"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("4935c4ad-acb9-44e3-9ce2-418af0c31c29"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("4bcce890-b3e1-451c-b6e6-6617b3a7094a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("66bb0cf0-05cb-47de-854d-877f0bd22fd3"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("7505fc73-01c6-456d-abd3-14dd4a843f62"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("8662702e-1843-4d54-b31a-073cb6a8a54a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("932a62cc-22e2-4e3b-8fa8-6bc7ef7e94a4"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("9a0d74e3-ea12-49a4-9c5f-bebf13baee81"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("9e873265-5ac8-41a3-9fe0-ff0ebe5bd959"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("a6d57755-5d55-4e20-83eb-de7d70293e6d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("b060b282-8989-4c70-8557-55a613ad72e7"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("be5080c0-2949-4452-8567-083ab8507c30"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("c2821932-e694-4115-9ba3-84d8fb8f31b1"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("c8baeaf5-fecc-448c-b1c7-8000ff4f30b3"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ceca1fab-9d5a-40d7-8748-10393770da1a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("d27839eb-2f69-43c2-a59a-6840b0cbcb2d"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("dc6f4e34-7b47-4f39-8843-327ccb9fbb55"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e0f3ee23-8e68-49e2-8b39-4beeb700ce9b"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e374ef46-abed-401a-a7b6-7b3561bf5abb"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("e8c3ada9-0560-4fb2-b59d-467e5451722e"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("ec1c332b-0312-4c99-bc3f-c879f075656a"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f26f345c-c241-44f0-a980-527cf60100bb"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f278d59e-479e-4c73-8349-da8094e5befc"));

            migrationBuilder.DeleteData(
                table: "Buildings",
                keyColumn: "Id",
                keyValue: new Guid("f6883bb1-e4ea-4f5b-b4c8-db81d9eaf02d"));

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "LinkedUserId",
                table: "Professors");

            migrationBuilder.DropColumn(
                name: "ProfessorId",
                table: "ProfessorRecords");

            migrationBuilder.DropColumn(
                name: "FacultyId",
                table: "FacultyRecords");

            migrationBuilder.DropColumn(
                name: "BuildingId",
                table: "BuildingsRecords");

            migrationBuilder.DropColumn(
                name: "AuditoriumId",
                table: "AuditoriaRecords");

            migrationBuilder.RenameTable(
                name: "Subject",
                newName: "Subjects");

            migrationBuilder.RenameTable(
                name: "Classes",
                newName: "ScheduleEvents");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_TimeSlotIndex",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_TimeSlotIndex");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_SubjectId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_StudentGroupId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_StudentGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_ProfessorId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_ProfessorId");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_CreatedById",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_CreatedById");

            migrationBuilder.RenameIndex(
                name: "IX_Classes_AuditoriumId",
                table: "ScheduleEvents",
                newName: "IX_ScheduleEvents_AuditoriumId");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LinkedUserId",
                table: "Staff",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "FacultyId",
                table: "Staff",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<Guid>(
                name: "RelatedEntityId",
                table: "ProfessorRecords",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RelatedEntityId",
                table: "FacultyRecords",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RelatedEntityId",
                table: "BuildingsRecords",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RelatedEntityId",
                table: "AuditoriaRecords",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EmailSubscription",
                table: "ScheduleEvents",
                type: "boolean",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subjects",
                table: "Subjects",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ScheduleEvents",
                table: "ScheduleEvents",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "StudentGroupUser",
                columns: table => new
                {
                    RelatedGroupsId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentGroupUser", x => new { x.RelatedGroupsId, x.UserId });
                    table.ForeignKey(
                        name: "FK_StudentGroupUser_StudentGroups_RelatedGroupsId",
                        column: x => x.RelatedGroupsId,
                        principalTable: "StudentGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentGroupUser_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectRecords",
                columns: table => new
                {
                    RemoteId = table.Column<string>(type: "text", nullable: false),
                    RelatedEntityId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectRecords", x => x.RemoteId);
                    table.ForeignKey(
                        name: "FK_SubjectRecords_Subjects_RelatedEntityId",
                        column: x => x.RelatedEntityId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Staff_LinkedUserId",
                table: "Staff",
                column: "LinkedUserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorRecords_RelatedEntityId",
                table: "ProfessorRecords",
                column: "RelatedEntityId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FacultyRecords_RelatedEntityId",
                table: "FacultyRecords",
                column: "RelatedEntityId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingsRecords_RelatedEntityId",
                table: "BuildingsRecords",
                column: "RelatedEntityId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuditoriaRecords_RelatedEntityId",
                table: "AuditoriaRecords",
                column: "RelatedEntityId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentGroupUser_UserId",
                table: "StudentGroupUser",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectRecords_RelatedEntityId",
                table: "SubjectRecords",
                column: "RelatedEntityId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AuditoriaRecords_Auditoria_RelatedEntityId",
                table: "AuditoriaRecords",
                column: "RelatedEntityId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingsRecords_Buildings_RelatedEntityId",
                table: "BuildingsRecords",
                column: "RelatedEntityId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FacultyRecords_Faculties_RelatedEntityId",
                table: "FacultyRecords",
                column: "RelatedEntityId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorRecords_Professors_RelatedEntityId",
                table: "ProfessorRecords",
                column: "RelatedEntityId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Users_Id",
                table: "Professors",
                column: "Id",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Auditoria_AuditoriumId",
                table: "ScheduleEvents",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Professors_ProfessorId",
                table: "ScheduleEvents",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_StudentGroups_StudentGroupId",
                table: "ScheduleEvents",
                column: "StudentGroupId",
                principalTable: "StudentGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Subjects_SubjectId",
                table: "ScheduleEvents",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_TimeSlots_TimeSlotIndex",
                table: "ScheduleEvents",
                column: "TimeSlotIndex",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleEvents_Users_CreatedById",
                table: "ScheduleEvents",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Faculties_FacultyId",
                table: "Staff",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Professors_Id",
                table: "Staff",
                column: "Id",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Users_LinkedUserId",
                table: "Staff",
                column: "LinkedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuditoriaRecords_Auditoria_RelatedEntityId",
                table: "AuditoriaRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_BuildingsRecords_Buildings_RelatedEntityId",
                table: "BuildingsRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_FacultyRecords_Faculties_RelatedEntityId",
                table: "FacultyRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorRecords_Professors_RelatedEntityId",
                table: "ProfessorRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Users_Id",
                table: "Professors");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Auditoria_AuditoriumId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Professors_ProfessorId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_StudentGroups_StudentGroupId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Subjects_SubjectId",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_TimeSlots_TimeSlotIndex",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleEvents_Users_CreatedById",
                table: "ScheduleEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Faculties_FacultyId",
                table: "Staff");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Professors_Id",
                table: "Staff");

            migrationBuilder.DropForeignKey(
                name: "FK_Staff_Users_LinkedUserId",
                table: "Staff");

            migrationBuilder.DropTable(
                name: "StudentGroupUser");

            migrationBuilder.DropTable(
                name: "SubjectRecords");

            migrationBuilder.DropIndex(
                name: "IX_Staff_LinkedUserId",
                table: "Staff");

            migrationBuilder.DropIndex(
                name: "IX_ProfessorRecords_RelatedEntityId",
                table: "ProfessorRecords");

            migrationBuilder.DropIndex(
                name: "IX_FacultyRecords_RelatedEntityId",
                table: "FacultyRecords");

            migrationBuilder.DropIndex(
                name: "IX_BuildingsRecords_RelatedEntityId",
                table: "BuildingsRecords");

            migrationBuilder.DropIndex(
                name: "IX_AuditoriaRecords_RelatedEntityId",
                table: "AuditoriaRecords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subjects",
                table: "Subjects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ScheduleEvents",
                table: "ScheduleEvents");

            migrationBuilder.DropColumn(
                name: "RelatedEntityId",
                table: "ProfessorRecords");

            migrationBuilder.DropColumn(
                name: "RelatedEntityId",
                table: "FacultyRecords");

            migrationBuilder.DropColumn(
                name: "RelatedEntityId",
                table: "BuildingsRecords");

            migrationBuilder.DropColumn(
                name: "RelatedEntityId",
                table: "AuditoriaRecords");

            migrationBuilder.DropColumn(
                name: "EmailSubscription",
                table: "ScheduleEvents");

            migrationBuilder.RenameTable(
                name: "Subjects",
                newName: "Subject");

            migrationBuilder.RenameTable(
                name: "ScheduleEvents",
                newName: "Classes");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_TimeSlotIndex",
                table: "Classes",
                newName: "IX_Classes_TimeSlotIndex");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_SubjectId",
                table: "Classes",
                newName: "IX_Classes_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_StudentGroupId",
                table: "Classes",
                newName: "IX_Classes_StudentGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_ProfessorId",
                table: "Classes",
                newName: "IX_Classes_ProfessorId");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_CreatedById",
                table: "Classes",
                newName: "IX_Classes_CreatedById");

            migrationBuilder.RenameIndex(
                name: "IX_ScheduleEvents_AuditoriumId",
                table: "Classes",
                newName: "IX_Classes_AuditoriumId");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<Guid>(
                name: "GroupId",
                table: "Users",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LinkedUserId",
                table: "Staff",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "FacultyId",
                table: "Staff",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "LinkedUserId",
                table: "Professors",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProfessorId",
                table: "ProfessorRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "FacultyId",
                table: "FacultyRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "BuildingId",
                table: "BuildingsRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "AuditoriumId",
                table: "AuditoriaRecords",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subject",
                table: "Subject",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Classes",
                table: "Classes",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Buildings",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("06ccf9ac-179f-4b73-bd3a-65401fa0a41d"), "23 корпус" },
                    { new Guid("093eb00b-9ca5-411d-8b62-88559b09e92f"), "6 корпус" },
                    { new Guid("0e5ea39c-2bcb-46bc-a94c-c7740f733ca1"), "7 корпус" },
                    { new Guid("1aae5ef0-05d0-4641-b9e6-b18b106dee76"), "17 корпус" },
                    { new Guid("26aff5be-1ea9-4e91-a349-2b8451dfc1c2"), "12 корпус" },
                    { new Guid("27f7547b-7e2e-48b2-a7d8-2ef5610d123f"), "Криогенный корпус" },
                    { new Guid("348c0484-5dce-49ee-aa00-40da52aab306"), "20 корпус" },
                    { new Guid("4935c4ad-acb9-44e3-9ce2-418af0c31c29"), "16 корпус" },
                    { new Guid("4bcce890-b3e1-451c-b6e6-6617b3a7094a"), "2 корпус" },
                    { new Guid("66bb0cf0-05cb-47de-854d-877f0bd22fd3"), "4 корпус" },
                    { new Guid("7505fc73-01c6-456d-abd3-14dd4a843f62"), "31 корпус" },
                    { new Guid("8662702e-1843-4d54-b31a-073cb6a8a54a"), "5 корпус" },
                    { new Guid("932a62cc-22e2-4e3b-8fa8-6bc7ef7e94a4"), "1 корпус" },
                    { new Guid("9a0d74e3-ea12-49a4-9c5f-bebf13baee81"), "34 корпус" },
                    { new Guid("9e873265-5ac8-41a3-9fe0-ff0ebe5bd959"), "29 корпус" },
                    { new Guid("a6d57755-5d55-4e20-83eb-de7d70293e6d"), "11 корпус " },
                    { new Guid("b060b282-8989-4c70-8557-55a613ad72e7"), "9 корпус" },
                    { new Guid("be5080c0-2949-4452-8567-083ab8507c30"), "32 корпус" },
                    { new Guid("c2821932-e694-4115-9ba3-84d8fb8f31b1"), "8 корпус" },
                    { new Guid("c8baeaf5-fecc-448c-b1c7-8000ff4f30b3"), "15 корпус" },
                    { new Guid("ceca1fab-9d5a-40d7-8748-10393770da1a"), "Онлайн" },
                    { new Guid("d27839eb-2f69-43c2-a59a-6840b0cbcb2d"), "21 корпус (Ботсад)" },
                    { new Guid("dc6f4e34-7b47-4f39-8843-327ccb9fbb55"), "24 корпус" },
                    { new Guid("e0f3ee23-8e68-49e2-8b39-4beeb700ce9b"), "18 корпус" },
                    { new Guid("e374ef46-abed-401a-a7b6-7b3561bf5abb"), "13 корпус (НИИББ)" },
                    { new Guid("e8c3ada9-0560-4fb2-b59d-467e5451722e"), "14 корпус" },
                    { new Guid("ec1c332b-0312-4c99-bc3f-c879f075656a"), "10 корпус" },
                    { new Guid("f26f345c-c241-44f0-a980-527cf60100bb"), "33 корпус" },
                    { new Guid("f278d59e-479e-4c73-8349-da8094e5befc"), "3 корпус" },
                    { new Guid("f6883bb1-e4ea-4f5b-b4c8-db81d9eaf02d"), "Детский сад № 49" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_GroupId",
                table: "Users",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_LinkedUserId",
                table: "Staff",
                column: "LinkedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Professors_LinkedUserId",
                table: "Professors",
                column: "LinkedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorRecords_ProfessorId",
                table: "ProfessorRecords",
                column: "ProfessorId");

            migrationBuilder.CreateIndex(
                name: "IX_FacultyRecords_FacultyId",
                table: "FacultyRecords",
                column: "FacultyId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingsRecords_BuildingId",
                table: "BuildingsRecords",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_AuditoriaRecords_AuditoriumId",
                table: "AuditoriaRecords",
                column: "AuditoriumId");

            migrationBuilder.AddForeignKey(
                name: "FK_AuditoriaRecords_Auditoria_AuditoriumId",
                table: "AuditoriaRecords",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingsRecords_Buildings_BuildingId",
                table: "BuildingsRecords",
                column: "BuildingId",
                principalTable: "Buildings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Auditoria_AuditoriumId",
                table: "Classes",
                column: "AuditoriumId",
                principalTable: "Auditoria",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Professors_ProfessorId",
                table: "Classes",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_StudentGroups_StudentGroupId",
                table: "Classes",
                column: "StudentGroupId",
                principalTable: "StudentGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Subject_SubjectId",
                table: "Classes",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_TimeSlots_TimeSlotIndex",
                table: "Classes",
                column: "TimeSlotIndex",
                principalTable: "TimeSlots",
                principalColumn: "Index",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Users_CreatedById",
                table: "Classes",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FacultyRecords_Faculties_FacultyId",
                table: "FacultyRecords",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorRecords_Professors_ProfessorId",
                table: "ProfessorRecords",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Users_LinkedUserId",
                table: "Professors",
                column: "LinkedUserId",
                principalTable: "Users",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Faculties_FacultyId",
                table: "Staff",
                column: "FacultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staff_Users_LinkedUserId",
                table: "Staff",
                column: "LinkedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_StudentGroups_GroupId",
                table: "Users",
                column: "GroupId",
                principalTable: "StudentGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
