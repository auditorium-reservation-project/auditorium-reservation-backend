﻿namespace AuditoriumReservation.DTO.Enumerations
{
    public enum ReservationStatus
    {
        Proposed,
        Approved
    }
}
