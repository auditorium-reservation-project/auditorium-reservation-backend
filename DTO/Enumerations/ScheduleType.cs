﻿namespace AuditoriumReservation.DTO.Enumerations
{
    public enum ScheduleType
    {
        StudentGroup,
        Professor,
        Auditorium
    }
}
