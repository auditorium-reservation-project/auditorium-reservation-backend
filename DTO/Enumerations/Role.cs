﻿namespace AuditoriumReservation.DTO.Enumerations
{
    public enum Role
    {
        Admin,
        Staff,
        Student
    }
}
