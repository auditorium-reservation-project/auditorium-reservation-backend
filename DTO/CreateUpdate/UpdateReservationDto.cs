﻿using AuditoriumReservation.DTO.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class UpdateReservationDto
    {
        public string? Name { get; set; }
        public string? Details { get; set; }
        public bool? NotifyByEmail { get; set; }
    }
}