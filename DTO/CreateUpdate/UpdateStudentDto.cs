﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class UpdateStudentDto
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MiddleName { get; set; }
        public Guid groupId { get; set; }
    }
}
