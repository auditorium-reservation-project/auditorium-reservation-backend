﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.CreateUpdate
{
    public class UpdateGroupDto
    {
        public string? Name { get; set; }
        public Guid? FacultyId { get; set; }
        public List<Guid>? StudentIds { get; set; }
    }
}
