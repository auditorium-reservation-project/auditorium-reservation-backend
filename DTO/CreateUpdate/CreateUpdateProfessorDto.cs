﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class CreateUpdateProfessorDto
    {
        public string? FullName { get; set; } = "Unnamed Professor";
        public ICollection<FacultyDto>? Faculties { get; set; }
        public Guid? LinkedUser { get; set; }
    }
}