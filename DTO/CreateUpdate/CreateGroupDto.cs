﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.CreateUpdate
{
    public class CreateGroupDto
    {
        public string? Name { get; set; }
        [Required]
        public Guid FacultyId { get; set; }
        public List<Guid>? StudentIds { get; set; }
    }
}
