﻿using AuditoriumReservation.DTO.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class CreateReservationDto
    {
        [Required]
        public Guid AuditoriumId { get; set; }
        [Required]
        public DateOnly Date { get; set; }
        [Required]
        public int TimeSlotIndex { get; set; }
        [Required]
        public string Name { get; set; }
        public string? Details { get; set; }
        public bool NotifyByEmail { get; set; } = true;
    }
}