﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class CreateAuditoriumDto
    {
        [Required]
        [StringLength(50, ErrorMessage = "Auditorium Name must be between 5 and 50 symbols long", MinimumLength = 5)]
        public string Name { get; set; }
        [Required]
        public Guid BuildingId { get; set; }
    }
}