﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class CreateUpdateFacultyDto
    {
        public string? Name { get; set; }
        public ICollection<Guid>? GroupIds { get; set; }
        public ICollection<Guid>? ProfessorIds { get; set; }
    }
}
