﻿using AuditoriumReservation.DAL.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class UpdateUserDto
    {
        [DataType(DataType.EmailAddress)]
        public string? Email { get; set; }
        public string? Password { get; set; }
        [Url]
        public string? AvatarUrl { get; set; }
        [Display(Name = "Requested Roles")]
        public List<Role>? Roles { get; set; } = new List<Role> { Role.Student };
        [StringLength(80, MinimumLength = 1)]
        public string? FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
    }
}
