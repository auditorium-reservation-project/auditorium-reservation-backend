﻿using AuditoriumReservation.DTO.Enumerations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class UpdateClassDto

    {
        public Guid? SubjectId { get; set; }
        public Guid? ProfessorId { get; set; }
        public ICollection<Guid> GroupIds { get; set; } = new List<Guid>();
        public Guid? AuditoriumId { get; set; }
        public DateOnly? Date { get; set; }
        public ClassType? Type { get; set; }
    }
}