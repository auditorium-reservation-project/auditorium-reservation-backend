﻿using AuditoriumReservation.DTO.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class CreateClassDto
    {
        [Required]
        public Guid SubjectId { get; set; }
        [Required]
        public Guid ProfessorId { get; set; }
        [Required]
        public ICollection<Guid> GroupIds { get; set; } = new List<Guid>();
        [Required]
        public Guid AuditoriumId { get; set; }
        [Required]
        public DateOnly Date { get; set; }
        [Required]
        public int TimeSlotIndex { get; set; }
        [Required]
        public ClassType Type { get; set; }
    }
}