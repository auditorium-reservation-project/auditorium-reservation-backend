﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class CreateUpdateSubjectDto
    {
        public string? Name { get; set; }
    }
}