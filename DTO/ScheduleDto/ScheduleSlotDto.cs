﻿using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.Enumerations;

namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleSlotDto
    {
        public ScheduleSlotType Type { get; set; }
        public int SlotIndex { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        //Class
        public Guid? ClassId { get; set; }
        public string? Name { get; set; }
        public string? ProfessorName { get; set; }
        public ICollection<string>? StudentGroupNames { get; set; }
        public string? AuditoriumName { get; set; }
        public ClassType? ClassType { get; set; }
        //Reservation
        public Guid? ReservationId { get; set; }
        public DateTime? ReservedOn { get; set; }
        public string? Title { get; set; }
        public string? Desc { get; set; }
        public UserDto? ReservedBy { get; set; }
    }
}
