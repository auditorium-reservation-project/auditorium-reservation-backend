﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.Converters;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Services.Schedule;

namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleConflictDto
    {
        public string? Message;
        public ScheduleConflictDto(string? message = null)
        {
            this.Message = message;
        }
    }

    public class ClassConcurrencyConflictDto : ScheduleConflictDto
    {
        public ConcurrencyConflictType ConflictType;
        public DateOnly Date;
        public TimeSlotDto TimeSlot;
        public ClassDto Occupant;
        public ClassConcurrencyConflictDto(ClassConcurrencyConflict conflict)
        {
            this.Date = conflict.Date;
            this.TimeSlot = TimeSlotConverter.FromTimeSlot(conflict.TimeSlot);
            Occupant = ClassConverter.FromClass(conflict.Occupant);
        }
    }
}
