﻿namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleDayDto
    {
        public DateOnly Date { get; set; }
        public ICollection<ScheduleSlotDto> Slots { get; set; } = new List<ScheduleSlotDto>(); 
    }
}
