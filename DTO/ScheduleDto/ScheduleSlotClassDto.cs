﻿using AuditoriumReservation.DTO.Enumerations;

namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleSlotClassDto: ScheduleSlotDto
    {
        public ScheduleSlotClassDto() { this.Type = ScheduleSlotType.Class; }        
    }
}
