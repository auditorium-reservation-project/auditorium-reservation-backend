﻿namespace AuditoriumReservation.DTO.ScheduleDto
{
    public enum ScheduleSlotType
    {
        Empty,
        Class,
        Reservation
    }
}