﻿namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleDto
    {
        public DateOnly StartDate { get; set; }
        public DateOnly EndDate { get; set; }
        public string RequestedForEntityName { get; set; }
        public ICollection<ScheduleDayDto> Days { get; set; } = new List<ScheduleDayDto>();
    }
}
