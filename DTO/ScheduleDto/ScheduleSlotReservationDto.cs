﻿using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleSlotReservationDto: ScheduleSlotClassDto
    {
        public ScheduleSlotReservationDto() { this.Type = ScheduleSlotType.Reservation; }        
    }
}
