﻿namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ScheduleSlotEmpty: ScheduleSlotDto
    {
        public ScheduleSlotEmpty() { this.Type = ScheduleSlotType.Empty; }
    }
}
