﻿using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.Services;

namespace AuditoriumReservation.DTO.ScheduleDto
{
    public class ReservationCancellationDto
    {
        public ReservationDto Reservation { get; set; }
        public CancellationReason Reason { get; set; }
        public ClassDto? ConflictingClass { get; set; }
    }
}
