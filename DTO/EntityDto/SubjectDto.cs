﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class SubjectDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}