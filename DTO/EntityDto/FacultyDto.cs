﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class FacultyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<GroupDto>? Groups { get; set; }
        public ICollection<ProfessorDto>? Professors { get; set; }
    }
}
