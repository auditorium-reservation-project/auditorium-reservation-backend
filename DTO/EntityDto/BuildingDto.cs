﻿using System;
using System.Collections;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class BuildingDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<AuditoriumDto>? Auditoria { get; set; } = null;
    }
}
