﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class GroupDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = "Unnamed Group";
        public FacultyDto? Faculty { get; set; }
    }
}
