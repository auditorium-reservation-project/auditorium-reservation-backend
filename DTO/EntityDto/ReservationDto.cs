﻿using AuditoriumReservation.DTO.Enumerations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class ReservationDto
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public Guid? CreatorId { get; set; }
        public string? CreatorName { get; set; }
        public string Title { get; set; }
        public string? Details { get; set; }
        public DateOnly Date { get; set; }
        public AuditoriumDto Auditorium { get; set; }
        public TimeSlotDto TimeSlot { get; set; }
        public ReservationStatus Status { get; set; }
    }
}
