﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class ProfessorDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; } = "Unnamed Professor";
        public string? Email { get; set; }
        public ICollection<FacultyDto>? Faculties { get; set; }
        public UserDto? LinkedUser { get; set; }
    }
}