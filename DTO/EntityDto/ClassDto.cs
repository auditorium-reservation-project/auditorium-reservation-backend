﻿using AuditoriumReservation.DTO.Enumerations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class ClassDto
    {
        public Guid Id { get; set; }
        public SubjectDto Subject { get; set; }
        public ProfessorDto Professor { get; set; }
        public ICollection<GroupDto> Groups { get; set; } = new List<GroupDto>();
        public AuditoriumDto Auditorium { get; set; }
        public ClassType Type { get; set; }
    }

    public class ClassExtendedDto : ClassDto
    {
        public DateOnly StartDate { get; set; }
        public DateOnly EndDate { get; set; }
    }
}