﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class TimeSlotDto
    {
        public int Index { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
    }
}
