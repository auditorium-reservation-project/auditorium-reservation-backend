﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DAL.Enumerations;
using AuditoriumReservation.DTO.Enumerations;
using System.Runtime.CompilerServices;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class RoleApplicationDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public Enumerations.Role Role { get; set; }
        public DateTime DateTimeOfApplication { get; set; }
    }

    public static class RoleApplicationConverter
    {
        public static RoleApplicationDto ToDto(this RoleApplication appl)
        {
            return new RoleApplicationDto()
            {
                DateTimeOfApplication = appl.DateTimeOfApplication,
                Id = appl.Id,
                Role = matchRoleToDtoRole(appl.Role),
                UserId = appl.User.Id,
                UserName = appl.User.FullName
            };
        }

        public static Enumerations.Role matchRoleToDtoRole(DAL.Enumerations.Role role) => role switch
        {
            DAL.Enumerations.Role.Student => Enumerations.Role.Student,
            DAL.Enumerations.Role.Admin => Enumerations.Role.Admin,
            DAL.Enumerations.Role.Staff => Enumerations.Role.Staff,
            _ => Enumerations.Role.Student
        };
    }
}
