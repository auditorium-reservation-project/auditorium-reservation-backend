﻿using AuditoriumReservation.DTO.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class UserDto
    {
        [Required]
        public Guid Id { get; set; }
        public string? Email { get; set; }
        public ICollection<Role> Roles { get; set; } = new List<Role>();
        public string? AvatarUrl;
        [Required]
        public string FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
    }
}
