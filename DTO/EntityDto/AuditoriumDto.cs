﻿namespace AuditoriumReservation.DTO.EntityDto
{
    public class AuditoriumDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public BuildingDto? Building { get; set; } = null;
    }
}