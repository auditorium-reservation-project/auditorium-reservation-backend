﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.EntityDto
{
    public class StudentDto : UserDto
    {
        public GroupDto Group { get; set; }
    }
}
