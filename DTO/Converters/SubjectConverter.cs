﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.Converters
{
    public static class SubjectConverter
    {
        public static SubjectDto FromSubject(Subject subject)
        {
            return new SubjectDto()
            {
                Id = subject.Id,
                Name = subject.Name
            };
        }
    }
}