﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.Converters
{
    public static class ProfessorConverter
    {
        public static ProfessorDto FromProfessor(Professor professor)
        {
            return new ProfessorDto
            {
                Id = professor.Id,
                FullName = professor.Name
            };
        }

        public static ProfessorDto FromProfessorFull(Professor professor)
        {
            return new ProfessorDto
            {
                Id = professor.Id,
                FullName = professor.Name,
                Email = professor.LinkedUser?.Email,
                LinkedUser = professor.LinkedUser is not null ?
                    UserConverter.FromUser(professor.LinkedUser)
                    : null,
                Faculties = professor?.Faculties.Select(x => FacultyConverter.FromFaculty(x)).ToArray() ?? null
            };
        }
    }
}
