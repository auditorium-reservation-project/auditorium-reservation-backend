﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.ScheduleDto;
using AuditoriumReservation.Services;

namespace AuditoriumReservation.DTO.Converters
{
    public static class ReservationConverter
    {
        public static ReservationDto ToDto(this Reservation reservation)
        {
            return new ReservationDto()
            {
                Id = reservation.Id,
                Auditorium = AuditoriumConverter.FromAuditorum(reservation.Auditorium),
                CreatorId = reservation.CreatedBy.Id,
                CreatorName = reservation.CreatedBy.FullName,
                Created = reservation.CreatedOn,
                TimeSlot = TimeSlotConverter.FromTimeSlot(reservation.TimeSlot),
                Date = reservation.Date,
                Details = reservation.Details,
                Title = reservation.Name
            };
        }
        public static ReservationCancellationDto FromCancelled(Reservation reservation,
            CancellationReason reason,
            Class? conflictingClass = null)
        {
            var dto = new ReservationCancellationDto()
            {
                Reason = reason,
                Reservation = ReservationConverter.ToDto(reservation)
            };

            if (conflictingClass is not null)
            {
                dto.ConflictingClass = ClassConverter.FromClass(conflictingClass);
            }
            return dto;
        }
    }
}
