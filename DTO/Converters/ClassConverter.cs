﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DTO.Enumerations;
using DtoClassType = AuditoriumReservation.DTO.Enumerations.ClassType;
using ClassType = AuditoriumReservation.DAL.Enumerations.ClassType;

namespace AuditoriumReservation.DTO.Converters
{
    public static class ClassConverter
    {
        public static DtoClassType MatchTypeToDtoType(ClassType type) => type switch
        {
            ClassType.LaboratoryWork => DtoClassType.LaboratoryWork,
            ClassType.Control => DtoClassType.Control,
            ClassType.Practicum => DtoClassType.Practicum,
            ClassType.Lecture => DtoClassType.Lecture,
            ClassType.Seminar => DtoClassType.Seminar,
            ClassType.Credit => DtoClassType.Credit,
            ClassType.DiffCredit => DtoClassType.DiffCredit,
            ClassType.Consultation => DtoClassType.Consultation,
            ClassType.Individual => DtoClassType.Individual,
            ClassType.Exam => DtoClassType.Exam,
            ClassType.Other => DtoClassType.Other,
            _ => DtoClassType.Other
        };
        public static ClassDto FromClass(Class @class)
        {
            return new ClassDto
            {
                Id = @class.Id,
                Type = MatchTypeToDtoType(@class.Type),
                Auditorium = AuditoriumConverter.FromAuditorum(@class.Auditorium),
                Groups = @class.StudentGroups.Select(x =>  StudentGroupConverter.FromGroup(x)).ToArray(),
                Professor = ProfessorConverter.FromProfessor(@class.Professor),
                Subject = SubjectConverter.FromSubject(@class.Subject)
            };
        }

        public static ClassType MatchDtoTypeToType(DtoClassType type) => type switch
        {
            DtoClassType.LaboratoryWork => ClassType.LaboratoryWork,
            DtoClassType.Control => ClassType.Control,
            DtoClassType.Practicum => ClassType.Practicum,
            DtoClassType.Lecture => ClassType.Lecture,
            DtoClassType.Seminar => ClassType.Seminar,
            DtoClassType.Consultation => ClassType.Consultation,
            DtoClassType.DiffCredit => ClassType.DiffCredit,
            DtoClassType.Credit => ClassType.Credit,
            DtoClassType.Exam => ClassType.Exam,
            DtoClassType.Other => ClassType.Other,
            _ => ClassType.Other
        };
    }
}
