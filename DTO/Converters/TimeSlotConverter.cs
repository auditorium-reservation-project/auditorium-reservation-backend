﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.Converters
{
    public static class TimeSlotConverter
    {
        public static TimeSlotDto FromTimeSlot(TimeSlot timeSlot)
        {
            return new TimeSlotDto()
            {
                Index = timeSlot.Index,
                StartTime = timeSlot.StartTime,
                EndTime = timeSlot.EndTime
            };
        }
    }
}
