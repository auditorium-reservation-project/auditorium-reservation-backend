﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using AuditoriumReservation.DAL.Enumerations;
using DtoRole = AuditoriumReservation.DTO.Enumerations.Role;


namespace AuditoriumReservation.DTO.Converters
{
    public static class UserConverter
    {
        public static UserDto FromUser(User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                AvatarUrl = user.AvatarUrl,
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = user.MiddleName,
                Roles = user.Roles.Select(x => MatchRoleToDtoRole(x)).ToArray(),
            };
        }
        public static DtoRole MatchRoleToDtoRole(DAL.Enumerations.Role role) => role switch
        {
            Role.Student => DtoRole.Student,
            Role.Admin => DtoRole.Admin,
            Role.Staff => DtoRole.Staff,
            _ => DtoRole.Student
        };
        public static Role MatchDtoRoleToRole(DtoRole role) => role switch
        {
            DtoRole.Student => Role.Student,
            DtoRole.Admin => Role.Admin,
            DtoRole.Staff => Role.Staff,
            _ => Role.Student
        };
    }
}
