﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.Converters
{
    public static class BuildingConverter
    {
        public static BuildingDto FromBuilding(Building building)
        {
            return new BuildingDto()
            {
                Id = building.Id,
                Name = building.Name
            };
        }

        public static BuildingDto FromBuildingFull(Building building)
        {
            var auditoria = building.Auditoria.Select(a => AuditoriumConverter.FromAuditorum(a)).ToList();            
            return new BuildingDto
            {
                Id = building.Id,
                Name = building.Name,
                Auditoria = auditoria
            };
        }
    }
}
