﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.Converters
{
    public static class FacultyConverter
    {
        public static FacultyDto FromFaculty(Faculty faculty)
        {
            return new FacultyDto
            {
                Id = faculty.Id,
                Name = faculty.Name
            };
        }

        public static FacultyDto FromFacultyFull(Faculty faculty)
        {
            var groups = faculty.StudentGroups.Select(g => StudentGroupConverter.FromGroup(g)).ToList();
            var professors = faculty?.Professors?.Select(p => ProfessorConverter.FromProfessor(p)).ToList();
            return new FacultyDto
            {
                Id = faculty.Id,
                Name = faculty.Name,
                Groups = groups,
                Professors = professors
            };
        }
    }
}
