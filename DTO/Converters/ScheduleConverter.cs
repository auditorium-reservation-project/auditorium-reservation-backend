﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.ScheduleDto;
using System.Security.Claims;

namespace AuditoriumReservation.DTO.Converters
{
    public static class ScheduleConverter
    {
        public static ScheduleSlotDto SlotDtoFromClass(Class @class)
        {
            return new ScheduleSlotDto()
            {
                Type = ScheduleSlotType.Class,
                Name = @class.Subject.Name,
                AuditoriumName = @class.Auditorium.Name,
                ClassId = @class.Id,
                ClassType = ClassConverter.MatchTypeToDtoType(@class.Type),
                StartTime = @class.TimeSlot.StartTime,
                EndTime = @class.TimeSlot.EndTime,
                ProfessorName = @class.Professor.Name,
                SlotIndex = @class.TimeSlot.Index,
                //StudentGroupNames = @class.StudentGroups.Select(x => x.Name).ToList()
                StudentGroupNames = @class.StudentGroups.Select(x => x.Name).ToList(),
            };
        }

        public static ScheduleSlotDto SlotDtoFromTimeSlot(TimeSlot slot)
        {
            return new ScheduleSlotDto
            {
                Type = ScheduleSlotType.Empty,
                StartTime = slot.StartTime,
                EndTime = slot.EndTime,
                SlotIndex = slot.Index
            };
        }

        public static ScheduleSlotDto SlotDtoFromReservation(Reservation res) {
            return new ScheduleSlotDto()
            {
                Type = ScheduleSlotType.Reservation,
                ReservedOn = res.CreatedOn,
                ReservedBy = UserConverter.FromUser(res.CreatedBy),
                Title = res.Name,
                Desc = res.Details,
                AuditoriumName = res.Auditorium.Name,
                StartTime = res.TimeSlot.StartTime,
                EndTime = res.TimeSlot.EndTime,                
                SlotIndex = res.TimeSlot.Index,
                ReservationId = res.Id,
                Name = res.Name
            };
        }
    }
}
