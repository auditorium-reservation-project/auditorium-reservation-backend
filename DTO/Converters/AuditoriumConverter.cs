﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;

namespace AuditoriumReservation.DTO.Converters
{
    public static class AuditoriumConverter
    {
        public static AuditoriumDto FromAuditorum(Auditorium auditorium)
        {
            return new AuditoriumDto()
            {
                Id = auditorium.Id,
                Name = auditorium.Name
            };
        }

        public static AuditoriumDto FromAuditoriumFull(Auditorium auditorium)
        {
            var parentBuilding = BuildingConverter.FromBuilding(auditorium.Building);
            return new AuditoriumDto()
            {
                Id = auditorium.Id,
                Name = auditorium.Name,
                Building = parentBuilding
            };
        }
    }
}