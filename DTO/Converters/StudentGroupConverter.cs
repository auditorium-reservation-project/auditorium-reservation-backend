﻿using AuditoriumReservation.DAL.Entities;
using AuditoriumReservation.DTO.EntityDto;
using System.Text.RegularExpressions;

namespace AuditoriumReservation.DTO.Converters
{
    public static class StudentGroupConverter
    {
        public static GroupDto FromGroup(StudentGroup group)
        {
            return new GroupDto()
            {
                Id = group.Id,
                Name = group.Name
            };
        }

        public static GroupDto FromGroupFull(StudentGroup group) {
            return new GroupDto()
            {
                Id = group.Id,
                Name = group.Name,
                Faculty = FacultyConverter.FromFaculty(group.Faculty)
            };
        }
    }
}