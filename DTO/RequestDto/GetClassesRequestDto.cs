﻿using AuditoriumReservation.DTO.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.RequestDto
{
    public class GetClassesRequestDto
    {
        [Required]
        public ScheduleType Type { get; set; }
        [Required]
        public string RequestForEntityId { get; set; }
        [Required]
        public DateOnly StartDate { get; set; }
        [Required]
        public DateOnly EndDate { get; set; }
        public bool IncludeReservation { get; set; } = false;
    }
}