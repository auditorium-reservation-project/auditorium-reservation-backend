﻿using System.ComponentModel.DataAnnotations;

namespace AuditoriumReservation.DTO.RequestDto
{
    public class GetReservationsRequestDto
    {
        [Required]
        public DateOnly StartDate { get; set; }
        [Required]
        public DateOnly EndDate { get; set; }
        public ICollection<Guid>? AuditoriaIds { get; set; }
    }
}
